﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoxSports.WingController.Data.DataAccess
{
    using System;
    using System.Data;
    using System.Data.SqlClient;

    using FoxSports.WingController.Data.DataModel;

    public class TopicRundownAccess : ConnectionAccess, ITopicRundownAccess
    {
        public string WingDBConnectionString { get; set; }

        // Method to get specified Topic Rundown from SQL DB
        public DataTable GetTopicRundown(double topicRundownID)
        {
            DataTable dataTable = new DataTable();

            using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
            {
                // Create the command and set its properties
                sqlDataAdapter.SelectCommand = new SqlCommand();
                //sqlDataAdapter.SelectCommand.Connection = new SqlConnection(this.ConnectionString);
                sqlDataAdapter.SelectCommand.Connection = new SqlConnection(WingDBConnectionString);
                //sqlDataAdapter.SelectCommand.Connection = new SqlConnection("Data Source = OWNER-PC\\SQLEXPRESS;Initial Catalog = FoxSportsWing;User ID=sa;Password=Vds@dmin1");
                sqlDataAdapter.SelectCommand.CommandType = CommandType.Text;

                // Assign the SQL to the command object
                sqlDataAdapter.SelectCommand.CommandText = "sp_GetTopicRundown " + Convert.ToString(topicRundownID);

                // Fill the datatable from adapter
                sqlDataAdapter.Fill(dataTable);
            }
            return dataTable;
        }

        // Method to save current topic rundown to DB
        public bool SaveTopicRundown(double topicRundownID, DataTable topicRundown)
        {
            return true;
        }
        
        // Method to delete specified topic rundown from DB
        public bool DeleteTopicRundown(double topicRundownID)
        {
            return true;
        }
    }
}
