﻿namespace Fox_Sports_Playout_Controller
{
    partial class frmSkinSelect
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSkinSelect));
            this.label4 = new System.Windows.Forms.Label();
            this.TopicRundownPnl = new System.Windows.Forms.Panel();
            this.TakeSelectedTopicBtn = new System.Windows.Forms.Button();
            this.MoveTopicDownBtn = new System.Windows.Forms.Button();
            this.MoveTopicUpBtn = new System.Windows.Forms.Button();
            this.CreateNewTopicBtn = new System.Windows.Forms.Button();
            this.EditSelectedTopicBtn = new System.Windows.Forms.Button();
            this.AvailableSkinsGrid = new System.Windows.Forms.DataGridView();
            this.TakeNextTopicBtn = new System.Windows.Forms.Button();
            this.CancelBtn = new System.Windows.Forms.Button();
            this.LoadBtn = new System.Windows.Forms.Button();
            this.Skin_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TopicRundownPnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AvailableSkinsGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(8, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(130, 20);
            this.label4.TabIndex = 22;
            this.label4.Text = "Available Skins";
            // 
            // TopicRundownPnl
            // 
            this.TopicRundownPnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TopicRundownPnl.Controls.Add(this.TakeSelectedTopicBtn);
            this.TopicRundownPnl.Controls.Add(this.MoveTopicDownBtn);
            this.TopicRundownPnl.Controls.Add(this.MoveTopicUpBtn);
            this.TopicRundownPnl.Controls.Add(this.CreateNewTopicBtn);
            this.TopicRundownPnl.Controls.Add(this.EditSelectedTopicBtn);
            this.TopicRundownPnl.Controls.Add(this.AvailableSkinsGrid);
            this.TopicRundownPnl.Controls.Add(this.TakeNextTopicBtn);
            this.TopicRundownPnl.Location = new System.Drawing.Point(12, 32);
            this.TopicRundownPnl.Name = "TopicRundownPnl";
            this.TopicRundownPnl.Size = new System.Drawing.Size(400, 290);
            this.TopicRundownPnl.TabIndex = 21;
            // 
            // TakeSelectedTopicBtn
            // 
            this.TakeSelectedTopicBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TakeSelectedTopicBtn.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.Symbols_Play_32xLG;
            this.TakeSelectedTopicBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TakeSelectedTopicBtn.Location = new System.Drawing.Point(1056, 82);
            this.TakeSelectedTopicBtn.Name = "TakeSelectedTopicBtn";
            this.TakeSelectedTopicBtn.Size = new System.Drawing.Size(194, 63);
            this.TakeSelectedTopicBtn.TabIndex = 8;
            this.TakeSelectedTopicBtn.Text = "Take Selected";
            this.TakeSelectedTopicBtn.UseVisualStyleBackColor = true;
            // 
            // MoveTopicDownBtn
            // 
            this.MoveTopicDownBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MoveTopicDownBtn.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.arrow_Down_16xLG;
            this.MoveTopicDownBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.MoveTopicDownBtn.Location = new System.Drawing.Point(1158, 155);
            this.MoveTopicDownBtn.Name = "MoveTopicDownBtn";
            this.MoveTopicDownBtn.Size = new System.Drawing.Size(92, 35);
            this.MoveTopicDownBtn.TabIndex = 11;
            this.MoveTopicDownBtn.Text = "Move";
            this.MoveTopicDownBtn.UseVisualStyleBackColor = true;
            // 
            // MoveTopicUpBtn
            // 
            this.MoveTopicUpBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MoveTopicUpBtn.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.arrow_Up_16xLG;
            this.MoveTopicUpBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.MoveTopicUpBtn.Location = new System.Drawing.Point(1056, 155);
            this.MoveTopicUpBtn.Name = "MoveTopicUpBtn";
            this.MoveTopicUpBtn.Size = new System.Drawing.Size(92, 35);
            this.MoveTopicUpBtn.TabIndex = 10;
            this.MoveTopicUpBtn.Text = "Move";
            this.MoveTopicUpBtn.UseVisualStyleBackColor = true;
            // 
            // CreateNewTopicBtn
            // 
            this.CreateNewTopicBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateNewTopicBtn.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.action_add_16xLG;
            this.CreateNewTopicBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.CreateNewTopicBtn.Location = new System.Drawing.Point(1056, 245);
            this.CreateNewTopicBtn.Name = "CreateNewTopicBtn";
            this.CreateNewTopicBtn.Size = new System.Drawing.Size(194, 35);
            this.CreateNewTopicBtn.TabIndex = 9;
            this.CreateNewTopicBtn.Text = "Create New";
            this.CreateNewTopicBtn.UseVisualStyleBackColor = true;
            // 
            // EditSelectedTopicBtn
            // 
            this.EditSelectedTopicBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EditSelectedTopicBtn.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.dialog_32xLG;
            this.EditSelectedTopicBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.EditSelectedTopicBtn.Location = new System.Drawing.Point(1056, 200);
            this.EditSelectedTopicBtn.Name = "EditSelectedTopicBtn";
            this.EditSelectedTopicBtn.Size = new System.Drawing.Size(194, 35);
            this.EditSelectedTopicBtn.TabIndex = 8;
            this.EditSelectedTopicBtn.Text = "Edit Selected";
            this.EditSelectedTopicBtn.UseVisualStyleBackColor = true;
            // 
            // AvailableSkinsGrid
            // 
            this.AvailableSkinsGrid.AllowUserToAddRows = false;
            this.AvailableSkinsGrid.AllowUserToDeleteRows = false;
            this.AvailableSkinsGrid.AllowUserToOrderColumns = true;
            this.AvailableSkinsGrid.AllowUserToResizeColumns = false;
            this.AvailableSkinsGrid.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.AvailableSkinsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.AvailableSkinsGrid.ColumnHeadersHeight = 26;
            this.AvailableSkinsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Skin_Name});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.AvailableSkinsGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.AvailableSkinsGrid.Location = new System.Drawing.Point(18, 12);
            this.AvailableSkinsGrid.MultiSelect = false;
            this.AvailableSkinsGrid.Name = "AvailableSkinsGrid";
            this.AvailableSkinsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.AvailableSkinsGrid.Size = new System.Drawing.Size(362, 262);
            this.AvailableSkinsGrid.TabIndex = 0;
            // 
            // TakeNextTopicBtn
            // 
            this.TakeNextTopicBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TakeNextTopicBtn.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.Symbols_Play_32xLG;
            this.TakeNextTopicBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TakeNextTopicBtn.Location = new System.Drawing.Point(1056, 11);
            this.TakeNextTopicBtn.Name = "TakeNextTopicBtn";
            this.TakeNextTopicBtn.Size = new System.Drawing.Size(194, 63);
            this.TakeNextTopicBtn.TabIndex = 7;
            this.TakeNextTopicBtn.Text = "Take Next (F5)";
            this.TakeNextTopicBtn.UseVisualStyleBackColor = true;
            // 
            // CancelBtn
            // 
            this.CancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CancelBtn.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.StatusAnnotations_Critical_16xLG;
            this.CancelBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.CancelBtn.Location = new System.Drawing.Point(218, 328);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(194, 35);
            this.CancelBtn.TabIndex = 20;
            this.CancelBtn.Text = "Cancel";
            this.CancelBtn.UseVisualStyleBackColor = true;
            // 
            // LoadBtn
            // 
            this.LoadBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoadBtn.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.StatusAnnotations_Complete_and_ok_16xLG;
            this.LoadBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.LoadBtn.Location = new System.Drawing.Point(12, 328);
            this.LoadBtn.Name = "LoadBtn";
            this.LoadBtn.Size = new System.Drawing.Size(194, 35);
            this.LoadBtn.TabIndex = 19;
            this.LoadBtn.Text = "Load";
            this.LoadBtn.UseVisualStyleBackColor = true;
            this.LoadBtn.Click += new System.EventHandler(this.LoadBtn_Click);
            // 
            // Skin_Name
            // 
            this.Skin_Name.HeaderText = "Skin Name";
            this.Skin_Name.Name = "Skin_Name";
            this.Skin_Name.ReadOnly = true;
            this.Skin_Name.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Skin_Name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Skin_Name.Width = 300;
            // 
            // SkinSelect
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 372);
            this.ControlBox = false;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TopicRundownPnl);
            this.Controls.Add(this.CancelBtn);
            this.Controls.Add(this.LoadBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SkinSelect";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Show Look (Skin) Select";
            this.TopicRundownPnl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AvailableSkinsGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel TopicRundownPnl;
        private System.Windows.Forms.Button TakeSelectedTopicBtn;
        private System.Windows.Forms.Button MoveTopicDownBtn;
        private System.Windows.Forms.Button MoveTopicUpBtn;
        private System.Windows.Forms.Button CreateNewTopicBtn;
        private System.Windows.Forms.Button EditSelectedTopicBtn;
        private System.Windows.Forms.DataGridView AvailableSkinsGrid;
        private System.Windows.Forms.Button TakeNextTopicBtn;
        private System.Windows.Forms.Button CancelBtn;
        private System.Windows.Forms.Button LoadBtn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Skin_Name;
    }
}