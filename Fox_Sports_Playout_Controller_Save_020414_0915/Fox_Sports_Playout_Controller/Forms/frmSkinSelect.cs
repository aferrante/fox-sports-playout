﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fox_Sports_Playout_Controller
{
    public partial class frmSkinSelect : Form
    {
        public frmSkinSelect()
        {
            InitializeComponent();

            //Fill with dummy data
            AvailableSkinsGrid.RowCount = 2;
            DataGridViewRow row1 = AvailableSkinsGrid.Rows[0];
            row1.Cells["Skin_Name"].Value = "Default";
            DataGridViewRow row2 = AvailableSkinsGrid.Rows[1];
            row2.Cells["Skin_Name"].Value = "UFC on Fox";

        }

        private void LoadBtn_Click(object sender, EventArgs e)
        {

        }
    }
}
