﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoxSports.WingController.Data.Enums
{
    using System.ComponentModel;

    // Enumerator for template fields types
    public enum TemplateFieldTypes
    {
        // Template field - Text
        [Description("Text")]
        Text = 1,

        // Template field - Team Logo
        [Description("LeagueLogo")]
        LeagueLogo,

        // Template field - Team Logo
        [Description("TeamLogo")]
        TeamLogo,

        // Template field - Player Headshot
        [Description("Headshot")]
        Headshot
    }
}
