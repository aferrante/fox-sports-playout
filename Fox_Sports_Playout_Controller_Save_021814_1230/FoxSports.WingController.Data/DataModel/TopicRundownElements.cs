﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPoco;

namespace FoxSports.WingController.Data.DataModel
{
    /// <summary>
    /// Class definition for topic rundown element collection
    /// </summary>
    [TableName("TopicRundownElements")]
    [PrimaryKey("RundownId,PageID")]
    public class TopicRundownElements
    {
        public double rundownId { get; set; }
        public Int32 pageId { get; set; }       
        public bool enabled { get; set; }
        public int templateId { get; set; }
        public string slug { get; set; }
        public string longSlug { get; set; }
    }
}
