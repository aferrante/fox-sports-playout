﻿// Main form for Fox Sports Wing Playout Controller application
// © Copyright 2013-2014 Video Design Software Inc. - All rights reserved.
// You may use this code module and all associated code modules only under the specific terms of the source code license between Video Design Software Inc. and Fox Sports Media Group.
// M Dilworth  Video Design Software 
// Rev: 2014/02/10

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using FoxSports.WingController.VizIntf.VizControlFunctions;
using FoxSports.WingController.VizIntf.ClientSocket;
using FoxSports.WingController.Data.DataAccess;
using FoxSports.WingController.Logic.RundownCollection;
using FoxSports.WingController.Logic.SceneActions;
using FoxSports.WingController.Data.DataModel;
using FoxSports.WingController.Data.Enums;

namespace Fox_Sports_Playout_Controller
{
    /// <summary>
    /// Class definition for main program form
    /// </summary>
    public partial class frmMain : Form
    {
        /// <summary>
        /// Globals
        /// </summary>
        // Flag to indicator wing is in
        bool wingIn = false;
        // Database connection string
        private string wingDBConnectionString;

        // Define the collection object for the Topic Rundown
        private TopicRundownCollection topicRundownCollection;
        BindingList<TopicRundownElementModel> topicRundownElements;

        // Define the class for the scene actions collection
        private SceneActionCommandsCollection sceneActionCommandsCollection;
        List<SceneActionCommandModel> sceneActionCommands;

        // Data strucure for Widget Rundown elements
        struct widgetRundownElement
        {
            // Page IDs declared as double to match SQL type float
            public double rundownid;
            public double pageid;
            public bool enabled;
            public int templateId;
            public string data;
            public string supplementalData;
        }

        /// <summary>
        /// Define the collection object for the Widget Rundown
        /// </summary>
        List<widgetRundownElement> widgetRundownElements = new List<widgetRundownElement>(500);

        /// <summary>
        /// Declare socket parameters for Viz communications & Viz scene name
        /// </summary>
        private Int32 socketPort;
        private string socketIPAddress;
        private string vizSceneName;
        public VizControlPort VizControl;

        /// <summary>
        /// Function to read in data from application config file
        /// </summary>
        private void ReadInConfigData()
        {
            //Read in values from the config file
            socketIPAddress = Properties.Settings.Default.vizControlIPAddress;
            socketPort = Properties.Settings.Default.vizControlPortNumber;
            wingDBConnectionString = Properties.Settings.Default.wingDBConnectionString;
            vizSceneName = Properties.Settings.Default.vizSceneName;
        }

        /// <summary>
        /// Handler for main form activation
        /// </summary>
        public frmMain()
        {
            InitializeComponent();

            // Read in values from the config file
            ReadInConfigData();

            // Setup topic rundown collection 
            this.topicRundownCollection = new TopicRundownCollection();
            this.topicRundownCollection.WingDBConnectionString = wingDBConnectionString;
            topicRundownElements = this.topicRundownCollection.GetTopicRundownCollection(0);

            //var topicRundownBindingList = new BindingList<TopicRundownElementModel>(topicRundownElements);
            var gridDataSource = new BindingSource(topicRundownElements, null);
            TopicRundownGrid.DataSource = gridDataSource;

            // Get the current data for the topic rundown from the database
            GetTopicRundownElementsFromDB();

            // Get the data for the widget rundown from the database
            GetWidgetRundownElementsFromDB();

            // Update the widget rundown data grid
            UpdateWidgetRundownGrid();

            // Instantiate Viz interface component & connect to the Viz engine
            VizControl = new VizControlPort(System.Net.IPAddress.Parse(socketIPAddress), socketPort, true);
            // Initialize the events
            VizControl.DataReceived += new TCPClientWrapper.delDataReceived(VizControl_DataReceived);
            VizControl.ConnectionStatusChanged += new TCPClientWrapper.delConnectionStatusChanged(VizControl_ConnectionStatusChanged);
            // Connect
            VizControl.AutoReconnect = true;
            VizControl.Connect();
            VizControl.LoadScene(vizSceneName);

            // Setup the scene commands collection
            this.sceneActionCommandsCollection = new SceneActionCommandsCollection(VizControl, topicRundownElements);
            this.sceneActionCommandsCollection.WingDBConnectionString = wingDBConnectionString;
            sceneActionCommands = this.sceneActionCommandsCollection.GetSceneActionCommandCollection();
            // For testing
            sceneActionCommandsCollection.CommandAdded += new SceneActionCommandsCollection.delCommandAdded(AddToListbox);

            // Enable timers
            TimeOfDayTimer.Enabled = true;
        }

        // Test Function
        private void AddToListbox(SceneActionCommandsCollection sender, string Command)
        {
            //listBox1.Items.Add(Command);
        }

        //Fired when the connection status changes in the TCP client       
        void VizControl_ConnectionStatusChanged(TCPClientWrapper sender, VizControlPort.ConnectionStatus status)
        {
            //Check if this event was fired on a different thread, if it is then we must invoke it on the UI thread
            if (InvokeRequired)
            {
                Invoke(new TCPClientWrapper.delConnectionStatusChanged(VizControl_ConnectionStatusChanged), sender, status);
                return;
            }
            statusLabel.Text = "Connection Status: " + status.ToString();

            //Set the indicator color if the connection is good
            if (status.ToString() == "Connected")
            {
                vizConnectLED.BackColor = Color.Lime;
            }
            else
            {
                vizConnectLED.BackColor = Color.Transparent;
            }
        }

        //Fired when new data is received in the TCP client
        void VizControl_DataReceived(TCPClientWrapper sender, object data)
        {
            //Check if this needs to be invoked in the UI thread
            if (InvokeRequired)
            {
                try
                {
                    Invoke(new VizControlPort.delDataReceived(VizControl_DataReceived), sender, data);
                }
                catch
                { }
                return;
            }

            //Interpret the received data object as a string
            string strData = data as string;

            //Add the received data to a rich text box
            statusLabel.Text = "Data received: " + strData.Trim() + "\r\n";
        }

        /// <summary>
        /// TOPIC RUNDOWN FUNCTIONS
        /// </summary>
        private void GetTopicRundownElementsFromDB()
        {
            topicRundownElements = this.topicRundownCollection.GetTopicRundownCollection(0);
            RundownNameLabel.Text = this.topicRundownCollection.TopicRundownName.ToString();
        }
        // Delete the selected entry from the topic rundown collection
        private void deleteSelectedItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (TopicRundownGrid.SelectedRows.Count > 0)
            {
                topicRundownCollection.DeleteTopicRundownElement((short)TopicRundownGrid.CurrentRow.Index);
                this.Validate();
            }
        }

        /// <summary>
        /// Move the selected item down in the topic rundown
        /// </summary>
        private void btnMoveTopicDown_Click(object sender, EventArgs e)
        {
            if (TopicRundownGrid.RowCount > 0)
            {
                topicRundownCollection.MoveTopicRundownElementDown((short)TopicRundownGrid.CurrentRow.Index);
                if (TopicRundownGrid.CurrentRow.Index < TopicRundownGrid.RowCount - 1)
                {
                    TopicRundownGrid.Rows[TopicRundownGrid.CurrentRow.Index + 1].Selected = true;
                    TopicRundownGrid.CurrentCell = TopicRundownGrid[0, TopicRundownGrid.CurrentRow.Index + 1];
                }
            }
        }

        /// <summary>
        /// Move the selected item up in the topic rundown
        /// </summary>
        private void btnMoveTopicUp_Click(object sender, EventArgs e)
        {
            if (TopicRundownGrid.RowCount > 0)
            {
                topicRundownCollection.MoveTopicRundownElementUp((short)TopicRundownGrid.CurrentRow.Index);
                if (TopicRundownGrid.CurrentRow.Index > 0)
                {
                    TopicRundownGrid.Rows[TopicRundownGrid.CurrentRow.Index - 1].Selected = true;
                    TopicRundownGrid.CurrentCell = TopicRundownGrid[0, TopicRundownGrid.CurrentRow.Index - 1];
                }
            }
        }

        /// <summary>
        /// Enable the selected item in the Topic Rundown
        /// </summary>
        private void enableSelectedItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (TopicRundownGrid.RowCount > 0)
            {
                //TopicRundownElementModel obj = (TopicRundownElementModel)TopicRundownGrid.CurrentRow.DataBoundItem;
                //obj.enabled = true;
                topicRundownCollection.EnableRundownElement((short)TopicRundownGrid.CurrentRow.Index);
                TopicRundownGrid.Refresh();

                // Need to force validation
                this.Validate();
            }
        }

        /// <summary>
        /// Disable the selected item in the Topic Rundown
        /// </summary>
        private void disableSelectedItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (TopicRundownGrid.RowCount > 0)
            {
                //TopicRundownElementModel obj = (TopicRundownElementModel)TopicRundownGrid.CurrentRow.DataBoundItem;
                //obj.enabled = false;
                topicRundownCollection.DisableRundownElement((short)TopicRundownGrid.CurrentRow.Index);
                TopicRundownGrid.Refresh();

                // Need to force validation
                this.Validate();
            }
        }

        /// <summary>
        /// Save the Topic Rundown out to the database
        /// </summary>
        private void btnSaveTopicPlaylist_Click(object sender, EventArgs e)
        {
            if (TopicRundownGrid.RowCount > 0)
            {
                topicRundownCollection.SaveTopicRundownCollection(topicRundownElements);
            }
        }

        // WIDGET RUNDOWN FUNCTIONS
        // Function to query the SQL database and get the data for the specified widgetrundown
        private void GetWidgetRundownElementsFromDB()
        {
            //Setup connection string
            string connectionString = "Data Source = OWNER-PC\\SQLEXPRESS;Initial Catalog = FoxSportsWing;User ID=sa;Password=Vds@dmin1";

            //Setup and open SQL connection
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();

            //Setup query
            string selectStatement = "SELECT * FROM WidgetRundownElements";
            //string selectStatement = "GetMessagesByType @Type";

            //Setup the SQL command using the query string
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            //selectCommand.Parameters.AddWithValue("@Type", "list");

            //Setup data reader & iterate through results
            SqlDataReader reader = selectCommand.ExecuteReader(CommandBehavior.CloseConnection);

            int count = 1;
            while (reader.Read())
            {
                //Add widget to the collection
                //Declare
                widgetRundownElement widget = new widgetRundownElement();
                //Set values
                widget.enabled = reader.GetBoolean(reader.GetOrdinal("Enabled"));
                //Page IDs declared as double to match SQL type float
                widget.rundownid = reader.GetDouble(reader.GetOrdinal("RundownId"));
                widget.pageid = reader.GetInt32(reader.GetOrdinal("PageId"));
                widget.templateId = reader.GetInt32(reader.GetOrdinal("TemplateId"));
                widget.data = reader["DataValue_1"].ToString();
                widget.supplementalData = reader["DataValue_2"].ToString();

                //Insert into collection
                widgetRundownElements.Add(widget);

                //Next record
                count++;
            }

            //Actually not required here beacuase reader object will close query when done
            connection.Close();
        }

        //Function to update the topic rundown grid
        private void UpdateWidgetRundownGrid()
        {
            for (int i = 0; i < widgetRundownElements.Count; i++)
            {
                WidgetRundownGrid.RowCount = widgetRundownElements.Count;
                DataGridViewRow row = WidgetRundownGrid.Rows[i];
                row.Cells["Widget_Enabled"].Value = widgetRundownElements[i].enabled;
                row.Cells["Widget_Id"].Value = widgetRundownElements[i].pageid;
                row.Cells["Widget_TemplateId"].Value = widgetRundownElements[i].templateId;
                row.Cells["Widget_Data"].Value = widgetRundownElements[i].data;
                row.Cells["Widget_SupplementalData"].Value = widgetRundownElements[i].supplementalData;
            }
        }

        //Handler for change in iNews monitoring checkbox status - enable/disable save/load playlist butons accordingly
        private void iNewsMonitoringEnableCheckbox_CheckStateChanged(object sender, EventArgs e)
        {
            if (iNewsMonitoringEnableCheckbox.Checked)
            {
                btnSaveTopicPlaylist.Visible = false;
                btnLoadTopicPlaylist.Visible = false;
                iNewsMonitoringEnableCheckbox.BackColor = Color.Transparent;
                iNewsMonitoringEnableCheckbox.Text = "iNews Monitoring Enabled";
            }
            else
            {
                btnSaveTopicPlaylist.Visible = true;
                btnLoadTopicPlaylist.Visible = true;
                iNewsMonitoringEnableCheckbox.BackColor = Color.Yellow;
                iNewsMonitoringEnableCheckbox.Text = "iNews Monitoring Disabled";
            }
        }

        //Handler for main Wing In/Out button
        private void WingInBtn_Click(object sender, EventArgs e)
        {
            if (wingIn == false)
            {
                //Set button/indicator properties & set flag
                wingIn = true;
                MainWingControlPanel.BackColor = Color.Lime;
                btnWingIn.Text = "Take Wing Out (F1)";
                btnWingIn.Image = Fox_Sports_Playout_Controller.Properties.Resources.StatusAnnotations_Stop_32xLG;
                // Send the command
                this.sceneActionCommandsCollection.SendPlayoutCommands((int)ActionIDs.Wing_In);
            }
            else
            {
                //Set button/indicator properties & clear flag
                wingIn = false;
                MainWingControlPanel.BackColor = Color.Transparent;
                btnWingIn.Text = "Take Wing In (F1)";
                btnWingIn.Image = Fox_Sports_Playout_Controller.Properties.Resources.StatusAnnotations_Play_32xLG_color;
                // Send the command
                this.sceneActionCommandsCollection.SendPlayoutCommands((int)ActionIDs.Wing_Out);
               
            }
        }

        #region Functions for launching support dialogs
        /// <summary>
        /// Handler for create new topic rundown element
        /// </summary>
        private void CreateNewTopicBtn_Click(object sender, EventArgs e)
        {
            Form createNewTopicRecord = new frmTemplateDataEntry();
            createNewTopicRecord.ShowDialog();
        }

        //Handler for load topic playlist button
        private void LoadTopicPlaylistBtn_Click(object sender, EventArgs e)
        {
            Form loadTopicPlaylist = new frmPlaylistLoad();
            loadTopicPlaylist.ShowDialog();
        }

        //Handler for Change Skin button
        private void button5_Click(object sender, EventArgs e)
        {
            Form selectSkin = new frmSkinSelect();
            selectSkin.ShowDialog();
        }

        //Handler for time of day timer
        private void TimeOfDayTimer_Tick(object sender, EventArgs e)
        {
            DateTime currentDateTime = DateTime.Now;
            lblTimeOfDay.Text = currentDateTime.ToString();
        }
        #endregion

        private void TopicRundownGrid_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        // Take next rundown element to air
        private void btnTakeNextTopic_Click(object sender, EventArgs e)
        {
            // Send the command
            if (wingIn)
            {
                this.sceneActionCommandsCollection.SendPlayoutCommands((int)ActionIDs.Send_Rundown);
            }
        }

    }
}
