﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoxSports.WingController.Data.DataModel
{
    public class TopicRundownElementModel
    {
        public TopicRundownElementModel()
        {
        }

        public double rundownid { get; set; }
        public Int32 pageid { get; set; }       
        public bool enabled { get; set; }
        public int templateId { get; set; }
        public string slug { get; set; }
        public string longSlug { get; set; }
    }
}
