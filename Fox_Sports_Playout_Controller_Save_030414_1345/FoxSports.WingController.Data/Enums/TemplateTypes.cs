﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoxSports.WingController.Data.Enums
{
    using System.ComponentModel;

    // Enumerator for template types
    public enum TemplateTypes
    {
        // Template - Rundown
        [Description("Rundown")]
        Rundown = 1,

        // Template - Widget
        [Description("Widget")]
        Widget
    }
}
