﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoxSports.WingController.Data.Enums
{
    using System.ComponentModel;

    // Enumerator for template fields types
    public enum ActionIDs
    {
        [Description("Wing In")]
        Wing_In = 1,

        [Description("Wing Out")]
        Wing_Out,

        [Description("Change Show Skin")]
        Change_Show_Skin,

        [Description("On Deck Feed In")]
        On_Deck_Feed_In,

        [Description("On Deck Feed Out")]
        On_Deck_Feed_Out,

        [Description("Send Rundown")]
        Send_Rundown,

        [Description("Update Rundown Row 1")]
        Update_Rundown_Row_1,

        [Description("Update Rundown Row 2")]
        Update_Rundown_Row_2,

        [Description("Update Rundown Row 3")]
        Update_Rundown_Row_3,
        
        [Description("Update Rundown Row 4")]
        Update_Rundown_Row_4,
        
        [Description("Update Rundown Row 5")]
        Update_Rundown_Row_5,
        
        [Description("Update Rundown Row 6")]
        Update_Rundown_Row_6,

        [Description("Live Update Rundown Row 1")]
        Live_Update_Rundown_Row_1,

        [Description("Live Update Rundown Row 2")]
        Live_Update_Rundown_Row_2,

        [Description("Live Update Rundown Row 3")]
        Live_Update_Rundown_Row_3,

        [Description("Live Update Rundown Row 4")]
        Live_Update_Rundown_Row_4,

        [Description("Live Update Rundown Row 5")]
        Live_Update_Rundown_Row_5,

        [Description("Live Update Rundown Row 6")]
        Live_Update_Rundown_Row_6,

        [Description("Live Update Rundown Note")]
        Live_Update_Rundown_Note,

        [Description("Live Update Rundown Color")]
        Live_Update_Rundown_Color,

        [Description("Widget In")]
        Widget_In,

        [Description("Widget Out")]
        Widget_Out,

        [Description("Widget Color Feed")]
        Widget_Color_Feed,

        [Description("Set Widget Title Size 1")]
        Set_Widget_Title_Size_1,

        [Description("Set Widget Title Size 2")]
        Set_Widget_Title_Size_2,

        [Description("Set Widget Title Line 1")]
        Set_Widget_Title_Line_1,

        [Description("Set Widget Title Line 2")]
        Set_Widget_Title_Line_2,

        [Description("Widget Update Note")]
        Widget_Update_Note,

        [Description("Widget Update Half Note Logo")]
        Widget_Update_Half_Note_Logo,

        [Description("Widget Update Half Note Logo Headshot")]
        Widget_Update_Half_Note_Logo_Headshot,

        [Description("Widget Update Half Note Headshot")]
        Widget_Update_Half_Note_Headshot,

        [Description("Widget Update Promo")]
        Widget_Update_Promo
    }
}
