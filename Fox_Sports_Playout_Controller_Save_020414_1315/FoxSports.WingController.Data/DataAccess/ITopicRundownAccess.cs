﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoxSports.WingController.Data.DataAccess
{
    using System.Data;
    using FoxSports.WingController.Data.DataModel;

    // Interface ITopicRundownAccess
    public interface ITopicRundownAccess
    {
        // Method to get topic rundown with specified ID
        DataTable GetTopicRundown(double topicRundownID);

        // Method to save current topic rundown to DB
        bool SaveTopicRundown(double topicRundownID, DataTable topicRundown);

        // Method to delete specified topic rundown from DB
        bool DeleteTopicRundown(double topicRundownID);
    }
}
