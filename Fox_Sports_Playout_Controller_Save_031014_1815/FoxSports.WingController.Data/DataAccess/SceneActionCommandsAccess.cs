﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoxSports.WingController.Data.DataAccess
{
    using System;
    using System.Data;
    using System.Data.SqlClient;

    using FoxSports.WingController.Data.DataModel;

    /// <summary>
    /// Class for handling database access for Scene Action Commands
    /// </summary>
    public class SceneActionCommandsAccess
    {
        #region Properties and Members
        public string WingDBConnectionString { get; set; }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method to get the specified Topic Rundown from SQL DB and pass it back to the logic layer as a DataTable
        /// </summary>
        public DataTable GetSceneActionCommands()
        {
            DataTable dataTable = new DataTable();

            try
            {
                // Instantiate the connection
                using (SqlConnection connection = new SqlConnection(WingDBConnectionString))
                {
                    // Create the command and set its properties
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
                        {
                            cmd.CommandText = "SELECT * FROM SceneActionCommands";
                            sqlDataAdapter.SelectCommand = cmd;
                            sqlDataAdapter.SelectCommand.Connection = connection;
                            sqlDataAdapter.SelectCommand.CommandType = CommandType.Text;

                            // Fill the datatable from adapter
                            sqlDataAdapter.Fill(dataTable);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return dataTable;
        }
        #endregion
    }
}
