﻿namespace Fox_Sports_Playout_Controller
{
    partial class frmPlaylistLoad
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPlaylistLoad));
            this.label4 = new System.Windows.Forms.Label();
            this.TopicRundownPnl = new System.Windows.Forms.Panel();
            this.TakeSelectedTopicBtn = new System.Windows.Forms.Button();
            this.MoveTopicDownBtn = new System.Windows.Forms.Button();
            this.MoveTopicUpBtn = new System.Windows.Forms.Button();
            this.CreateNewTopicBtn = new System.Windows.Forms.Button();
            this.EditSelectedTopicBtn = new System.Windows.Forms.Button();
            this.AvailablePlaylistsGrid = new System.Windows.Forms.DataGridView();
            this.TakeNextTopicBtn = new System.Windows.Forms.Button();
            this.CancelBtn = new System.Windows.Forms.Button();
            this.LoadBtn = new System.Windows.Forms.Button();
            this.Playlist_Name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TopicRundownPnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.AvailablePlaylistsGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(151, 20);
            this.label4.TabIndex = 26;
            this.label4.Text = "Available Playlists";
            // 
            // TopicRundownPnl
            // 
            this.TopicRundownPnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TopicRundownPnl.Controls.Add(this.TakeSelectedTopicBtn);
            this.TopicRundownPnl.Controls.Add(this.MoveTopicDownBtn);
            this.TopicRundownPnl.Controls.Add(this.MoveTopicUpBtn);
            this.TopicRundownPnl.Controls.Add(this.CreateNewTopicBtn);
            this.TopicRundownPnl.Controls.Add(this.EditSelectedTopicBtn);
            this.TopicRundownPnl.Controls.Add(this.AvailablePlaylistsGrid);
            this.TopicRundownPnl.Controls.Add(this.TakeNextTopicBtn);
            this.TopicRundownPnl.Location = new System.Drawing.Point(14, 32);
            this.TopicRundownPnl.Name = "TopicRundownPnl";
            this.TopicRundownPnl.Size = new System.Drawing.Size(400, 290);
            this.TopicRundownPnl.TabIndex = 25;
            // 
            // TakeSelectedTopicBtn
            // 
            this.TakeSelectedTopicBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TakeSelectedTopicBtn.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.Symbols_Play_32xLG;
            this.TakeSelectedTopicBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TakeSelectedTopicBtn.Location = new System.Drawing.Point(1056, 82);
            this.TakeSelectedTopicBtn.Name = "TakeSelectedTopicBtn";
            this.TakeSelectedTopicBtn.Size = new System.Drawing.Size(194, 63);
            this.TakeSelectedTopicBtn.TabIndex = 8;
            this.TakeSelectedTopicBtn.Text = "Take Selected";
            this.TakeSelectedTopicBtn.UseVisualStyleBackColor = true;
            // 
            // MoveTopicDownBtn
            // 
            this.MoveTopicDownBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MoveTopicDownBtn.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.arrow_Down_16xLG;
            this.MoveTopicDownBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.MoveTopicDownBtn.Location = new System.Drawing.Point(1158, 155);
            this.MoveTopicDownBtn.Name = "MoveTopicDownBtn";
            this.MoveTopicDownBtn.Size = new System.Drawing.Size(92, 35);
            this.MoveTopicDownBtn.TabIndex = 11;
            this.MoveTopicDownBtn.Text = "Move";
            this.MoveTopicDownBtn.UseVisualStyleBackColor = true;
            // 
            // MoveTopicUpBtn
            // 
            this.MoveTopicUpBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MoveTopicUpBtn.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.arrow_Up_16xLG;
            this.MoveTopicUpBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.MoveTopicUpBtn.Location = new System.Drawing.Point(1056, 155);
            this.MoveTopicUpBtn.Name = "MoveTopicUpBtn";
            this.MoveTopicUpBtn.Size = new System.Drawing.Size(92, 35);
            this.MoveTopicUpBtn.TabIndex = 10;
            this.MoveTopicUpBtn.Text = "Move";
            this.MoveTopicUpBtn.UseVisualStyleBackColor = true;
            // 
            // CreateNewTopicBtn
            // 
            this.CreateNewTopicBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateNewTopicBtn.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.action_add_16xLG;
            this.CreateNewTopicBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.CreateNewTopicBtn.Location = new System.Drawing.Point(1056, 245);
            this.CreateNewTopicBtn.Name = "CreateNewTopicBtn";
            this.CreateNewTopicBtn.Size = new System.Drawing.Size(194, 35);
            this.CreateNewTopicBtn.TabIndex = 9;
            this.CreateNewTopicBtn.Text = "Create New";
            this.CreateNewTopicBtn.UseVisualStyleBackColor = true;
            // 
            // EditSelectedTopicBtn
            // 
            this.EditSelectedTopicBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EditSelectedTopicBtn.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.dialog_32xLG;
            this.EditSelectedTopicBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.EditSelectedTopicBtn.Location = new System.Drawing.Point(1056, 200);
            this.EditSelectedTopicBtn.Name = "EditSelectedTopicBtn";
            this.EditSelectedTopicBtn.Size = new System.Drawing.Size(194, 35);
            this.EditSelectedTopicBtn.TabIndex = 8;
            this.EditSelectedTopicBtn.Text = "Edit Selected";
            this.EditSelectedTopicBtn.UseVisualStyleBackColor = true;
            // 
            // AvailablePlaylistsGrid
            // 
            this.AvailablePlaylistsGrid.AllowUserToAddRows = false;
            this.AvailablePlaylistsGrid.AllowUserToDeleteRows = false;
            this.AvailablePlaylistsGrid.AllowUserToOrderColumns = true;
            this.AvailablePlaylistsGrid.AllowUserToResizeColumns = false;
            this.AvailablePlaylistsGrid.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.AvailablePlaylistsGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.AvailablePlaylistsGrid.ColumnHeadersHeight = 26;
            this.AvailablePlaylistsGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Playlist_Name});
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.AvailablePlaylistsGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.AvailablePlaylistsGrid.Location = new System.Drawing.Point(18, 12);
            this.AvailablePlaylistsGrid.MultiSelect = false;
            this.AvailablePlaylistsGrid.Name = "AvailablePlaylistsGrid";
            this.AvailablePlaylistsGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.AvailablePlaylistsGrid.Size = new System.Drawing.Size(362, 262);
            this.AvailablePlaylistsGrid.TabIndex = 0;
            // 
            // TakeNextTopicBtn
            // 
            this.TakeNextTopicBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TakeNextTopicBtn.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.Symbols_Play_32xLG;
            this.TakeNextTopicBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TakeNextTopicBtn.Location = new System.Drawing.Point(1056, 11);
            this.TakeNextTopicBtn.Name = "TakeNextTopicBtn";
            this.TakeNextTopicBtn.Size = new System.Drawing.Size(194, 63);
            this.TakeNextTopicBtn.TabIndex = 7;
            this.TakeNextTopicBtn.Text = "Take Next (F5)";
            this.TakeNextTopicBtn.UseVisualStyleBackColor = true;
            // 
            // CancelBtn
            // 
            this.CancelBtn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.CancelBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CancelBtn.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.StatusAnnotations_Critical_16xLG;
            this.CancelBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.CancelBtn.Location = new System.Drawing.Point(220, 328);
            this.CancelBtn.Name = "CancelBtn";
            this.CancelBtn.Size = new System.Drawing.Size(194, 35);
            this.CancelBtn.TabIndex = 24;
            this.CancelBtn.Text = "Cancel";
            this.CancelBtn.UseVisualStyleBackColor = true;
            // 
            // LoadBtn
            // 
            this.LoadBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoadBtn.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.StatusAnnotations_Complete_and_ok_16xLG;
            this.LoadBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.LoadBtn.Location = new System.Drawing.Point(14, 328);
            this.LoadBtn.Name = "LoadBtn";
            this.LoadBtn.Size = new System.Drawing.Size(194, 35);
            this.LoadBtn.TabIndex = 23;
            this.LoadBtn.Text = "Load";
            this.LoadBtn.UseVisualStyleBackColor = true;
            this.LoadBtn.Click += new System.EventHandler(this.LoadBtn_Click);
            // 
            // Playlist_Name
            // 
            this.Playlist_Name.HeaderText = "Playlist Name";
            this.Playlist_Name.Name = "Playlist_Name";
            this.Playlist_Name.ReadOnly = true;
            this.Playlist_Name.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Playlist_Name.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Playlist_Name.Width = 300;
            // 
            // PlaylistLoad
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(425, 372);
            this.ControlBox = false;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TopicRundownPnl);
            this.Controls.Add(this.CancelBtn);
            this.Controls.Add(this.LoadBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "PlaylistLoad";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PlaylistLoad";
            this.TopicRundownPnl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.AvailablePlaylistsGrid)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel TopicRundownPnl;
        private System.Windows.Forms.Button TakeSelectedTopicBtn;
        private System.Windows.Forms.Button MoveTopicDownBtn;
        private System.Windows.Forms.Button MoveTopicUpBtn;
        private System.Windows.Forms.Button CreateNewTopicBtn;
        private System.Windows.Forms.Button EditSelectedTopicBtn;
        private System.Windows.Forms.DataGridView AvailablePlaylistsGrid;
        private System.Windows.Forms.Button TakeNextTopicBtn;
        private System.Windows.Forms.Button CancelBtn;
        private System.Windows.Forms.Button LoadBtn;
        private System.Windows.Forms.DataGridViewTextBoxColumn Playlist_Name;
    }
}