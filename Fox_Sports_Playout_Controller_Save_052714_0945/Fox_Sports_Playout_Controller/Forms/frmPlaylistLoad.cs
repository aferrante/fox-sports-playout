﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fox_Sports_Playout_Controller
{
    public partial class frmPlaylistLoad : Form
    {
        public frmPlaylistLoad()
        {
            InitializeComponent();

            //Fill with dummy data
            AvailablePlaylistsGrid.RowCount = 2;
            DataGridViewRow row1 = AvailablePlaylistsGrid.Rows[0];
            row1.Cells["Playlist_Name"].Value = "Fox Sports Live 10:00 PM Special";
            DataGridViewRow row2 = AvailablePlaylistsGrid.Rows[1];
            row2.Cells["Playlist_Name"].Value = "Fox Sports Live 11:00 PM Special";

        }

        private void LoadBtn_Click(object sender, EventArgs e)
        {

        }
    }
}
