﻿// Definition for class used for all rundown collection operations
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FoxSports.WingController.Data.DataAccess;
using FoxSports.WingController.Data.DataModel;
using System.Data.SqlClient;
using System.ComponentModel;

namespace FoxSports.WingController.Logic.RundownCollection
{
    /// <summary>
    /// Class for operations related to the topic rundown
    /// </summary>
    public class TopicRundownCollection
    {
        #region Properties and Members
        public BindingList<TopicRundownElements> TopicRundownElementsCollection;
        public string WingDBConnectionString { get; set; }
        public double RundownID { get; set; }
        public string RundownName { get; set; }
        #endregion

        #region Public Methods
        /// <summary>
        /// Get the topic list from the SQL DB; clears out existing collection first
        /// </summary>
        public BindingList<TopicRundownElements> GetTopicRundownCollection(double topicRundownID)
        {
            try
            {
                TopicRundownAccess.WingDBConnectionString = WingDBConnectionString;
                TopicRundownElementsCollection = TopicRundownAccess.GetTopicRundownCollection(topicRundownID);
                if (TopicRundownElementsCollection.Count > 0)
                {
                    RundownName = TopicRundownAccess.RundownName;
                    RundownID = TopicRundownAccess.RundownID;
                }
                else
                {
                    RundownName = "N/A";
                    RundownID = -1;
                }
            }
            catch
            {
                throw;
            }
            // Return 
            return TopicRundownElementsCollection;
        }

        /// <summary>
        /// Save the topic list to the SQL DB; clears out existing collection first
        /// </summary>
        public void SaveTopicRundownCollection(BindingList<TopicRundownElements> topicRundownElements)
        {                      
            if (topicRundownElements.Count > 0)
            {
                TopicRundownAccess.SaveTopicRundownCollection(topicRundownElements); 
            }
        }

        /// <summary>
        /// Append an element to the collection
        /// </summary>
        public void AppendTopicRundownElement(TopicRundownElements topicRundownElement)
        {
            TopicRundownElementsCollection.Add(topicRundownElement);
        }

        /// <summary>
        /// Insert an element into the collection at the specified location
        /// </summary>
        public void AppendTopicRundownElement(Int16 insertPoint, TopicRundownElements topicRundownElement)
        {
            TopicRundownElementsCollection.Insert(insertPoint, topicRundownElement);
        }

        /// <summary>
        /// Delete an element from the collection at the specified location
        /// </summary>
        public void DeleteTopicRundownElement(Int16 deletePoint)
        {
            if (TopicRundownElementsCollection.Count > 0 && deletePoint <= (TopicRundownElementsCollection.Count - 1))
            {
                TopicRundownElementsCollection.RemoveAt(deletePoint);
            }
        }

        /// <summary>
        /// Move the specified element down in the list
        /// </summary>
        public void MoveTopicRundownElementDown(Int16 itemIndex)
        {
            if (itemIndex < TopicRundownElementsCollection.Count - 1)
            {
                var item = TopicRundownElementsCollection[itemIndex];
                TopicRundownElementsCollection.RemoveAt(itemIndex);
                TopicRundownElementsCollection.Insert(itemIndex + 1, item);
            }
        }

        /// <summary>
        /// Move the specified element up in the list
        /// </summary>
        public void MoveTopicRundownElementUp(Int16 itemIndex)
        {
            if (itemIndex > 0)
            {
                var item = TopicRundownElementsCollection[itemIndex];
                TopicRundownElementsCollection.RemoveAt(itemIndex);
                TopicRundownElementsCollection.Insert(itemIndex - 1, item);
            }
        }

        /// <summary>
        /// Enable the specified element
        /// </summary>
        public void EnableRundownElement(Int16 itemIndex)
        {
            var item = TopicRundownElementsCollection[itemIndex];
            item.enabled = true;
        }

        /// <summary>
        /// Disable the specified element
        /// </summary>
        public void DisableRundownElement(Int16 itemIndex)
        {
            var item = TopicRundownElementsCollection[itemIndex];
            item.enabled = false;
        }
        #endregion
    }
}
