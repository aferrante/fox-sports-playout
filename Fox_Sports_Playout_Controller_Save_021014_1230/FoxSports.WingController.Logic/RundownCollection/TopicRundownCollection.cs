﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FoxSports.WingController.Data.DataAccess;
using FoxSports.WingController.Data.DataModel;
using System.Data.SqlClient;

namespace FoxSports.WingController.Logic.RundownCollection
{
    public class TopicRundownCollection
    {
        public TopicRundownCollection()
        {
        }

        public string WingDBConnectionString { get; set; }

        public List<TopicRundownElementModel> GetTopicRundownCollection(double topicRundownID)
        {
            var topicRundownCollection = new List<TopicRundownElementModel>();
            DataTable dataTable = new DataTable();

            try
            {
                TopicRundownAccess topicRundown = new TopicRundownAccess();
                topicRundown.WingDBConnectionString = WingDBConnectionString;
                dataTable = topicRundown.GetTopicRundown(topicRundownID);

                foreach (DataRow row in dataTable.Rows)
                {
                    var newTopic = new TopicRundownElementModel()
                    {
                        enabled = Convert.ToBoolean(row["Enabled"]),
                        rundownid = Convert.ToDouble(row["RundownId"]),
                        pageid = Convert.ToInt32(row["PageId"]),
                        templateId = Convert.ToInt32(row["TemplateId"]),
                        slug = row["Slug"].ToString(),
                        longSlug = row["longSlug"].ToString()+ "Test"
                    };
                    topicRundownCollection.Add(newTopic);
                }

            }
            catch
            {
                throw;
            }
            return topicRundownCollection;
        }
    }
}
