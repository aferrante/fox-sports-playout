﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPoco;

namespace FoxSports.WingController.Data.DataModel
{
    /// <summary>
    /// Class definition for topic rundown element collection
    /// </summary>
    [TableName("TopicRundowns")]
    [PrimaryKey("RundownId")]
    public class TopicRundowns
    {
        public double RundownId { get; set; } //(float, not null)
        public string RundownName { get; set; } //(nvarchar(50), null)
        public DateTime LastUpdated { get; set; } //(datetime, null)
    }
}
