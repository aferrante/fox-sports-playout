﻿namespace Fox_Sports_Playout_Controller
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.TopicRundownPnl = new System.Windows.Forms.Panel();
            this.btnLoadTopicPlaylist = new System.Windows.Forms.Button();
            this.btnSaveTopicPlaylist = new System.Windows.Forms.Button();
            this.iNewsMonitoringEnableCheckbox = new System.Windows.Forms.CheckBox();
            this.btnTakeSelectedTopic = new System.Windows.Forms.Button();
            this.btnMoveTopicDown = new System.Windows.Forms.Button();
            this.btnMoveTopicUp = new System.Windows.Forms.Button();
            this.btnCreateNewTopic = new System.Windows.Forms.Button();
            this.btnEditSelectedTopic = new System.Windows.Forms.Button();
            this.TopicRundownGrid = new System.Windows.Forms.DataGridView();
            this.Topic_Enabled = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Topic_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Topic_Slug = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Topic_LongSlug = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wingDBConnectionStringDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.topicRundownNameDataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStripTopicRundown = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteSelectedIRundownItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.enableSelectedRundownItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disableSelectedRundownItem = new System.Windows.Forms.ToolStripMenuItem();
            this.topicRundownCollectionBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.btnTakeNextTopic = new System.Windows.Forms.Button();
            this.TopicRundownContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.EnableSelectedEntry = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.DeleteSelectedEntry = new System.Windows.Forms.ToolStripMenuItem();
            this.WidgetRundownPnl = new System.Windows.Forms.Panel();
            this.btnLoadWidgetPlaylist = new System.Windows.Forms.Button();
            this.btnSaveWidgetPlaylist = new System.Windows.Forms.Button();
            this.btnMoveWidgetDown = new System.Windows.Forms.Button();
            this.btnMoveWidgetUp = new System.Windows.Forms.Button();
            this.btnCreateNewWidget = new System.Windows.Forms.Button();
            this.btnEditSelectedWidget = new System.Windows.Forms.Button();
            this.WidgetRundownGrid = new System.Windows.Forms.DataGridView();
            this.Widget_Enabled = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Widget_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Widget_TemplateID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Widget_Data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Widget_SupplementalData = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wingDBConnectionStringDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.topicRundownNameDataGridViewTextBoxColumn = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.contextMenuStripWidgetRundown = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.deleteSelectedWidgetItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.enableSelectedWidgetItem = new System.Windows.Forms.ToolStripMenuItem();
            this.disableSelectedWidgetItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnTakeSelectedWidget = new System.Windows.Forms.Button();
            this.btnTakeNextWidget = new System.Windows.Forms.Button();
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.programToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generalPreferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vizEngineSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.StatusPnl = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.vizConnectLED = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.lblTimeOfDay = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.ShowLookNameLabel = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lblTopicRundownName = new System.Windows.Forms.Label();
            this.MainWingControlPanel = new System.Windows.Forms.Panel();
            this.btnWingIn = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.statusBar = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.label11 = new System.Windows.Forms.Label();
            this.lblTopicRundownCount = new System.Windows.Forms.Label();
            this.lblWidgetRundownCount = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.TimeOfDayTimer = new System.Windows.Forms.Timer(this.components);
            this.lblWidgetRundownName = new System.Windows.Forms.Label();
            this.TopicRundownPnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TopicRundownGrid)).BeginInit();
            this.contextMenuStripTopicRundown.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.topicRundownCollectionBindingSource)).BeginInit();
            this.TopicRundownContextMenu.SuspendLayout();
            this.WidgetRundownPnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WidgetRundownGrid)).BeginInit();
            this.contextMenuStripWidgetRundown.SuspendLayout();
            this.MainMenu.SuspendLayout();
            this.StatusPnl.SuspendLayout();
            this.MainWingControlPanel.SuspendLayout();
            this.statusBar.SuspendLayout();
            this.SuspendLayout();
            // 
            // TopicRundownPnl
            // 
            this.TopicRundownPnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TopicRundownPnl.Controls.Add(this.btnLoadTopicPlaylist);
            this.TopicRundownPnl.Controls.Add(this.btnSaveTopicPlaylist);
            this.TopicRundownPnl.Controls.Add(this.iNewsMonitoringEnableCheckbox);
            this.TopicRundownPnl.Controls.Add(this.btnTakeSelectedTopic);
            this.TopicRundownPnl.Controls.Add(this.btnMoveTopicDown);
            this.TopicRundownPnl.Controls.Add(this.btnMoveTopicUp);
            this.TopicRundownPnl.Controls.Add(this.btnCreateNewTopic);
            this.TopicRundownPnl.Controls.Add(this.btnEditSelectedTopic);
            this.TopicRundownPnl.Controls.Add(this.TopicRundownGrid);
            this.TopicRundownPnl.Controls.Add(this.btnTakeNextTopic);
            this.TopicRundownPnl.Location = new System.Drawing.Point(43, 168);
            this.TopicRundownPnl.Name = "TopicRundownPnl";
            this.TopicRundownPnl.Size = new System.Drawing.Size(1281, 290);
            this.TopicRundownPnl.TabIndex = 0;
            // 
            // btnLoadTopicPlaylist
            // 
            this.btnLoadTopicPlaylist.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadTopicPlaylist.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.folder_Open_16xLG;
            this.btnLoadTopicPlaylist.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLoadTopicPlaylist.Location = new System.Drawing.Point(833, 245);
            this.btnLoadTopicPlaylist.Name = "btnLoadTopicPlaylist";
            this.btnLoadTopicPlaylist.Size = new System.Drawing.Size(194, 35);
            this.btnLoadTopicPlaylist.TabIndex = 15;
            this.btnLoadTopicPlaylist.Text = "Load Playlist";
            this.btnLoadTopicPlaylist.UseVisualStyleBackColor = true;
            this.btnLoadTopicPlaylist.Click += new System.EventHandler(this.LoadTopicPlaylistBtn_Click);
            // 
            // btnSaveTopicPlaylist
            // 
            this.btnSaveTopicPlaylist.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveTopicPlaylist.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.save_16xLG;
            this.btnSaveTopicPlaylist.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSaveTopicPlaylist.Location = new System.Drawing.Point(610, 245);
            this.btnSaveTopicPlaylist.Name = "btnSaveTopicPlaylist";
            this.btnSaveTopicPlaylist.Size = new System.Drawing.Size(194, 35);
            this.btnSaveTopicPlaylist.TabIndex = 14;
            this.btnSaveTopicPlaylist.Text = "Save Playlist";
            this.btnSaveTopicPlaylist.UseVisualStyleBackColor = true;
            this.btnSaveTopicPlaylist.Click += new System.EventHandler(this.btnSaveTopicPlaylist_Click);
            // 
            // iNewsMonitoringEnableCheckbox
            // 
            this.iNewsMonitoringEnableCheckbox.AutoSize = true;
            this.iNewsMonitoringEnableCheckbox.BackColor = System.Drawing.SystemColors.Control;
            this.iNewsMonitoringEnableCheckbox.Checked = true;
            this.iNewsMonitoringEnableCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.iNewsMonitoringEnableCheckbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iNewsMonitoringEnableCheckbox.Location = new System.Drawing.Point(12, 251);
            this.iNewsMonitoringEnableCheckbox.Name = "iNewsMonitoringEnableCheckbox";
            this.iNewsMonitoringEnableCheckbox.Size = new System.Drawing.Size(235, 24);
            this.iNewsMonitoringEnableCheckbox.TabIndex = 13;
            this.iNewsMonitoringEnableCheckbox.Text = "iNews Monitoring Enabled";
            this.iNewsMonitoringEnableCheckbox.UseVisualStyleBackColor = false;
            this.iNewsMonitoringEnableCheckbox.CheckStateChanged += new System.EventHandler(this.iNewsMonitoringEnableCheckbox_CheckStateChanged);
            // 
            // btnTakeSelectedTopic
            // 
            this.btnTakeSelectedTopic.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTakeSelectedTopic.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.Symbols_Play_32xLG;
            this.btnTakeSelectedTopic.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnTakeSelectedTopic.Location = new System.Drawing.Point(1056, 82);
            this.btnTakeSelectedTopic.Name = "btnTakeSelectedTopic";
            this.btnTakeSelectedTopic.Size = new System.Drawing.Size(194, 63);
            this.btnTakeSelectedTopic.TabIndex = 8;
            this.btnTakeSelectedTopic.Text = "Take Selected";
            this.btnTakeSelectedTopic.UseVisualStyleBackColor = true;
            // 
            // btnMoveTopicDown
            // 
            this.btnMoveTopicDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveTopicDown.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.arrow_Down_16xLG;
            this.btnMoveTopicDown.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMoveTopicDown.Location = new System.Drawing.Point(1158, 155);
            this.btnMoveTopicDown.Name = "btnMoveTopicDown";
            this.btnMoveTopicDown.Size = new System.Drawing.Size(92, 35);
            this.btnMoveTopicDown.TabIndex = 11;
            this.btnMoveTopicDown.Text = "Move";
            this.btnMoveTopicDown.UseVisualStyleBackColor = true;
            this.btnMoveTopicDown.Click += new System.EventHandler(this.btnMoveTopicDown_Click);
            // 
            // btnMoveTopicUp
            // 
            this.btnMoveTopicUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveTopicUp.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.arrow_Up_16xLG;
            this.btnMoveTopicUp.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMoveTopicUp.Location = new System.Drawing.Point(1056, 155);
            this.btnMoveTopicUp.Name = "btnMoveTopicUp";
            this.btnMoveTopicUp.Size = new System.Drawing.Size(92, 35);
            this.btnMoveTopicUp.TabIndex = 10;
            this.btnMoveTopicUp.Text = "Move";
            this.btnMoveTopicUp.UseVisualStyleBackColor = true;
            this.btnMoveTopicUp.Click += new System.EventHandler(this.btnMoveTopicUp_Click);
            // 
            // btnCreateNewTopic
            // 
            this.btnCreateNewTopic.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateNewTopic.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.action_add_16xLG;
            this.btnCreateNewTopic.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCreateNewTopic.Location = new System.Drawing.Point(1056, 245);
            this.btnCreateNewTopic.Name = "btnCreateNewTopic";
            this.btnCreateNewTopic.Size = new System.Drawing.Size(194, 35);
            this.btnCreateNewTopic.TabIndex = 9;
            this.btnCreateNewTopic.Text = "Create New";
            this.btnCreateNewTopic.UseVisualStyleBackColor = true;
            this.btnCreateNewTopic.Click += new System.EventHandler(this.CreateNewTopicBtn_Click);
            // 
            // btnEditSelectedTopic
            // 
            this.btnEditSelectedTopic.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditSelectedTopic.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.dialog_32xLG;
            this.btnEditSelectedTopic.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEditSelectedTopic.Location = new System.Drawing.Point(1056, 200);
            this.btnEditSelectedTopic.Name = "btnEditSelectedTopic";
            this.btnEditSelectedTopic.Size = new System.Drawing.Size(194, 35);
            this.btnEditSelectedTopic.TabIndex = 8;
            this.btnEditSelectedTopic.Text = "Edit Selected";
            this.btnEditSelectedTopic.UseVisualStyleBackColor = true;
            // 
            // TopicRundownGrid
            // 
            this.TopicRundownGrid.AllowUserToAddRows = false;
            this.TopicRundownGrid.AllowUserToDeleteRows = false;
            this.TopicRundownGrid.AllowUserToResizeRows = false;
            this.TopicRundownGrid.AutoGenerateColumns = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TopicRundownGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.TopicRundownGrid.ColumnHeadersHeight = 26;
            this.TopicRundownGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Topic_Enabled,
            this.Topic_ID,
            this.Topic_Slug,
            this.Topic_LongSlug,
            this.wingDBConnectionStringDataGridViewTextBoxColumn1,
            this.topicRundownNameDataGridViewTextBoxColumn1});
            this.TopicRundownGrid.ContextMenuStrip = this.contextMenuStripTopicRundown;
            this.TopicRundownGrid.DataSource = this.topicRundownCollectionBindingSource;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.TopicRundownGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.TopicRundownGrid.Location = new System.Drawing.Point(12, 13);
            this.TopicRundownGrid.MultiSelect = false;
            this.TopicRundownGrid.Name = "TopicRundownGrid";
            this.TopicRundownGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TopicRundownGrid.Size = new System.Drawing.Size(1015, 222);
            this.TopicRundownGrid.TabIndex = 0;
            // 
            // Topic_Enabled
            // 
            this.Topic_Enabled.DataPropertyName = "enabled";
            this.Topic_Enabled.HeaderText = "Enabled";
            this.Topic_Enabled.Name = "Topic_Enabled";
            this.Topic_Enabled.Width = 70;
            // 
            // Topic_ID
            // 
            this.Topic_ID.DataPropertyName = "pageId";
            this.Topic_ID.HeaderText = "Item ID";
            this.Topic_ID.Name = "Topic_ID";
            this.Topic_ID.ReadOnly = true;
            this.Topic_ID.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Topic_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Topic_Slug
            // 
            this.Topic_Slug.DataPropertyName = "slug";
            this.Topic_Slug.HeaderText = "Slug";
            this.Topic_Slug.Name = "Topic_Slug";
            this.Topic_Slug.ReadOnly = true;
            this.Topic_Slug.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Topic_Slug.Width = 250;
            // 
            // Topic_LongSlug
            // 
            this.Topic_LongSlug.DataPropertyName = "longSlug";
            this.Topic_LongSlug.HeaderText = "Long Slug";
            this.Topic_LongSlug.Name = "Topic_LongSlug";
            this.Topic_LongSlug.ReadOnly = true;
            this.Topic_LongSlug.Width = 540;
            // 
            // wingDBConnectionStringDataGridViewTextBoxColumn1
            // 
            this.wingDBConnectionStringDataGridViewTextBoxColumn1.DataPropertyName = "WingDBConnectionString";
            this.wingDBConnectionStringDataGridViewTextBoxColumn1.HeaderText = "WingDBConnectionString";
            this.wingDBConnectionStringDataGridViewTextBoxColumn1.Name = "wingDBConnectionStringDataGridViewTextBoxColumn1";
            // 
            // topicRundownNameDataGridViewTextBoxColumn1
            // 
            this.topicRundownNameDataGridViewTextBoxColumn1.DataPropertyName = "TopicRundownName";
            this.topicRundownNameDataGridViewTextBoxColumn1.HeaderText = "TopicRundownName";
            this.topicRundownNameDataGridViewTextBoxColumn1.Name = "topicRundownNameDataGridViewTextBoxColumn1";
            // 
            // contextMenuStripTopicRundown
            // 
            this.contextMenuStripTopicRundown.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteSelectedIRundownItem,
            this.toolStripMenuItem1,
            this.enableSelectedRundownItem,
            this.disableSelectedRundownItem});
            this.contextMenuStripTopicRundown.Name = "contextMenuStripTopicRundown";
            this.contextMenuStripTopicRundown.Size = new System.Drawing.Size(187, 76);
            // 
            // deleteSelectedIRundownItem
            // 
            this.deleteSelectedIRundownItem.Name = "deleteSelectedIRundownItem";
            this.deleteSelectedIRundownItem.Size = new System.Drawing.Size(186, 22);
            this.deleteSelectedIRundownItem.Text = "Delete Selected Item";
            this.deleteSelectedIRundownItem.Click += new System.EventHandler(this.deleteSelectedItemToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(183, 6);
            // 
            // enableSelectedRundownItem
            // 
            this.enableSelectedRundownItem.Name = "enableSelectedRundownItem";
            this.enableSelectedRundownItem.Size = new System.Drawing.Size(186, 22);
            this.enableSelectedRundownItem.Text = "Enable Selected Item";
            this.enableSelectedRundownItem.Click += new System.EventHandler(this.enableSelectedItemToolStripMenuItem_Click);
            // 
            // disableSelectedRundownItem
            // 
            this.disableSelectedRundownItem.Name = "disableSelectedRundownItem";
            this.disableSelectedRundownItem.Size = new System.Drawing.Size(186, 22);
            this.disableSelectedRundownItem.Text = "Disable Selected Item";
            this.disableSelectedRundownItem.Click += new System.EventHandler(this.disableSelectedItemToolStripMenuItem_Click);
            // 
            // topicRundownCollectionBindingSource
            // 
            this.topicRundownCollectionBindingSource.DataSource = typeof(FoxSports.WingController.Logic.RundownCollection.TopicRundownCollection);
            // 
            // btnTakeNextTopic
            // 
            this.btnTakeNextTopic.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTakeNextTopic.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.Symbols_Play_32xLG;
            this.btnTakeNextTopic.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnTakeNextTopic.Location = new System.Drawing.Point(1056, 11);
            this.btnTakeNextTopic.Name = "btnTakeNextTopic";
            this.btnTakeNextTopic.Size = new System.Drawing.Size(194, 63);
            this.btnTakeNextTopic.TabIndex = 7;
            this.btnTakeNextTopic.Text = "Take Next (F5)";
            this.btnTakeNextTopic.UseVisualStyleBackColor = true;
            this.btnTakeNextTopic.Click += new System.EventHandler(this.btnTakeNextTopic_Click);
            // 
            // TopicRundownContextMenu
            // 
            this.TopicRundownContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.EnableSelectedEntry,
            this.toolStripSeparator1,
            this.DeleteSelectedEntry});
            this.TopicRundownContextMenu.Name = "TopicRundownContextMenu";
            this.TopicRundownContextMenu.Size = new System.Drawing.Size(187, 54);
            // 
            // EnableSelectedEntry
            // 
            this.EnableSelectedEntry.Name = "EnableSelectedEntry";
            this.EnableSelectedEntry.Size = new System.Drawing.Size(186, 22);
            this.EnableSelectedEntry.Text = "Enable Selected Entry";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(183, 6);
            // 
            // DeleteSelectedEntry
            // 
            this.DeleteSelectedEntry.Name = "DeleteSelectedEntry";
            this.DeleteSelectedEntry.Size = new System.Drawing.Size(186, 22);
            this.DeleteSelectedEntry.Text = "Delete Selected Entry";
            // 
            // WidgetRundownPnl
            // 
            this.WidgetRundownPnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.WidgetRundownPnl.Controls.Add(this.btnLoadWidgetPlaylist);
            this.WidgetRundownPnl.Controls.Add(this.btnSaveWidgetPlaylist);
            this.WidgetRundownPnl.Controls.Add(this.btnMoveWidgetDown);
            this.WidgetRundownPnl.Controls.Add(this.btnMoveWidgetUp);
            this.WidgetRundownPnl.Controls.Add(this.btnCreateNewWidget);
            this.WidgetRundownPnl.Controls.Add(this.btnEditSelectedWidget);
            this.WidgetRundownPnl.Controls.Add(this.WidgetRundownGrid);
            this.WidgetRundownPnl.Controls.Add(this.btnTakeSelectedWidget);
            this.WidgetRundownPnl.Controls.Add(this.btnTakeNextWidget);
            this.WidgetRundownPnl.Location = new System.Drawing.Point(43, 484);
            this.WidgetRundownPnl.Name = "WidgetRundownPnl";
            this.WidgetRundownPnl.Size = new System.Drawing.Size(1281, 290);
            this.WidgetRundownPnl.TabIndex = 1;
            // 
            // btnLoadWidgetPlaylist
            // 
            this.btnLoadWidgetPlaylist.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLoadWidgetPlaylist.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.folder_Open_16xLG;
            this.btnLoadWidgetPlaylist.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnLoadWidgetPlaylist.Location = new System.Drawing.Point(833, 245);
            this.btnLoadWidgetPlaylist.Name = "btnLoadWidgetPlaylist";
            this.btnLoadWidgetPlaylist.Size = new System.Drawing.Size(194, 35);
            this.btnLoadWidgetPlaylist.TabIndex = 17;
            this.btnLoadWidgetPlaylist.Text = "Load Playlist";
            this.btnLoadWidgetPlaylist.UseVisualStyleBackColor = true;
            // 
            // btnSaveWidgetPlaylist
            // 
            this.btnSaveWidgetPlaylist.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveWidgetPlaylist.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.save_16xLG;
            this.btnSaveWidgetPlaylist.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSaveWidgetPlaylist.Location = new System.Drawing.Point(610, 245);
            this.btnSaveWidgetPlaylist.Name = "btnSaveWidgetPlaylist";
            this.btnSaveWidgetPlaylist.Size = new System.Drawing.Size(194, 35);
            this.btnSaveWidgetPlaylist.TabIndex = 16;
            this.btnSaveWidgetPlaylist.Text = "Save Playlist";
            this.btnSaveWidgetPlaylist.UseVisualStyleBackColor = true;
            this.btnSaveWidgetPlaylist.Click += new System.EventHandler(this.btnSaveWidgetPlaylist_Click);
            // 
            // btnMoveWidgetDown
            // 
            this.btnMoveWidgetDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveWidgetDown.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.arrow_Down_16xLG;
            this.btnMoveWidgetDown.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMoveWidgetDown.Location = new System.Drawing.Point(1158, 155);
            this.btnMoveWidgetDown.Name = "btnMoveWidgetDown";
            this.btnMoveWidgetDown.Size = new System.Drawing.Size(92, 35);
            this.btnMoveWidgetDown.TabIndex = 15;
            this.btnMoveWidgetDown.Text = "Move";
            this.btnMoveWidgetDown.UseVisualStyleBackColor = true;
            this.btnMoveWidgetDown.Click += new System.EventHandler(this.btnMoveWidgetDown_Click);
            // 
            // btnMoveWidgetUp
            // 
            this.btnMoveWidgetUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMoveWidgetUp.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.arrow_Up_16xLG;
            this.btnMoveWidgetUp.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnMoveWidgetUp.Location = new System.Drawing.Point(1056, 155);
            this.btnMoveWidgetUp.Name = "btnMoveWidgetUp";
            this.btnMoveWidgetUp.Size = new System.Drawing.Size(92, 35);
            this.btnMoveWidgetUp.TabIndex = 14;
            this.btnMoveWidgetUp.Text = "Move";
            this.btnMoveWidgetUp.UseVisualStyleBackColor = true;
            this.btnMoveWidgetUp.Click += new System.EventHandler(this.btnMoveWidgetUp_Click);
            // 
            // btnCreateNewWidget
            // 
            this.btnCreateNewWidget.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCreateNewWidget.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.action_add_16xLG;
            this.btnCreateNewWidget.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCreateNewWidget.Location = new System.Drawing.Point(1056, 245);
            this.btnCreateNewWidget.Name = "btnCreateNewWidget";
            this.btnCreateNewWidget.Size = new System.Drawing.Size(194, 35);
            this.btnCreateNewWidget.TabIndex = 13;
            this.btnCreateNewWidget.Text = "Create New";
            this.btnCreateNewWidget.UseVisualStyleBackColor = true;
            // 
            // btnEditSelectedWidget
            // 
            this.btnEditSelectedWidget.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEditSelectedWidget.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.dialog_32xLG;
            this.btnEditSelectedWidget.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnEditSelectedWidget.Location = new System.Drawing.Point(1056, 200);
            this.btnEditSelectedWidget.Name = "btnEditSelectedWidget";
            this.btnEditSelectedWidget.Size = new System.Drawing.Size(194, 35);
            this.btnEditSelectedWidget.TabIndex = 12;
            this.btnEditSelectedWidget.Text = "Edit Selected";
            this.btnEditSelectedWidget.UseVisualStyleBackColor = true;
            // 
            // WidgetRundownGrid
            // 
            this.WidgetRundownGrid.AllowUserToAddRows = false;
            this.WidgetRundownGrid.AllowUserToDeleteRows = false;
            this.WidgetRundownGrid.AllowUserToOrderColumns = true;
            this.WidgetRundownGrid.AllowUserToResizeColumns = false;
            this.WidgetRundownGrid.AllowUserToResizeRows = false;
            this.WidgetRundownGrid.AutoGenerateColumns = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.WidgetRundownGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.WidgetRundownGrid.ColumnHeadersHeight = 26;
            this.WidgetRundownGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Widget_Enabled,
            this.Widget_ID,
            this.Widget_TemplateID,
            this.Widget_Data,
            this.Widget_SupplementalData,
            this.wingDBConnectionStringDataGridViewTextBoxColumn,
            this.topicRundownNameDataGridViewTextBoxColumn});
            this.WidgetRundownGrid.ContextMenuStrip = this.contextMenuStripWidgetRundown;
            this.WidgetRundownGrid.DataSource = this.topicRundownCollectionBindingSource;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.WidgetRundownGrid.DefaultCellStyle = dataGridViewCellStyle4;
            this.WidgetRundownGrid.Location = new System.Drawing.Point(12, 13);
            this.WidgetRundownGrid.MultiSelect = false;
            this.WidgetRundownGrid.Name = "WidgetRundownGrid";
            this.WidgetRundownGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.WidgetRundownGrid.Size = new System.Drawing.Size(1015, 222);
            this.WidgetRundownGrid.TabIndex = 11;
            // 
            // Widget_Enabled
            // 
            this.Widget_Enabled.DataPropertyName = "enabled";
            this.Widget_Enabled.HeaderText = "Enabled";
            this.Widget_Enabled.Name = "Widget_Enabled";
            this.Widget_Enabled.Width = 70;
            // 
            // Widget_ID
            // 
            this.Widget_ID.DataPropertyName = "pageID";
            this.Widget_ID.HeaderText = "ID";
            this.Widget_ID.Name = "Widget_ID";
            this.Widget_ID.ReadOnly = true;
            this.Widget_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Widget_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Widget_TemplateID
            // 
            this.Widget_TemplateID.DataPropertyName = "templateId";
            this.Widget_TemplateID.HeaderText = "Template";
            this.Widget_TemplateID.Name = "Widget_TemplateID";
            this.Widget_TemplateID.ReadOnly = true;
            this.Widget_TemplateID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Widget_TemplateID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Widget_TemplateID.Width = 200;
            // 
            // Widget_Data
            // 
            this.Widget_Data.DataPropertyName = "titleText_1";
            this.Widget_Data.HeaderText = "Widget Title 1";
            this.Widget_Data.Name = "Widget_Data";
            this.Widget_Data.ReadOnly = true;
            this.Widget_Data.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Widget_Data.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Widget_Data.Width = 200;
            // 
            // Widget_SupplementalData
            // 
            this.Widget_SupplementalData.DataPropertyName = "titleText_2";
            this.Widget_SupplementalData.HeaderText = "Widget Title 2";
            this.Widget_SupplementalData.Name = "Widget_SupplementalData";
            this.Widget_SupplementalData.ReadOnly = true;
            this.Widget_SupplementalData.Width = 400;
            // 
            // wingDBConnectionStringDataGridViewTextBoxColumn
            // 
            this.wingDBConnectionStringDataGridViewTextBoxColumn.DataPropertyName = "WingDBConnectionString";
            this.wingDBConnectionStringDataGridViewTextBoxColumn.HeaderText = "WingDBConnectionString";
            this.wingDBConnectionStringDataGridViewTextBoxColumn.Name = "wingDBConnectionStringDataGridViewTextBoxColumn";
            // 
            // topicRundownNameDataGridViewTextBoxColumn
            // 
            this.topicRundownNameDataGridViewTextBoxColumn.DataPropertyName = "TopicRundownName";
            this.topicRundownNameDataGridViewTextBoxColumn.HeaderText = "TopicRundownName";
            this.topicRundownNameDataGridViewTextBoxColumn.Name = "topicRundownNameDataGridViewTextBoxColumn";
            // 
            // contextMenuStripWidgetRundown
            // 
            this.contextMenuStripWidgetRundown.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.deleteSelectedWidgetItem,
            this.toolStripSeparator2,
            this.enableSelectedWidgetItem,
            this.disableSelectedWidgetItem});
            this.contextMenuStripWidgetRundown.Name = "contextMenuStripWidgetRundown";
            this.contextMenuStripWidgetRundown.Size = new System.Drawing.Size(187, 76);
            // 
            // deleteSelectedWidgetItem
            // 
            this.deleteSelectedWidgetItem.Name = "deleteSelectedWidgetItem";
            this.deleteSelectedWidgetItem.Size = new System.Drawing.Size(186, 22);
            this.deleteSelectedWidgetItem.Text = "Delete Selected Item";
            this.deleteSelectedWidgetItem.Click += new System.EventHandler(this.deleteSelectedWidgetItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(183, 6);
            // 
            // enableSelectedWidgetItem
            // 
            this.enableSelectedWidgetItem.Name = "enableSelectedWidgetItem";
            this.enableSelectedWidgetItem.Size = new System.Drawing.Size(186, 22);
            this.enableSelectedWidgetItem.Text = "Enable Selected Item";
            this.enableSelectedWidgetItem.Click += new System.EventHandler(this.enableSelectedWidgetItem_Click);
            // 
            // disableSelectedWidgetItem
            // 
            this.disableSelectedWidgetItem.Name = "disableSelectedWidgetItem";
            this.disableSelectedWidgetItem.Size = new System.Drawing.Size(186, 22);
            this.disableSelectedWidgetItem.Text = "Disable Selected Item";
            this.disableSelectedWidgetItem.Click += new System.EventHandler(this.disableSelectedWidgetItem_Click);
            // 
            // btnTakeSelectedWidget
            // 
            this.btnTakeSelectedWidget.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTakeSelectedWidget.Location = new System.Drawing.Point(1056, 83);
            this.btnTakeSelectedWidget.Name = "btnTakeSelectedWidget";
            this.btnTakeSelectedWidget.Size = new System.Drawing.Size(194, 63);
            this.btnTakeSelectedWidget.TabIndex = 10;
            this.btnTakeSelectedWidget.Text = "Take Selected";
            this.btnTakeSelectedWidget.UseVisualStyleBackColor = true;
            // 
            // btnTakeNextWidget
            // 
            this.btnTakeNextWidget.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTakeNextWidget.Location = new System.Drawing.Point(1056, 11);
            this.btnTakeNextWidget.Name = "btnTakeNextWidget";
            this.btnTakeNextWidget.Size = new System.Drawing.Size(194, 63);
            this.btnTakeNextWidget.TabIndex = 9;
            this.btnTakeNextWidget.Text = "Take Next (F9)";
            this.btnTakeNextWidget.UseVisualStyleBackColor = true;
            // 
            // MainMenu
            // 
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.programToolStripMenuItem,
            this.preferencesToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.ShowItemToolTips = true;
            this.MainMenu.Size = new System.Drawing.Size(1366, 24);
            this.MainMenu.TabIndex = 2;
            this.MainMenu.Text = "MainMenu";
            // 
            // programToolStripMenuItem
            // 
            this.programToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.programToolStripMenuItem.Name = "programToolStripMenuItem";
            this.programToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.programToolStripMenuItem.Text = "&Program";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            // 
            // preferencesToolStripMenuItem
            // 
            this.preferencesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generalPreferencesToolStripMenuItem,
            this.vizEngineSetupToolStripMenuItem});
            this.preferencesToolStripMenuItem.Name = "preferencesToolStripMenuItem";
            this.preferencesToolStripMenuItem.Size = new System.Drawing.Size(80, 20);
            this.preferencesToolStripMenuItem.Text = "&Preferences";
            // 
            // generalPreferencesToolStripMenuItem
            // 
            this.generalPreferencesToolStripMenuItem.Name = "generalPreferencesToolStripMenuItem";
            this.generalPreferencesToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.generalPreferencesToolStripMenuItem.Text = "&General Preferences";
            // 
            // vizEngineSetupToolStripMenuItem
            // 
            this.vizEngineSetupToolStripMenuItem.Name = "vizEngineSetupToolStripMenuItem";
            this.vizEngineSetupToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.vizEngineSetupToolStripMenuItem.Text = "&Viz Engine Setup";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "&About";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(39, 145);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Topic Rundown";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(39, 461);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Widget Rundown";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(38, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "Status";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(-1, -24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Status";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(757, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "Viz Engine";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(757, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(131, 20);
            this.label6.TabIndex = 9;
            this.label6.Text = "iNews Gateway";
            // 
            // StatusPnl
            // 
            this.StatusPnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.StatusPnl.Controls.Add(this.button2);
            this.StatusPnl.Controls.Add(this.button1);
            this.StatusPnl.Controls.Add(this.vizConnectLED);
            this.StatusPnl.Controls.Add(this.label17);
            this.StatusPnl.Controls.Add(this.lblTimeOfDay);
            this.StatusPnl.Controls.Add(this.label15);
            this.StatusPnl.Controls.Add(this.ShowLookNameLabel);
            this.StatusPnl.Controls.Add(this.label10);
            this.StatusPnl.Controls.Add(this.label6);
            this.StatusPnl.Controls.Add(this.label5);
            this.StatusPnl.Controls.Add(this.label3);
            this.StatusPnl.Location = new System.Drawing.Point(42, 48);
            this.StatusPnl.Name = "StatusPnl";
            this.StatusPnl.Size = new System.Drawing.Size(939, 94);
            this.StatusPnl.TabIndex = 5;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.Transparent;
            this.button2.Location = new System.Drawing.Point(724, 61);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(27, 23);
            this.button2.TabIndex = 22;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.Location = new System.Drawing.Point(724, 34);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(27, 23);
            this.button1.TabIndex = 21;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // vizConnectLED
            // 
            this.vizConnectLED.BackColor = System.Drawing.Color.Transparent;
            this.vizConnectLED.Location = new System.Drawing.Point(724, 5);
            this.vizConnectLED.Name = "vizConnectLED";
            this.vizConnectLED.Size = new System.Drawing.Size(27, 23);
            this.vizConnectLED.TabIndex = 20;
            this.vizConnectLED.UseVisualStyleBackColor = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(9, 5);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(161, 20);
            this.label17.TabIndex = 19;
            this.label17.Text = "Current Date/Time:";
            // 
            // lblTimeOfDay
            // 
            this.lblTimeOfDay.BackColor = System.Drawing.Color.Black;
            this.lblTimeOfDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimeOfDay.ForeColor = System.Drawing.Color.Red;
            this.lblTimeOfDay.Location = new System.Drawing.Point(182, 5);
            this.lblTimeOfDay.Name = "lblTimeOfDay";
            this.lblTimeOfDay.Size = new System.Drawing.Size(317, 20);
            this.lblTimeOfDay.TabIndex = 18;
            this.lblTimeOfDay.Text = "01/03/2014 19:20";
            this.lblTimeOfDay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(757, 64);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(127, 20);
            this.label15.TabIndex = 14;
            this.label15.Text = "SQL Database";
            // 
            // ShowLookNameLabel
            // 
            this.ShowLookNameLabel.BackColor = System.Drawing.Color.Black;
            this.ShowLookNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ShowLookNameLabel.ForeColor = System.Drawing.Color.Red;
            this.ShowLookNameLabel.Location = new System.Drawing.Point(182, 38);
            this.ShowLookNameLabel.Name = "ShowLookNameLabel";
            this.ShowLookNameLabel.Size = new System.Drawing.Size(317, 20);
            this.ShowLookNameLabel.TabIndex = 13;
            this.ShowLookNameLabel.Text = "Default";
            this.ShowLookNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(9, 38);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(167, 20);
            this.label10.TabIndex = 12;
            this.label10.Text = "Current Show Look:";
            // 
            // lblTopicRundownName
            // 
            this.lblTopicRundownName.BackColor = System.Drawing.Color.Black;
            this.lblTopicRundownName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTopicRundownName.ForeColor = System.Drawing.Color.Red;
            this.lblTopicRundownName.Location = new System.Drawing.Point(225, 145);
            this.lblTopicRundownName.Name = "lblTopicRundownName";
            this.lblTopicRundownName.Size = new System.Drawing.Size(317, 20);
            this.lblTopicRundownName.TabIndex = 11;
            this.lblTopicRundownName.Text = "Not defined";
            this.lblTopicRundownName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MainWingControlPanel
            // 
            this.MainWingControlPanel.BackColor = System.Drawing.SystemColors.Control;
            this.MainWingControlPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MainWingControlPanel.Controls.Add(this.btnWingIn);
            this.MainWingControlPanel.Controls.Add(this.label9);
            this.MainWingControlPanel.Location = new System.Drawing.Point(987, 49);
            this.MainWingControlPanel.Name = "MainWingControlPanel";
            this.MainWingControlPanel.Size = new System.Drawing.Size(337, 94);
            this.MainWingControlPanel.TabIndex = 8;
            // 
            // btnWingIn
            // 
            this.btnWingIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnWingIn.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.StatusAnnotations_Play_32xLG_color;
            this.btnWingIn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnWingIn.Location = new System.Drawing.Point(26, 11);
            this.btnWingIn.Name = "btnWingIn";
            this.btnWingIn.Size = new System.Drawing.Size(284, 72);
            this.btnWingIn.TabIndex = 8;
            this.btnWingIn.Text = "Take Wing In (F1)";
            this.btnWingIn.UseVisualStyleBackColor = true;
            this.btnWingIn.Click += new System.EventHandler(this.WingInBtn_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(-1, -24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 20);
            this.label9.TabIndex = 6;
            this.label9.Text = "Status";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(984, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(155, 20);
            this.label7.TabIndex = 9;
            this.label7.Text = "Main Wing Control";
            // 
            // statusBar
            // 
            this.statusBar.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
            this.statusBar.Location = new System.Drawing.Point(0, 781);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(1366, 22);
            this.statusBar.TabIndex = 10;
            this.statusBar.Text = "statusStrip1";
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(578, 145);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(159, 20);
            this.label11.TabIndex = 11;
            this.label11.Text = "Number of Entries:";
            // 
            // lblTopicRundownCount
            // 
            this.lblTopicRundownCount.BackColor = System.Drawing.Color.Black;
            this.lblTopicRundownCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTopicRundownCount.ForeColor = System.Drawing.Color.Red;
            this.lblTopicRundownCount.Location = new System.Drawing.Point(743, 145);
            this.lblTopicRundownCount.Name = "lblTopicRundownCount";
            this.lblTopicRundownCount.Size = new System.Drawing.Size(50, 20);
            this.lblTopicRundownCount.TabIndex = 12;
            this.lblTopicRundownCount.Text = "0";
            this.lblTopicRundownCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblWidgetRundownCount
            // 
            this.lblWidgetRundownCount.BackColor = System.Drawing.Color.Black;
            this.lblWidgetRundownCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWidgetRundownCount.ForeColor = System.Drawing.Color.Red;
            this.lblWidgetRundownCount.Location = new System.Drawing.Point(743, 461);
            this.lblWidgetRundownCount.Name = "lblWidgetRundownCount";
            this.lblWidgetRundownCount.Size = new System.Drawing.Size(50, 20);
            this.lblWidgetRundownCount.TabIndex = 14;
            this.lblWidgetRundownCount.Text = "0";
            this.lblWidgetRundownCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(578, 461);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(159, 20);
            this.label14.TabIndex = 13;
            this.label14.Text = "Number of Entries:";
            // 
            // TimeOfDayTimer
            // 
            this.TimeOfDayTimer.Interval = 1000;
            this.TimeOfDayTimer.Tick += new System.EventHandler(this.TimeOfDayTimer_Tick);
            // 
            // lblWidgetRundownName
            // 
            this.lblWidgetRundownName.BackColor = System.Drawing.Color.Black;
            this.lblWidgetRundownName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblWidgetRundownName.ForeColor = System.Drawing.Color.Red;
            this.lblWidgetRundownName.Location = new System.Drawing.Point(225, 461);
            this.lblWidgetRundownName.Name = "lblWidgetRundownName";
            this.lblWidgetRundownName.Size = new System.Drawing.Size(317, 20);
            this.lblWidgetRundownName.TabIndex = 15;
            this.lblWidgetRundownName.Text = "Not defined";
            this.lblWidgetRundownName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1366, 803);
            this.Controls.Add(this.lblWidgetRundownName);
            this.Controls.Add(this.lblWidgetRundownCount);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.lblTopicRundownCount);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.statusBar);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblTopicRundownName);
            this.Controls.Add(this.MainWingControlPanel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.StatusPnl);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.WidgetRundownPnl);
            this.Controls.Add(this.TopicRundownPnl);
            this.Controls.Add(this.MainMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.MainMenu;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fox Sports Wing Playout Controller  V1.0.0  Video Design Software Inc.";
            this.TopicRundownPnl.ResumeLayout(false);
            this.TopicRundownPnl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TopicRundownGrid)).EndInit();
            this.contextMenuStripTopicRundown.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.topicRundownCollectionBindingSource)).EndInit();
            this.TopicRundownContextMenu.ResumeLayout(false);
            this.WidgetRundownPnl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.WidgetRundownGrid)).EndInit();
            this.contextMenuStripWidgetRundown.ResumeLayout(false);
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.StatusPnl.ResumeLayout(false);
            this.StatusPnl.PerformLayout();
            this.MainWingControlPanel.ResumeLayout(false);
            this.MainWingControlPanel.PerformLayout();
            this.statusBar.ResumeLayout(false);
            this.statusBar.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel TopicRundownPnl;
        private System.Windows.Forms.Panel WidgetRundownPnl;
        private System.Windows.Forms.MenuStrip MainMenu;
        private System.Windows.Forms.ToolStripMenuItem programToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preferencesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generalPreferencesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vizEngineSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnTakeNextTopic;
        private System.Windows.Forms.Button btnTakeSelectedTopic;
        private System.Windows.Forms.Button btnTakeSelectedWidget;
        private System.Windows.Forms.Button btnTakeNextWidget;
        private System.Windows.Forms.Button btnCreateNewTopic;
        private System.Windows.Forms.Button btnEditSelectedTopic;
        private System.Windows.Forms.Button btnMoveTopicDown;
        private System.Windows.Forms.Button btnMoveTopicUp;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel StatusPnl;
        private System.Windows.Forms.Panel MainWingControlPanel;
        private System.Windows.Forms.Button btnWingIn;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label ShowLookNameLabel;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lblTopicRundownName;
        private System.Windows.Forms.Button btnMoveWidgetDown;
        private System.Windows.Forms.Button btnMoveWidgetUp;
        private System.Windows.Forms.Button btnCreateNewWidget;
        private System.Windows.Forms.Button btnEditSelectedWidget;
        private System.Windows.Forms.DataGridView WidgetRundownGrid;
        private System.Windows.Forms.StatusStrip statusBar;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lblTopicRundownCount;
        private System.Windows.Forms.Label lblWidgetRundownCount;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblTimeOfDay;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button btnSaveTopicPlaylist;
        private System.Windows.Forms.CheckBox iNewsMonitoringEnableCheckbox;
        private System.Windows.Forms.Button btnLoadTopicPlaylist;
        private System.Windows.Forms.Button btnLoadWidgetPlaylist;
        private System.Windows.Forms.Button btnSaveWidgetPlaylist;
        private System.Windows.Forms.ContextMenuStrip TopicRundownContextMenu;
        private System.Windows.Forms.ToolStripMenuItem EnableSelectedEntry;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem DeleteSelectedEntry;
        private System.Windows.Forms.Timer TimeOfDayTimer;
        private System.Windows.Forms.ToolStripStatusLabel statusLabel;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripTopicRundown;
        private System.Windows.Forms.ToolStripMenuItem deleteSelectedIRundownItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem enableSelectedRundownItem;
        private System.Windows.Forms.ToolStripMenuItem disableSelectedRundownItem;
        private System.Windows.Forms.BindingSource topicRundownCollectionBindingSource;
        private System.Windows.Forms.Button vizConnectLED;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lblWidgetRundownName;
        private System.Windows.Forms.DataGridView TopicRundownGrid;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Topic_Enabled;
        private System.Windows.Forms.DataGridViewTextBoxColumn Topic_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Topic_Slug;
        private System.Windows.Forms.DataGridViewTextBoxColumn Topic_LongSlug;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Widget_Enabled;
        private System.Windows.Forms.DataGridViewTextBoxColumn Widget_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Widget_TemplateID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Widget_Data;
        private System.Windows.Forms.DataGridViewTextBoxColumn Widget_SupplementalData;
        private System.Windows.Forms.DataGridViewTextBoxColumn wingDBConnectionStringDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn topicRundownNameDataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn wingDBConnectionStringDataGridViewTextBoxColumn;
        private System.Windows.Forms.DataGridViewTextBoxColumn topicRundownNameDataGridViewTextBoxColumn;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripWidgetRundown;
        private System.Windows.Forms.ToolStripMenuItem deleteSelectedWidgetItem;
        private System.Windows.Forms.ToolStripMenuItem enableSelectedWidgetItem;
        private System.Windows.Forms.ToolStripMenuItem disableSelectedWidgetItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
    }
}

