﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoxSports.WingController.Data.DataModel
{
    /// <summary>
    /// Class definition for widget rundown element collection
    /// </summary>
    public class WidgetRundownElementModel
    {
        public string rundownName { get; set; }
        public double rundownId { get; set; }
        public double pageId { get; set; }
        public bool enabled { get; set; }
        public int templateId { get; set; }
        public int titleSize { get; set; }
        public string titleText1 { get; set; }
        public string titleText2 { get; set; }
        public string dataType_1 { get; set; }
        public string dataValue_1 { get; set; }
        public string dataType_2 { get; set; }
        public string dataValue_2 { get; set; }
        public string dataType_3 { get; set; }
        public string dataValue_3 { get; set; }
        public string dataType_4 { get; set; }
        public string dataValue_4 { get; set; }
        public string dataType_5 { get; set; }
        public string dataValue_5 { get; set; }
        public string dataType_6 { get; set; }
        public string dataValue_6 { get; set; }
        public string dataType_7 { get; set; }
        public string dataValue_7 { get; set; }
        public string dataType_8 { get; set; }
        public string dataValue_8 { get; set; }
    }
}
