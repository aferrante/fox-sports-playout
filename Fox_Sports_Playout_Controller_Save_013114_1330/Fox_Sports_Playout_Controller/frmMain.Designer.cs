﻿namespace Fox_Sports_Playout_Controller
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            this.TopicRundownPnl = new System.Windows.Forms.Panel();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.LoadTopicPlaylistBtn = new System.Windows.Forms.Button();
            this.SaveTopicPlaylistBtn = new System.Windows.Forms.Button();
            this.iNewsMonitoringEnableCheckbox = new System.Windows.Forms.CheckBox();
            this.TakeSelectedTopicBtn = new System.Windows.Forms.Button();
            this.MoveTopicDownBtn = new System.Windows.Forms.Button();
            this.MoveTopicUpBtn = new System.Windows.Forms.Button();
            this.CreateNewTopicBtn = new System.Windows.Forms.Button();
            this.EditSelectedTopicBtn = new System.Windows.Forms.Button();
            this.TopicRundownGrid = new System.Windows.Forms.DataGridView();
            this.Topic_Enabled = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Topic_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Topic_TemplateID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Topic_Slug = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Topic_LongSlug = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TopicRundownContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.EnableSelectedEntry = new System.Windows.Forms.ToolStripMenuItem();
            this.DisableSelectedEntry = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.DeleteSelectedEntry = new System.Windows.Forms.ToolStripMenuItem();
            this.TakeNextTopicBtn = new System.Windows.Forms.Button();
            this.WidgetRundownPnl = new System.Windows.Forms.Panel();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.WidgetRundownGrid = new System.Windows.Forms.DataGridView();
            this.Widget_Enabled = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.Widget_ID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Widget_TemplateID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Widget_Data = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Widget_SupplementalData = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TakeSelectedWidgetBtn = new System.Windows.Forms.Button();
            this.TakeNextWidgetBtn = new System.Windows.Forms.Button();
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.programToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.preferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.generalPreferencesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.vizEngineSetupToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.shapeContainer1 = new Microsoft.VisualBasic.PowerPacks.ShapeContainer();
            this.rectangleShape5 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.rectangleShape2 = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.vizConnectLED = new Microsoft.VisualBasic.PowerPacks.RectangleShape();
            this.label3 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.StatusPnl = new System.Windows.Forms.Panel();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.btnTestViz = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.lblTimeOfDay = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.ShowLookNameLabel = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.RundownNameLabel = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.MainWingControlPanel = new System.Windows.Forms.Panel();
            this.WingInBtn = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.TimeOfDayTimer = new System.Windows.Forms.Timer(this.components);
            this.TopicRundownPnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TopicRundownGrid)).BeginInit();
            this.TopicRundownContextMenu.SuspendLayout();
            this.WidgetRundownPnl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.WidgetRundownGrid)).BeginInit();
            this.MainMenu.SuspendLayout();
            this.StatusPnl.SuspendLayout();
            this.MainWingControlPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // TopicRundownPnl
            // 
            this.TopicRundownPnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.TopicRundownPnl.Controls.Add(this.richTextBox1);
            this.TopicRundownPnl.Controls.Add(this.LoadTopicPlaylistBtn);
            this.TopicRundownPnl.Controls.Add(this.SaveTopicPlaylistBtn);
            this.TopicRundownPnl.Controls.Add(this.iNewsMonitoringEnableCheckbox);
            this.TopicRundownPnl.Controls.Add(this.TakeSelectedTopicBtn);
            this.TopicRundownPnl.Controls.Add(this.MoveTopicDownBtn);
            this.TopicRundownPnl.Controls.Add(this.MoveTopicUpBtn);
            this.TopicRundownPnl.Controls.Add(this.CreateNewTopicBtn);
            this.TopicRundownPnl.Controls.Add(this.EditSelectedTopicBtn);
            this.TopicRundownPnl.Controls.Add(this.TopicRundownGrid);
            this.TopicRundownPnl.Controls.Add(this.TakeNextTopicBtn);
            this.TopicRundownPnl.Location = new System.Drawing.Point(43, 168);
            this.TopicRundownPnl.Name = "TopicRundownPnl";
            this.TopicRundownPnl.Size = new System.Drawing.Size(1281, 290);
            this.TopicRundownPnl.TabIndex = 0;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(-1, 11);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(239, 113);
            this.richTextBox1.TabIndex = 16;
            this.richTextBox1.Text = "";
            // 
            // LoadTopicPlaylistBtn
            // 
            this.LoadTopicPlaylistBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LoadTopicPlaylistBtn.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.folder_Open_16xLG;
            this.LoadTopicPlaylistBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.LoadTopicPlaylistBtn.Location = new System.Drawing.Point(833, 245);
            this.LoadTopicPlaylistBtn.Name = "LoadTopicPlaylistBtn";
            this.LoadTopicPlaylistBtn.Size = new System.Drawing.Size(194, 35);
            this.LoadTopicPlaylistBtn.TabIndex = 15;
            this.LoadTopicPlaylistBtn.Text = "Load Playlist";
            this.LoadTopicPlaylistBtn.UseVisualStyleBackColor = true;
            this.LoadTopicPlaylistBtn.Visible = false;
            this.LoadTopicPlaylistBtn.Click += new System.EventHandler(this.LoadTopicPlaylistBtn_Click);
            // 
            // SaveTopicPlaylistBtn
            // 
            this.SaveTopicPlaylistBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveTopicPlaylistBtn.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.save_16xLG;
            this.SaveTopicPlaylistBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.SaveTopicPlaylistBtn.Location = new System.Drawing.Point(610, 245);
            this.SaveTopicPlaylistBtn.Name = "SaveTopicPlaylistBtn";
            this.SaveTopicPlaylistBtn.Size = new System.Drawing.Size(194, 35);
            this.SaveTopicPlaylistBtn.TabIndex = 14;
            this.SaveTopicPlaylistBtn.Text = "Save Playlist";
            this.SaveTopicPlaylistBtn.UseVisualStyleBackColor = true;
            this.SaveTopicPlaylistBtn.Visible = false;
            // 
            // iNewsMonitoringEnableCheckbox
            // 
            this.iNewsMonitoringEnableCheckbox.AutoSize = true;
            this.iNewsMonitoringEnableCheckbox.BackColor = System.Drawing.SystemColors.Control;
            this.iNewsMonitoringEnableCheckbox.Checked = true;
            this.iNewsMonitoringEnableCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
            this.iNewsMonitoringEnableCheckbox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.iNewsMonitoringEnableCheckbox.Location = new System.Drawing.Point(12, 251);
            this.iNewsMonitoringEnableCheckbox.Name = "iNewsMonitoringEnableCheckbox";
            this.iNewsMonitoringEnableCheckbox.Size = new System.Drawing.Size(235, 24);
            this.iNewsMonitoringEnableCheckbox.TabIndex = 13;
            this.iNewsMonitoringEnableCheckbox.Text = "iNews Monitoring Enabled";
            this.iNewsMonitoringEnableCheckbox.UseVisualStyleBackColor = false;
            this.iNewsMonitoringEnableCheckbox.CheckStateChanged += new System.EventHandler(this.iNewsMonitoringEnableCheckbox_CheckStateChanged);
            // 
            // TakeSelectedTopicBtn
            // 
            this.TakeSelectedTopicBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TakeSelectedTopicBtn.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.Symbols_Play_32xLG;
            this.TakeSelectedTopicBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TakeSelectedTopicBtn.Location = new System.Drawing.Point(1056, 82);
            this.TakeSelectedTopicBtn.Name = "TakeSelectedTopicBtn";
            this.TakeSelectedTopicBtn.Size = new System.Drawing.Size(194, 63);
            this.TakeSelectedTopicBtn.TabIndex = 8;
            this.TakeSelectedTopicBtn.Text = "Take Selected";
            this.TakeSelectedTopicBtn.UseVisualStyleBackColor = true;
            // 
            // MoveTopicDownBtn
            // 
            this.MoveTopicDownBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MoveTopicDownBtn.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.arrow_Down_16xLG;
            this.MoveTopicDownBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.MoveTopicDownBtn.Location = new System.Drawing.Point(1158, 155);
            this.MoveTopicDownBtn.Name = "MoveTopicDownBtn";
            this.MoveTopicDownBtn.Size = new System.Drawing.Size(92, 35);
            this.MoveTopicDownBtn.TabIndex = 11;
            this.MoveTopicDownBtn.Text = "Move";
            this.MoveTopicDownBtn.UseVisualStyleBackColor = true;
            // 
            // MoveTopicUpBtn
            // 
            this.MoveTopicUpBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.MoveTopicUpBtn.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.arrow_Up_16xLG;
            this.MoveTopicUpBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.MoveTopicUpBtn.Location = new System.Drawing.Point(1056, 155);
            this.MoveTopicUpBtn.Name = "MoveTopicUpBtn";
            this.MoveTopicUpBtn.Size = new System.Drawing.Size(92, 35);
            this.MoveTopicUpBtn.TabIndex = 10;
            this.MoveTopicUpBtn.Text = "Move";
            this.MoveTopicUpBtn.UseVisualStyleBackColor = true;
            // 
            // CreateNewTopicBtn
            // 
            this.CreateNewTopicBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CreateNewTopicBtn.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.action_add_16xLG;
            this.CreateNewTopicBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.CreateNewTopicBtn.Location = new System.Drawing.Point(1056, 245);
            this.CreateNewTopicBtn.Name = "CreateNewTopicBtn";
            this.CreateNewTopicBtn.Size = new System.Drawing.Size(194, 35);
            this.CreateNewTopicBtn.TabIndex = 9;
            this.CreateNewTopicBtn.Text = "Create New";
            this.CreateNewTopicBtn.UseVisualStyleBackColor = true;
            this.CreateNewTopicBtn.Click += new System.EventHandler(this.CreateNewTopicBtn_Click);
            // 
            // EditSelectedTopicBtn
            // 
            this.EditSelectedTopicBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.EditSelectedTopicBtn.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.dialog_32xLG;
            this.EditSelectedTopicBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.EditSelectedTopicBtn.Location = new System.Drawing.Point(1056, 200);
            this.EditSelectedTopicBtn.Name = "EditSelectedTopicBtn";
            this.EditSelectedTopicBtn.Size = new System.Drawing.Size(194, 35);
            this.EditSelectedTopicBtn.TabIndex = 8;
            this.EditSelectedTopicBtn.Text = "Edit Selected";
            this.EditSelectedTopicBtn.UseVisualStyleBackColor = true;
            // 
            // TopicRundownGrid
            // 
            this.TopicRundownGrid.AllowUserToAddRows = false;
            this.TopicRundownGrid.AllowUserToDeleteRows = false;
            this.TopicRundownGrid.AllowUserToOrderColumns = true;
            this.TopicRundownGrid.AllowUserToResizeColumns = false;
            this.TopicRundownGrid.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.TopicRundownGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.TopicRundownGrid.ColumnHeadersHeight = 26;
            this.TopicRundownGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Topic_Enabled,
            this.Topic_ID,
            this.Topic_TemplateID,
            this.Topic_Slug,
            this.Topic_LongSlug});
            this.TopicRundownGrid.ContextMenuStrip = this.TopicRundownContextMenu;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.TopicRundownGrid.DefaultCellStyle = dataGridViewCellStyle2;
            this.TopicRundownGrid.Location = new System.Drawing.Point(12, 13);
            this.TopicRundownGrid.MultiSelect = false;
            this.TopicRundownGrid.Name = "TopicRundownGrid";
            this.TopicRundownGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.TopicRundownGrid.Size = new System.Drawing.Size(1015, 222);
            this.TopicRundownGrid.TabIndex = 0;
            // 
            // Topic_Enabled
            // 
            this.Topic_Enabled.HeaderText = "Enabled";
            this.Topic_Enabled.Name = "Topic_Enabled";
            this.Topic_Enabled.Width = 70;
            // 
            // Topic_ID
            // 
            this.Topic_ID.HeaderText = "ID";
            this.Topic_ID.Name = "Topic_ID";
            this.Topic_ID.ReadOnly = true;
            this.Topic_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Topic_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Topic_TemplateID
            // 
            this.Topic_TemplateID.HeaderText = "Template";
            this.Topic_TemplateID.Name = "Topic_TemplateID";
            this.Topic_TemplateID.ReadOnly = true;
            this.Topic_TemplateID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Topic_TemplateID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Topic_TemplateID.Width = 200;
            // 
            // Topic_Slug
            // 
            this.Topic_Slug.HeaderText = "Slug";
            this.Topic_Slug.Name = "Topic_Slug";
            this.Topic_Slug.ReadOnly = true;
            this.Topic_Slug.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Topic_Slug.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Topic_Slug.Width = 200;
            // 
            // Topic_LongSlug
            // 
            this.Topic_LongSlug.HeaderText = "Long Slug";
            this.Topic_LongSlug.Name = "Topic_LongSlug";
            this.Topic_LongSlug.ReadOnly = true;
            this.Topic_LongSlug.Width = 400;
            // 
            // TopicRundownContextMenu
            // 
            this.TopicRundownContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.EnableSelectedEntry,
            this.DisableSelectedEntry,
            this.toolStripSeparator1,
            this.DeleteSelectedEntry});
            this.TopicRundownContextMenu.Name = "TopicRundownContextMenu";
            this.TopicRundownContextMenu.Size = new System.Drawing.Size(190, 76);
            // 
            // EnableSelectedEntry
            // 
            this.EnableSelectedEntry.Name = "EnableSelectedEntry";
            this.EnableSelectedEntry.Size = new System.Drawing.Size(189, 22);
            this.EnableSelectedEntry.Text = "Enable Selected Entry";
            // 
            // DisableSelectedEntry
            // 
            this.DisableSelectedEntry.Name = "DisableSelectedEntry";
            this.DisableSelectedEntry.Size = new System.Drawing.Size(189, 22);
            this.DisableSelectedEntry.Text = "Disable Selected Entry";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(186, 6);
            // 
            // DeleteSelectedEntry
            // 
            this.DeleteSelectedEntry.Name = "DeleteSelectedEntry";
            this.DeleteSelectedEntry.Size = new System.Drawing.Size(189, 22);
            this.DeleteSelectedEntry.Text = "Delete Selected Entry";
            // 
            // TakeNextTopicBtn
            // 
            this.TakeNextTopicBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TakeNextTopicBtn.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.Symbols_Play_32xLG;
            this.TakeNextTopicBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.TakeNextTopicBtn.Location = new System.Drawing.Point(1056, 11);
            this.TakeNextTopicBtn.Name = "TakeNextTopicBtn";
            this.TakeNextTopicBtn.Size = new System.Drawing.Size(194, 63);
            this.TakeNextTopicBtn.TabIndex = 7;
            this.TakeNextTopicBtn.Text = "Take Next (F5)";
            this.TakeNextTopicBtn.UseVisualStyleBackColor = true;
            // 
            // WidgetRundownPnl
            // 
            this.WidgetRundownPnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.WidgetRundownPnl.Controls.Add(this.button8);
            this.WidgetRundownPnl.Controls.Add(this.button9);
            this.WidgetRundownPnl.Controls.Add(this.button1);
            this.WidgetRundownPnl.Controls.Add(this.button2);
            this.WidgetRundownPnl.Controls.Add(this.button3);
            this.WidgetRundownPnl.Controls.Add(this.button4);
            this.WidgetRundownPnl.Controls.Add(this.WidgetRundownGrid);
            this.WidgetRundownPnl.Controls.Add(this.TakeSelectedWidgetBtn);
            this.WidgetRundownPnl.Controls.Add(this.TakeNextWidgetBtn);
            this.WidgetRundownPnl.Location = new System.Drawing.Point(43, 484);
            this.WidgetRundownPnl.Name = "WidgetRundownPnl";
            this.WidgetRundownPnl.Size = new System.Drawing.Size(1281, 290);
            this.WidgetRundownPnl.TabIndex = 1;
            // 
            // button8
            // 
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button8.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.folder_Open_16xLG;
            this.button8.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button8.Location = new System.Drawing.Point(833, 245);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(194, 35);
            this.button8.TabIndex = 17;
            this.button8.Text = "Load Playlist";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.save_16xLG;
            this.button9.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button9.Location = new System.Drawing.Point(610, 245);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(194, 35);
            this.button9.TabIndex = 16;
            this.button9.Text = "Save Playlist";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.arrow_Down_16xLG;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button1.Location = new System.Drawing.Point(1158, 155);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(92, 35);
            this.button1.TabIndex = 15;
            this.button1.Text = "Move";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.arrow_Up_16xLG;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button2.Location = new System.Drawing.Point(1056, 155);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(92, 35);
            this.button2.TabIndex = 14;
            this.button2.Text = "Move";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.action_add_16xLG;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.Location = new System.Drawing.Point(1056, 245);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(194, 35);
            this.button3.TabIndex = 13;
            this.button3.Text = "Create New";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.dialog_32xLG;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button4.Location = new System.Drawing.Point(1056, 200);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(194, 35);
            this.button4.TabIndex = 12;
            this.button4.Text = "Edit Selected";
            this.button4.UseVisualStyleBackColor = true;
            // 
            // WidgetRundownGrid
            // 
            this.WidgetRundownGrid.AllowUserToAddRows = false;
            this.WidgetRundownGrid.AllowUserToDeleteRows = false;
            this.WidgetRundownGrid.AllowUserToOrderColumns = true;
            this.WidgetRundownGrid.AllowUserToResizeColumns = false;
            this.WidgetRundownGrid.AllowUserToResizeRows = false;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.WidgetRundownGrid.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.WidgetRundownGrid.ColumnHeadersHeight = 26;
            this.WidgetRundownGrid.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Widget_Enabled,
            this.Widget_ID,
            this.Widget_TemplateID,
            this.Widget_Data,
            this.Widget_SupplementalData});
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.WidgetRundownGrid.DefaultCellStyle = dataGridViewCellStyle4;
            this.WidgetRundownGrid.Location = new System.Drawing.Point(12, 13);
            this.WidgetRundownGrid.MultiSelect = false;
            this.WidgetRundownGrid.Name = "WidgetRundownGrid";
            this.WidgetRundownGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.WidgetRundownGrid.Size = new System.Drawing.Size(1015, 222);
            this.WidgetRundownGrid.TabIndex = 11;
            // 
            // Widget_Enabled
            // 
            this.Widget_Enabled.HeaderText = "Enabled";
            this.Widget_Enabled.Name = "Widget_Enabled";
            this.Widget_Enabled.Width = 70;
            // 
            // Widget_ID
            // 
            this.Widget_ID.HeaderText = "ID";
            this.Widget_ID.Name = "Widget_ID";
            this.Widget_ID.ReadOnly = true;
            this.Widget_ID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Widget_ID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // Widget_TemplateID
            // 
            this.Widget_TemplateID.HeaderText = "Template";
            this.Widget_TemplateID.Name = "Widget_TemplateID";
            this.Widget_TemplateID.ReadOnly = true;
            this.Widget_TemplateID.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Widget_TemplateID.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Widget_TemplateID.Width = 200;
            // 
            // Widget_Data
            // 
            this.Widget_Data.HeaderText = "Data";
            this.Widget_Data.Name = "Widget_Data";
            this.Widget_Data.ReadOnly = true;
            this.Widget_Data.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.Widget_Data.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            this.Widget_Data.Width = 200;
            // 
            // Widget_SupplementalData
            // 
            this.Widget_SupplementalData.HeaderText = "Supplemental Data";
            this.Widget_SupplementalData.Name = "Widget_SupplementalData";
            this.Widget_SupplementalData.ReadOnly = true;
            this.Widget_SupplementalData.Width = 400;
            // 
            // TakeSelectedWidgetBtn
            // 
            this.TakeSelectedWidgetBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TakeSelectedWidgetBtn.Location = new System.Drawing.Point(1056, 83);
            this.TakeSelectedWidgetBtn.Name = "TakeSelectedWidgetBtn";
            this.TakeSelectedWidgetBtn.Size = new System.Drawing.Size(194, 63);
            this.TakeSelectedWidgetBtn.TabIndex = 10;
            this.TakeSelectedWidgetBtn.Text = "Take Selected";
            this.TakeSelectedWidgetBtn.UseVisualStyleBackColor = true;
            // 
            // TakeNextWidgetBtn
            // 
            this.TakeNextWidgetBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TakeNextWidgetBtn.Location = new System.Drawing.Point(1056, 11);
            this.TakeNextWidgetBtn.Name = "TakeNextWidgetBtn";
            this.TakeNextWidgetBtn.Size = new System.Drawing.Size(194, 63);
            this.TakeNextWidgetBtn.TabIndex = 9;
            this.TakeNextWidgetBtn.Text = "Take Next (F9)";
            this.TakeNextWidgetBtn.UseVisualStyleBackColor = true;
            // 
            // MainMenu
            // 
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.programToolStripMenuItem,
            this.preferencesToolStripMenuItem,
            this.helpToolStripMenuItem});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.ShowItemToolTips = true;
            this.MainMenu.Size = new System.Drawing.Size(1366, 24);
            this.MainMenu.TabIndex = 2;
            this.MainMenu.Text = "MainMenu";
            // 
            // programToolStripMenuItem
            // 
            this.programToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.programToolStripMenuItem.Name = "programToolStripMenuItem";
            this.programToolStripMenuItem.Size = new System.Drawing.Size(65, 20);
            this.programToolStripMenuItem.Text = "&Program";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            // 
            // preferencesToolStripMenuItem
            // 
            this.preferencesToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.generalPreferencesToolStripMenuItem,
            this.vizEngineSetupToolStripMenuItem});
            this.preferencesToolStripMenuItem.Name = "preferencesToolStripMenuItem";
            this.preferencesToolStripMenuItem.Size = new System.Drawing.Size(80, 20);
            this.preferencesToolStripMenuItem.Text = "&Preferences";
            // 
            // generalPreferencesToolStripMenuItem
            // 
            this.generalPreferencesToolStripMenuItem.Name = "generalPreferencesToolStripMenuItem";
            this.generalPreferencesToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.generalPreferencesToolStripMenuItem.Text = "&General Preferences";
            // 
            // vizEngineSetupToolStripMenuItem
            // 
            this.vizEngineSetupToolStripMenuItem.Name = "vizEngineSetupToolStripMenuItem";
            this.vizEngineSetupToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.vizEngineSetupToolStripMenuItem.Text = "&Viz Engine Setup";
            // 
            // helpToolStripMenuItem
            // 
            this.helpToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.aboutToolStripMenuItem});
            this.helpToolStripMenuItem.Name = "helpToolStripMenuItem";
            this.helpToolStripMenuItem.Size = new System.Drawing.Size(44, 20);
            this.helpToolStripMenuItem.Text = "&Help";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(107, 22);
            this.aboutToolStripMenuItem.Text = "&About";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(39, 145);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(132, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Topic Rundown";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(39, 461);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(145, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Widget Rundown";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(38, 25);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(62, 20);
            this.label4.TabIndex = 6;
            this.label4.Text = "Status";
            // 
            // shapeContainer1
            // 
            this.shapeContainer1.Location = new System.Drawing.Point(0, 0);
            this.shapeContainer1.Margin = new System.Windows.Forms.Padding(0);
            this.shapeContainer1.Name = "shapeContainer1";
            this.shapeContainer1.Shapes.AddRange(new Microsoft.VisualBasic.PowerPacks.Shape[] {
            this.rectangleShape5,
            this.rectangleShape2,
            this.vizConnectLED});
            this.shapeContainer1.Size = new System.Drawing.Size(937, 92);
            this.shapeContainer1.TabIndex = 7;
            this.shapeContainer1.TabStop = false;
            // 
            // rectangleShape5
            // 
            this.rectangleShape5.BackColor = System.Drawing.SystemColors.Control;
            this.rectangleShape5.FillColor = System.Drawing.Color.Transparent;
            this.rectangleShape5.FillGradientColor = System.Drawing.Color.DarkRed;
            this.rectangleShape5.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.rectangleShape5.Location = new System.Drawing.Point(724, 63);
            this.rectangleShape5.Name = "rectangleShape5";
            this.rectangleShape5.Size = new System.Drawing.Size(23, 23);
            // 
            // rectangleShape2
            // 
            this.rectangleShape2.BackColor = System.Drawing.SystemColors.Control;
            this.rectangleShape2.FillColor = System.Drawing.Color.Transparent;
            this.rectangleShape2.FillGradientColor = System.Drawing.Color.DarkRed;
            this.rectangleShape2.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.rectangleShape2.Location = new System.Drawing.Point(724, 34);
            this.rectangleShape2.Name = "rectangleShape2";
            this.rectangleShape2.Size = new System.Drawing.Size(23, 23);
            // 
            // vizConnectLED
            // 
            this.vizConnectLED.BackColor = System.Drawing.SystemColors.Control;
            this.vizConnectLED.FillColor = System.Drawing.Color.Transparent;
            this.vizConnectLED.FillGradientColor = System.Drawing.Color.DarkRed;
            this.vizConnectLED.FillStyle = Microsoft.VisualBasic.PowerPacks.FillStyle.Solid;
            this.vizConnectLED.Location = new System.Drawing.Point(724, 5);
            this.vizConnectLED.Name = "vizConnectLED";
            this.vizConnectLED.Size = new System.Drawing.Size(23, 23);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(-1, -24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 20);
            this.label3.TabIndex = 6;
            this.label3.Text = "Status";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(757, 6);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 20);
            this.label5.TabIndex = 8;
            this.label5.Text = "Viz Engine";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(757, 35);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(131, 20);
            this.label6.TabIndex = 9;
            this.label6.Text = "iNews Gateway";
            // 
            // StatusPnl
            // 
            this.StatusPnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.StatusPnl.Controls.Add(this.button7);
            this.StatusPnl.Controls.Add(this.button6);
            this.StatusPnl.Controls.Add(this.btnTestViz);
            this.StatusPnl.Controls.Add(this.button5);
            this.StatusPnl.Controls.Add(this.label17);
            this.StatusPnl.Controls.Add(this.lblTimeOfDay);
            this.StatusPnl.Controls.Add(this.label15);
            this.StatusPnl.Controls.Add(this.ShowLookNameLabel);
            this.StatusPnl.Controls.Add(this.label10);
            this.StatusPnl.Controls.Add(this.RundownNameLabel);
            this.StatusPnl.Controls.Add(this.label8);
            this.StatusPnl.Controls.Add(this.label6);
            this.StatusPnl.Controls.Add(this.label5);
            this.StatusPnl.Controls.Add(this.label3);
            this.StatusPnl.Controls.Add(this.shapeContainer1);
            this.StatusPnl.Location = new System.Drawing.Point(42, 48);
            this.StatusPnl.Name = "StatusPnl";
            this.StatusPnl.Size = new System.Drawing.Size(939, 94);
            this.StatusPnl.TabIndex = 5;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(611, 31);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 23;
            this.button7.Text = "Unload Scene";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(527, 31);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 22;
            this.button6.Text = "LoadScene";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // btnTestViz
            // 
            this.btnTestViz.Location = new System.Drawing.Point(527, 5);
            this.btnTestViz.Name = "btnTestViz";
            this.btnTestViz.Size = new System.Drawing.Size(75, 23);
            this.btnTestViz.TabIndex = 21;
            this.btnTestViz.Text = "Test Viz";
            this.btnTestViz.UseVisualStyleBackColor = true;
            this.btnTestViz.Click += new System.EventHandler(this.btnTestViz_Click);
            // 
            // button5
            // 
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.Location = new System.Drawing.Point(515, 63);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(138, 25);
            this.button5.TabIndex = 20;
            this.button5.Text = "Change Skin";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(9, 5);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(161, 20);
            this.label17.TabIndex = 19;
            this.label17.Text = "Current Date/Time:";
            // 
            // lblTimeOfDay
            // 
            this.lblTimeOfDay.BackColor = System.Drawing.Color.Black;
            this.lblTimeOfDay.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTimeOfDay.ForeColor = System.Drawing.Color.Red;
            this.lblTimeOfDay.Location = new System.Drawing.Point(182, 5);
            this.lblTimeOfDay.Name = "lblTimeOfDay";
            this.lblTimeOfDay.Size = new System.Drawing.Size(317, 20);
            this.lblTimeOfDay.TabIndex = 18;
            this.lblTimeOfDay.Text = "01/03/2014 19:20";
            this.lblTimeOfDay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(757, 64);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(127, 20);
            this.label15.TabIndex = 14;
            this.label15.Text = "SQL Database";
            // 
            // ShowLookNameLabel
            // 
            this.ShowLookNameLabel.BackColor = System.Drawing.Color.Black;
            this.ShowLookNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ShowLookNameLabel.ForeColor = System.Drawing.Color.Red;
            this.ShowLookNameLabel.Location = new System.Drawing.Point(182, 66);
            this.ShowLookNameLabel.Name = "ShowLookNameLabel";
            this.ShowLookNameLabel.Size = new System.Drawing.Size(317, 20);
            this.ShowLookNameLabel.TabIndex = 13;
            this.ShowLookNameLabel.Text = "Default";
            this.ShowLookNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(9, 66);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(167, 20);
            this.label10.TabIndex = 12;
            this.label10.Text = "Current Show Look:";
            // 
            // RundownNameLabel
            // 
            this.RundownNameLabel.BackColor = System.Drawing.Color.Black;
            this.RundownNameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RundownNameLabel.ForeColor = System.Drawing.Color.Red;
            this.RundownNameLabel.Location = new System.Drawing.Point(182, 34);
            this.RundownNameLabel.Name = "RundownNameLabel";
            this.RundownNameLabel.Size = new System.Drawing.Size(317, 20);
            this.RundownNameLabel.TabIndex = 11;
            this.RundownNameLabel.Text = "Fox Sports Live";
            this.RundownNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(9, 34);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(159, 20);
            this.label8.TabIndex = 10;
            this.label8.Text = "Current Rundown: ";
            // 
            // MainWingControlPanel
            // 
            this.MainWingControlPanel.BackColor = System.Drawing.SystemColors.Control;
            this.MainWingControlPanel.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.MainWingControlPanel.Controls.Add(this.WingInBtn);
            this.MainWingControlPanel.Controls.Add(this.label9);
            this.MainWingControlPanel.Location = new System.Drawing.Point(987, 49);
            this.MainWingControlPanel.Name = "MainWingControlPanel";
            this.MainWingControlPanel.Size = new System.Drawing.Size(337, 94);
            this.MainWingControlPanel.TabIndex = 8;
            // 
            // WingInBtn
            // 
            this.WingInBtn.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.WingInBtn.Image = global::Fox_Sports_Playout_Controller.Properties.Resources.StatusAnnotations_Play_32xLG_color;
            this.WingInBtn.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.WingInBtn.Location = new System.Drawing.Point(26, 11);
            this.WingInBtn.Name = "WingInBtn";
            this.WingInBtn.Size = new System.Drawing.Size(284, 72);
            this.WingInBtn.TabIndex = 8;
            this.WingInBtn.Text = "Take Wing In (F1)";
            this.WingInBtn.UseVisualStyleBackColor = true;
            this.WingInBtn.Click += new System.EventHandler(this.WingInBtn_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(-1, -24);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(62, 20);
            this.label9.TabIndex = 6;
            this.label9.Text = "Status";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(984, 24);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(155, 20);
            this.label7.TabIndex = 9;
            this.label7.Text = "Main Wing Control";
            // 
            // statusStrip1
            // 
            this.statusStrip1.Location = new System.Drawing.Point(0, 781);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1366, 22);
            this.statusStrip1.TabIndex = 10;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(215, 145);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(159, 20);
            this.label11.TabIndex = 11;
            this.label11.Text = "Number of Entries:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(369, 145);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(19, 20);
            this.label12.TabIndex = 12;
            this.label12.Text = "0";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(369, 461);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(19, 20);
            this.label13.TabIndex = 14;
            this.label13.Text = "0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(215, 461);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(159, 20);
            this.label14.TabIndex = 13;
            this.label14.Text = "Number of Entries:";
            // 
            // TimeOfDayTimer
            // 
            this.TimeOfDayTimer.Interval = 1000;
            this.TimeOfDayTimer.Tick += new System.EventHandler(this.TimeOfDayTimer_Tick);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1366, 803);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.MainWingControlPanel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.StatusPnl);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.WidgetRundownPnl);
            this.Controls.Add(this.TopicRundownPnl);
            this.Controls.Add(this.MainMenu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.MainMenu;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Fox Sports Wing Playout Controller  V1.0.0  Video Design Software Inc.";
            this.Activated += new System.EventHandler(this.frmMain_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
            this.TopicRundownPnl.ResumeLayout(false);
            this.TopicRundownPnl.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TopicRundownGrid)).EndInit();
            this.TopicRundownContextMenu.ResumeLayout(false);
            this.WidgetRundownPnl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.WidgetRundownGrid)).EndInit();
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.StatusPnl.ResumeLayout(false);
            this.StatusPnl.PerformLayout();
            this.MainWingControlPanel.ResumeLayout(false);
            this.MainWingControlPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel TopicRundownPnl;
        private System.Windows.Forms.Panel WidgetRundownPnl;
        private System.Windows.Forms.MenuStrip MainMenu;
        private System.Windows.Forms.ToolStripMenuItem programToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem preferencesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem generalPreferencesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem vizEngineSetupToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.DataGridView TopicRundownGrid;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button TakeNextTopicBtn;
        private System.Windows.Forms.Button TakeSelectedTopicBtn;
        private System.Windows.Forms.Button TakeSelectedWidgetBtn;
        private System.Windows.Forms.Button TakeNextWidgetBtn;
        private System.Windows.Forms.Button CreateNewTopicBtn;
        private System.Windows.Forms.Button EditSelectedTopicBtn;
        private System.Windows.Forms.Button MoveTopicDownBtn;
        private System.Windows.Forms.Button MoveTopicUpBtn;
        private Microsoft.VisualBasic.PowerPacks.ShapeContainer shapeContainer1;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape2;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape vizConnectLED;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel StatusPnl;
        private System.Windows.Forms.Panel MainWingControlPanel;
        private System.Windows.Forms.Button WingInBtn;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label ShowLookNameLabel;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label RundownNameLabel;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.DataGridView WidgetRundownGrid;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private Microsoft.VisualBasic.PowerPacks.RectangleShape rectangleShape5;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label lblTimeOfDay;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button SaveTopicPlaylistBtn;
        private System.Windows.Forms.CheckBox iNewsMonitoringEnableCheckbox;
        private System.Windows.Forms.Button LoadTopicPlaylistBtn;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Widget_Enabled;
        private System.Windows.Forms.DataGridViewTextBoxColumn Widget_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Widget_TemplateID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Widget_Data;
        private System.Windows.Forms.DataGridViewTextBoxColumn Widget_SupplementalData;
        private System.Windows.Forms.DataGridViewCheckBoxColumn Topic_Enabled;
        private System.Windows.Forms.DataGridViewTextBoxColumn Topic_ID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Topic_TemplateID;
        private System.Windows.Forms.DataGridViewTextBoxColumn Topic_Slug;
        private System.Windows.Forms.DataGridViewTextBoxColumn Topic_LongSlug;
        private System.Windows.Forms.ContextMenuStrip TopicRundownContextMenu;
        private System.Windows.Forms.ToolStripMenuItem EnableSelectedEntry;
        private System.Windows.Forms.ToolStripMenuItem DisableSelectedEntry;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem DeleteSelectedEntry;
        private System.Windows.Forms.Timer TimeOfDayTimer;
        private System.Windows.Forms.Button btnTestViz;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
    }
}

