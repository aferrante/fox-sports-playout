﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoxSports.WingController.Data.Enums
{
    using System.ComponentModel;

    // Enumerator for template fields types
    public enum SceneActionParameterTypes
    {
        // Parameter Type - Static Value
        [Description("Static Value")]
        Static_Value = 1,

        // Parameter Type - Variable
        [Description("Variable Value")]
        Variable_Value
    }
}
