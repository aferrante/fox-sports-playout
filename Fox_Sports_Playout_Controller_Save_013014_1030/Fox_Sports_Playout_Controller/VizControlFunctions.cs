﻿// Definition for class of Viz control functions; uses Asynchronous socket class defined in ClientSocket
// M. Dilworth  VDS  01/28/14
using System;
using ClientSocket;

namespace VizControlFunctions
{
    // Declare subclass of socket class for Viz communications
    public class VizControlPort : AsynchronousClient
    {
        #region Public Methods

        // Constructor
        public VizControlPort(string ipAddress, Int32 port)
            : base(ipAddress, port)
        {
        }

        // Function to load Viz scene
        public void LoadScene(String SceneName)
        {
            SendStringToViz("0 RENDERER*MAIN_LAYER SET_OBJECT SCENE*" + SceneName);
        }

        // Function to unload Viz scene
        public void UnloadScene(String SceneName)
        {
            SendStringToViz("0 SCENE*" + SceneName + " CLOSE");
        }

        // Function to send Datapool command
        public void SendDataPoolCommand(String Data)
        {
            SendStringToViz("0 RENDERER*FUNCTION*DataPool*Data SET " + Data);
        }
        #endregion

        #region Private Methods
        // Simple function to just always ensure we send the null character at the end of the string as required by Viz
        private void SendStringToViz(String Command)
        {
            // Append null character required for Viz & send
            char nullChar = '\0';
            this.Send(Command + nullChar);
        }
        #endregion
    }
}