﻿// Definition for class used for all scene action command functions
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FoxSports.WingController.Data.DataAccess;
using FoxSports.WingController.Data.DataModel;
using FoxSports.WingController.VizIntf.VizControlFunctions;
using System.Data.SqlClient;
using System.ComponentModel;
using FoxSports.WingController.Data.Enums;

namespace FoxSports.WingController.Logic.SceneActions
{
    /// <summary>
    /// Class for operations related to the topic rundown
    /// </summary>
    public class SceneActionCommandsCollection
    {
        #region Delegates & Events (testing only)
        public delegate void delCommandAdded(SceneActionCommandsCollection sender, string Command);
        public event delCommandAdded CommandAdded;        
        #endregion

        #region Properties and Members
        public List<SceneActionCommandModel> sceneActionCommands;
        public List<PlayoutCommandModel> playoutCommands;
        public string WingDBConnectionString { get; set; }
        private VizControlPort VizController;
        #endregion

        #region Public Methods
        // Constructor - instantiates list collection
        public SceneActionCommandsCollection(VizControlPort VizControl)
        {
            // Create collections
            sceneActionCommands = new List<SceneActionCommandModel>();
            playoutCommands = new List<PlayoutCommandModel>();
            VizController = VizControl;
        }

        /// <summary>
        /// Get the scene action list from the SQL DB; clears out existing collection first
        /// </summary>
        public List<SceneActionCommandModel> GetSceneActionCommandCollection()
        {
            DataTable dataTable;

            // Clear out the current collection
            sceneActionCommands.Clear();

            try
            {
                SceneActionCommandsAccess sceneActionCommandsAccess = new SceneActionCommandsAccess();
                sceneActionCommandsAccess.WingDBConnectionString = WingDBConnectionString;
                dataTable = sceneActionCommandsAccess.GetSceneActionCommands();

                foreach (DataRow row in dataTable.Rows)
                {
                    var newSceneActionCommand = new SceneActionCommandModel()
                    {
                        actionId = Convert.ToInt32(row["ActionId"]),
                        commandId = Convert.ToInt32(row["CommandId"]),
                        commandText = row["CommandText"].ToString(),
                        commandDescription = row["CommandDescription"].ToString(),
                        parameterType = Convert.ToInt32(row["ParameterType"]),
                        parameterValue = row["ParameterValue"].ToString(),
                        parameterDescription = row["ParameterDescription"].ToString(),
                    };
                    sceneActionCommands.Add(newSceneActionCommand);
                }
            }
            catch
            {
                throw;
            }
            // Return 
            return sceneActionCommands;
        }

        /// <summary>
        /// Function to create the playout command collection based on the action ID sent; also calls function to look up parameter values
        /// </summary>
        public void SendPlayoutCommands(Int32 selectedActionID)
        {
            // Clear out the current collection
            playoutCommands.Clear();

            try
            {
                // Do LINQ query to get commands accociated with the specified Action ID
                var applicableCommands = from sceneActionCommand in sceneActionCommands
                                 where (sceneActionCommand.actionId == selectedActionID)
                                 select new { Command = sceneActionCommand.commandText,
                                              ParameterType = sceneActionCommand.parameterType,
                                              ParameterValue = sceneActionCommand.parameterValue
                                 };
                if (applicableCommands != null)
                {
                    foreach (var item in applicableCommands)
                    {
                        // Fire event to update listbox for debug on main form
                        CommandAdded(this, item.Command + " = " + item.ParameterValue);

                        // Get value of parameter
                        string paramValue = "";
                        if (item.ParameterType == (int)SceneActionParameterTypes.Variable_Value)
                        {
                            paramValue = GetCurrentParameterValue(item.ParameterValue);
                        }
                        else if (item.ParameterType == (int)SceneActionParameterTypes.Static_Value)
                        {
                            paramValue = item.ParameterValue;
                        }

                        // Send command to Viz
                        this.VizController.SendDataPoolCommand(item.Command + "=" + paramValue);
                    }                                       
                }
            }
            catch
            {
                throw;
            }
        }

        // Function to get the current value of the specified variable for a single command
        private string GetCurrentParameterValue(string ParameterValue)
        {
            string outputValue = ParameterValue;
            return outputValue;
        }

        #endregion
    }
}
