﻿// Class for database access to load/store collections
// Instantiated by TopicRundownCollection class
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.ComponentModel;

using NPoco;

namespace FoxSports.WingController.Data.DataAccess
{
    using System;
    using System.Data;
    using System.Data.SqlClient;

    using FoxSports.WingController.Data.DataModel;

    /// <summary>
    /// Static class for handling database access for topic rundown
    /// </summary>
    public static class TopicRundownAccess
    {
        #region Properties and Members
        public static string WingDBConnectionString { get; set; }
        public static double RundownID { get; set; }
        public static string RundownName { get; set; }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method to get the specified Topic Rundown from SQL DB and pass it back to the logic layer as a BindingList
        /// Uses nPoco micro-ORM for strongly-type database access
        /// </summary>
        public static BindingList<TopicRundownElements> GetTopicRundownCollection(double topicRundownID)
        {
            try
            {
                IDatabase db = new Database(WingDBConnectionString, DatabaseType.SqlServer2008);
                BindingList<TopicRundownElements> rundownList;
                using (db)
                {
                    // Get the ID & name of the rundown (ID will be the same as value passed in)
                    TopicRundowns rundown = db.Single<TopicRundowns>("SELECT * FROM TopicRundowns WHERE RundownID = @0", topicRundownID);
                    RundownName = rundown.RundownName;
                    RundownID = rundown.RundownId;

                    // Get the list of rundown elements
                    rundownList = new BindingList<TopicRundownElements>(db.Fetch<TopicRundownElements>("EXEC sp_GetTopicRundown @@RundownID = @0", topicRundownID));
                }
                return rundownList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        /// <summary>
        /// Method to save current topic rundown to DB
        /// </summary>
        public static void SaveTopicRundownCollection(BindingList<TopicRundownElements> topicRundownElements)
        {
            if (topicRundownElements.Count > 0)
            {
                try
                {
                    IDatabase db = new Database(WingDBConnectionString, DatabaseType.SqlServer2008);

                    // Delete the rundown if it is already saved in the DB
                    DeleteTopicRundown(topicRundownElements.ElementAt(0).rundownId);

                    using (db)
                    {
                        // Insert metadata for rundown into TopicRundowns table in DB
                        DateTime currentTime = DateTime.Now;
                        Double _rundownID = RundownID;
                        String _rundownName = RundownName;
                        TopicRundowns topicRundown = new TopicRundowns
                        {
                            RundownId = _rundownID, 
                            RundownName = _rundownName + "Test", 
                            LastUpdated = currentTime
                        };
                        db.Insert(topicRundown);

                        // Insert elements for rundown into TopicRundownElements table in DB
                        foreach (var topicRundownElement in topicRundownElements)
                        {
                            db.Insert(topicRundownElement);
                        }
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Method to delete specified topic rundown from DB
        /// </summary>
        public static void DeleteTopicRundown(double topicRundownID)
        {
            try
            {
                // Instantiate the connection
                using (SqlConnection connection = new SqlConnection(WingDBConnectionString))
                {
                    connection.Open();

                    // Create the command and set its properties
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
                        {
                            cmd.CommandText = "sp_DeleteTopicRundown " + Convert.ToString(topicRundownID);
                            sqlDataAdapter.SelectCommand = cmd;
                            sqlDataAdapter.SelectCommand.Connection = connection;
                            sqlDataAdapter.SelectCommand.CommandType = CommandType.Text;

                            // Execute stored proc
                            sqlDataAdapter.SelectCommand.ExecuteNonQuery();
                        }
                    }
                    connection.Close();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
