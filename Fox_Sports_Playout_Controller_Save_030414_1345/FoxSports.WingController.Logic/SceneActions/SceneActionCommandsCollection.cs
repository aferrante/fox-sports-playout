﻿// Definition for class used for all scene action command functions
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FoxSports.WingController.Data.DataAccess;
using FoxSports.WingController.Data.DataModel;
using System.Data.SqlClient;
using System.ComponentModel;

namespace FoxSports.WingController.Logic.SceneActions
{
    /// <summary>
    /// Class for operations related to the topic rundown
    /// </summary>
    public class SceneActionCommandsCollection
    {
        #region Properties and Members
        public List<SceneActionCommandModel> sceneActionCommands;
        public string WingDBConnectionString { get; set; }
        #endregion

        #region Public Methods
        // Constructor - instantiates list collection
        public SceneActionCommandsCollection()
        {
            // Create list
            sceneActionCommands = new List<SceneActionCommandModel>();
        }

        /// <summary>
        /// Get the scene action list from the SQL DB; clears out existing collection first
        /// </summary>
        public List<SceneActionCommandModel> GetSceneActionCommandCollection()
        {
            DataTable dataTable;

            // Clear out the current collection
            sceneActionCommands.Clear();

            try
            {
                SceneActionCommandsAccess sceneActionCommandsAccess = new SceneActionCommandsAccess();
                sceneActionCommandsAccess.WingDBConnectionString = WingDBConnectionString;
                dataTable = sceneActionCommandsAccess.GetSceneActionCommands();

                foreach (DataRow row in dataTable.Rows)
                {
                    var newSceneActionCommand = new SceneActionCommandModel()
                    {
                        actionId = Convert.ToInt32(row["ActionId"]),
                        commandId = Convert.ToInt32(row["CommandId"]),
                        commandText = row["CommandText"].ToString(),
                        commandDescription = row["CommandDescription"].ToString(),
                        parameterType = Convert.ToInt32(row["ParameterType"]),
                        parameterValue = row["ParameterValue"].ToString(),
                        parameterDescription = row["ParameterDescription"].ToString(),
                    };
                    sceneActionCommands.Add(newSceneActionCommand);
                }
            }
            catch
            {
                throw;
            }
            // Return 
            return sceneActionCommands;
        }
        #endregion
    }
}
