﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoxSports.WingController.Data.DataModel
{
    /// <summary>
    /// Class definition for widget rundown element collection
    /// </summary>
    public class WidgetRundownElementModel
    {
        public double rundownId { get; set; }
        public Int32 pageId { get; set; }
        public bool enabled { get; set; }
        public int templateId { get; set; }
        public int titleSize { get; set; }
        public string titleText_1 { get; set; }
        public string titleText_2 { get; set; }
        public int dataType_1 { get; set; }
        public string dataValue_1 { get; set; }
        public int dataType_2 { get; set; }
        public string dataValue_2 { get; set; }
        public int dataType_3 { get; set; }
        public string dataValue_3 { get; set; }
        public int dataType_4 { get; set; }
        public string dataValue_4 { get; set; }
        public int dataType_5 { get; set; }
        public string dataValue_5 { get; set; }
        public int dataType_6 { get; set; }
        public string dataValue_6 { get; set; }
        public int dataType_7 { get; set; }
        public string dataValue_7 { get; set; }
        public int dataType_8 { get; set; }
        public string dataValue_8 { get; set; }
    }
}
