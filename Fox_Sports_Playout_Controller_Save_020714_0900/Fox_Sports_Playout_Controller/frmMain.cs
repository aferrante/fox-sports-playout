﻿//Main form for Fox Sports Wing Playout Controller application
//© Copyright 2013-2014 Video Design Software Inc. - All rights reserved.
//You may use this code module and all associated code modules only under the specific terms of the source code license between Video Design Software Inc. and Fox Sports Media Group.
// M Dilworth  Video Design Software 
//Rev: 2013/12/19

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using FoxSports.WingController.VizIntf.VizControlFunctions;
using FoxSports.WingController.VizIntf.ClientSocket;
using FoxSports.WingController.Data.DataAccess;

namespace Fox_Sports_Playout_Controller
{
    public partial class frmMain : Form
    {
     
        // Flag to indicator wing is in
        bool wingIn = false;

        // TopicRundownAccess
        private TopicRundownAccess topicRundownAccess;

        private string wingDBConnectionString;

        // Data strucure for Topic Rundown elements
        struct topicRundownElement
        {
            // Page IDs declared as double to match SQL type float
            public double rundownid;
            public double pageid;
            public bool enabled;
            public int templateId;
            public string slug;
            public string longSlug;
        }      

        // Data strucure for Widget Rundown elements
        struct widgetRundownElement
        {
            // Page IDs declared as double to match SQL type float
            public double rundownid;
            public double pageid;
            public bool enabled;
            public int templateId;
            public string data;
            public string supplementalData;
        }

        // Define the collection object for the Topic Rundown
        List<topicRundownElement> topicRundownElements = new List<topicRundownElement>(500);

        // Define the collection object for the Widget Rundown
        List<widgetRundownElement> widgetRundownElements = new List<widgetRundownElement>(500);

        // Declare socket parameters
        Int32 socketPort;
        string socketIPAddress;
        VizControlPort VizControl;

        //string wingDBConnectionString;


        private void ReadInConfigData()
        {
            //Read in values from the config file
            socketIPAddress = Properties.Settings.Default.vizControlIPAddress;
            //socketIPAddress = "192.168.1.4";
            //socketPort = 6100;
            socketPort = Properties.Settings.Default.vizControlPortNumber;
            wingDBConnectionString = Properties.Settings.Default.wingDBConnectionString;
        }

        // Handler for main form activation
        public frmMain()
        {
            InitializeComponent();

            // Read in values from the config file
            ReadInConfigData();

            this.topicRundownAccess = new TopicRundownAccess();
            this.topicRundownAccess.WingDBConnectionString = wingDBConnectionString;

            // Get the current data for the topic rundown from the database
            GetTopicRundownElementsFromDB();

            // Update the topic rundown data grid
            UpdateTopicRundownGrid();
            
            // Get the data for the widget rundown from the database
            GetWidgetRundownElementsFromDB();

            // Update the widget rundown data grid
            UpdateWidgetRundownGrid();

            // Connect to the Viz engine; instantiate Viz interface component
            VizControl = new VizControlPort(System.Net.IPAddress.Parse(socketIPAddress), socketPort, true);
            // Initialize the events
            VizControl.DataReceived += new TCPClientWrapper.delDataReceived(VizControl_DataReceived);
            VizControl.ConnectionStatusChanged += new TCPClientWrapper.delConnectionStatusChanged(VizControl_ConnectionStatusChanged);
            // Connect
            VizControl.AutoReconnect = true;
            VizControl.Connect();
            VizControl.LoadScene("USER/MATT/WINGBUILD_OBJECTSTORE_SKINTEST");

            // Enable timers
            TimeOfDayTimer.Enabled = true;
        }

        private void btnTestViz_Click(object sender, EventArgs e)
        {
            VizControl.SendDataPoolCommand("WING=ON");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            VizControl.LoadScene("USER/MATT/WINGBUILD_OBJECTSTORE_SKINTEST");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            VizControl.UnloadScene("USER/MATT/WINGBUILD_OBJECTSTORE_SKINTEST");
        }

        //Fired when the connection status changes in the TCP client       
        void VizControl_ConnectionStatusChanged(TCPClientWrapper sender, VizControlPort.ConnectionStatus status)
        {
            //Check if this event was fired on a different thread, if it is then we must invoke it on the UI thread
            if (InvokeRequired)
            {
                Invoke(new TCPClientWrapper.delConnectionStatusChanged(VizControl_ConnectionStatusChanged), sender, status);
                return;
            }
            richTextBox1.Text += "\r\n" + "Connection Status: " + status.ToString();

            //Set the indicator color if the connection is good
            if (status.ToString() == "Connected")
            {
                vizConnectLED.FillColor = Color.Lime;
            }
            else
            {
                vizConnectLED.FillColor = Color.Transparent;
            }
        }

        //Fired when new data is received in the TCP client
        void VizControl_DataReceived(TCPClientWrapper sender, object data)
        {
            //Check if this needs to be invoked in the UI thread
            if (InvokeRequired)
            {
                try
                {
                    Invoke(new VizControlPort.delDataReceived(VizControl_DataReceived), sender, data);
                }
                catch
                { }
                return;
            }

            //Interpret the received data object as a string
            string strData = data as string;

            //Add the received data to a rich text box
            richTextBox1.Text += "\r\n" + "Data received: " + strData.Trim() + "\r\n";
        }

        //TOPIC RUNDOWN FUNCTIONS
        //Function to query the SQL database and get the topics for the specified rundown
        private void GetTopicRundownElementsFromDB()
        {
            DataTable data = this.topicRundownAccess.GetTopicRundown(0);
            foreach (DataRow row in data.Rows)
            {
                //Add topic to the collection
                //Declare
                topicRundownElement topic = new topicRundownElement();
                //Set values
                topic.enabled = Convert.ToBoolean(row["Enabled"]);
                topic.rundownid = Convert.ToDouble(row["RundownId"]);
                topic.pageid = Convert.ToDouble(row["PageId"]);
                topic.templateId = Convert.ToInt32(row["TemplateId"]);
                topic.slug = row["Slug"].ToString();
                topic.longSlug = row["longSlug"].ToString();

                //Insert into collection
                topicRundownElements.Add(topic);
            }
        }


        //Function to update the topic rundown grid
        private void UpdateTopicRundownGrid()
        {
            for (int i = 0; i < topicRundownElements.Count; i++)
            {
                TopicRundownGrid.RowCount = topicRundownElements.Count;
                DataGridViewRow row = TopicRundownGrid.Rows[i];
                row.Cells["Topic_Enabled"].Value = topicRundownElements[i].enabled;
                row.Cells["Topic_Id"].Value = topicRundownElements[i].pageid;
                row.Cells["Topic_TemplateId"].Value = topicRundownElements[i].templateId;
                row.Cells["Topic_Slug"].Value = topicRundownElements[i].slug;
                row.Cells["Topic_LongSlug"].Value = topicRundownElements[i].longSlug;
            }
        }


        //WIDGET RUNDOWN FUNCTIONS
        //Function to query the SQL database and get the data for the specified widgetrundown
        private void GetWidgetRundownElementsFromDB()
        {
            //Setup connection string
            string connectionString = "Data Source = OWNER-PC\\SQLEXPRESS;Initial Catalog = FoxSportsWing;User ID=sa;Password=Vds@dmin1";// Properties.Settings.Default.wingDBConnectionString;

            //Setup and open SQL connection
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();

            //Setup query
            string selectStatement = "SELECT * FROM WidgetRundownElements";
            //string selectStatement = "GetMessagesByType @Type";

            //Setup the SQL command using the query string
            SqlCommand selectCommand = new SqlCommand(selectStatement, connection);
            //selectCommand.Parameters.AddWithValue("@Type", "list");

            //Setup data reader & iterate through results
            SqlDataReader reader = selectCommand.ExecuteReader(CommandBehavior.CloseConnection);

            int count = 1;
            while (reader.Read())
            {
                //Add widget to the collection
                //Declare
                widgetRundownElement widget = new widgetRundownElement();
                //Set values
                widget.enabled = reader.GetBoolean(reader.GetOrdinal("Enabled"));
                //Page IDs declared as double to match SQL type float
                widget.rundownid = reader.GetDouble(reader.GetOrdinal("RundownId"));
                widget.pageid = reader.GetDouble(reader.GetOrdinal("PageId"));
                widget.templateId = reader.GetInt32(reader.GetOrdinal("TemplateId"));
                widget.data = reader["DataValue_1"].ToString();
                widget.supplementalData = reader["DataValue_2"].ToString();

                //Insert into collection
                widgetRundownElements.Add(widget);

                //Next record
                count++;
            }

            //Actually not required here beacuase reader object will close query when done
            connection.Close();
        }


        //Function to update the topic rundown grid
        private void UpdateWidgetRundownGrid()
        {
            for (int i = 0; i < widgetRundownElements.Count; i++)
            {
                WidgetRundownGrid.RowCount = widgetRundownElements.Count;
                DataGridViewRow row = WidgetRundownGrid.Rows[i];
                row.Cells["Widget_Enabled"].Value = widgetRundownElements[i].enabled;
                row.Cells["Widget_Id"].Value = widgetRundownElements[i].pageid;
                row.Cells["Widget_TemplateId"].Value = widgetRundownElements[i].templateId;
                row.Cells["Widget_Data"].Value = widgetRundownElements[i].data;
                row.Cells["Widget_SupplementalData"].Value = widgetRundownElements[i].supplementalData;
            }

        }

        //Handler for change in iNews monitoring checkbox status - enable/disable save/load playlist butons accordingly
        private void iNewsMonitoringEnableCheckbox_CheckStateChanged(object sender, EventArgs e)
        {
            if (iNewsMonitoringEnableCheckbox.Checked)
            {
                SaveTopicPlaylistBtn.Visible = false;
                LoadTopicPlaylistBtn.Visible = false;
                iNewsMonitoringEnableCheckbox.BackColor = Color.Transparent;
                iNewsMonitoringEnableCheckbox.Text = "iNews Monitoring Enabled";
            }
            else
            {
                SaveTopicPlaylistBtn.Visible = true;
                LoadTopicPlaylistBtn.Visible = true;
                iNewsMonitoringEnableCheckbox.BackColor = Color.Yellow;
                iNewsMonitoringEnableCheckbox.Text = "iNews Monitoring Disabled";
            }
        }

        //Handler for main Wing In/Out button
        private void WingInBtn_Click(object sender, EventArgs e)
        {
            if (wingIn == false)
            {
                //Set button/indicator properties & set flag
                wingIn = true;
                MainWingControlPanel.BackColor = Color.Lime;
                WingInBtn.Text = "Take Wing Out (F1)";
                WingInBtn.Image = Fox_Sports_Playout_Controller.Properties.Resources.StatusAnnotations_Stop_32xLG;
                VizControl.SendDataPoolCommand("WING=ON");
            }
            else
            {
                //Set button/indicator properties & clear flag
                wingIn = false;
                MainWingControlPanel.BackColor = Color.Transparent;
                WingInBtn.Text = "Take Wing In (F1)";
                WingInBtn.Image = Fox_Sports_Playout_Controller.Properties.Resources.StatusAnnotations_Play_32xLG_color;
                VizControl.SendDataPoolCommand("WING=OFF");
            }
        }

        //Handler for create new topic rundown element
        private void CreateNewTopicBtn_Click(object sender, EventArgs e)
        {
            Form createNewTopicRecord = new frmTemplateDataEntry();
            createNewTopicRecord.ShowDialog();
        }

        //Handler for load topic playlist button
        private void LoadTopicPlaylistBtn_Click(object sender, EventArgs e)
        {
            Form loadTopicPlaylist = new frmPlaylistLoad();
            loadTopicPlaylist.ShowDialog();
        }

        //Handler for Change Skin button
        private void button5_Click(object sender, EventArgs e)
        {
            Form selectSkin = new frmSkinSelect();
            selectSkin.ShowDialog();
        }

        //Handler for time of day timer
        private void TimeOfDayTimer_Tick(object sender, EventArgs e)
        {
            DateTime currentDateTime = DateTime.Now;
            lblTimeOfDay.Text = currentDateTime.ToString();
        }

        private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
        {
        }

        private void frmMain_Activated(object sender, EventArgs e)
        {
        }

    }
}
