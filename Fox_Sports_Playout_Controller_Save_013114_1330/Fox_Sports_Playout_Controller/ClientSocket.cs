﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

// Asynchronous socket class developed by VDS based on MSDN examples
// Client Socket Example: http://msdn.microsoft.com/en-us/library/bew39x2a(v=vs.110).aspx
// Server Socket Example: http://msdn.microsoft.com/en-us/library/fx6588te(v=vs.110).aspx

namespace ClientSocket
{
    // Declare a state object for receiving data from the server socket
    public class StateObject
    {
        // Client socket
        public Socket workSocket = null;
        // Set the size of the receive buffer
        public const int BufferSize = 4096;
        // Setup the Receive buffer
        public byte[] buffer = new byte[BufferSize];
        // Create the StringBuilder object for the received data string
        public StringBuilder sb = new StringBuilder();
    }

    //Declare the async client socket class
    public class AsynchronousClient
    {
        #region Constants & Default Values
        const int DEFAULTTIMEOUT = 5000; //Default to 5 seconds on all timeouts
        const int RECONNECTINTERVAL = 2000; //Default to 2 seconds for the reconnect attempt rate
        #endregion

        #region Components, Timers, Events, Delegates
        // Declare client socket component
        private Socket client;

        // Timer used to detect receive timeouts
        private System.Timers.Timer tmrReceiveTimeout = new System.Timers.Timer();
        private System.Timers.Timer tmrSendTimeout = new System.Timers.Timer();
        private System.Timers.Timer tmrConnectTimeout = new System.Timers.Timer();

        // Define delegates & events
        // Data received event
        public delegate void delDataReceived(AsynchronousClient sender, object data);
        public event delDataReceived DataReceived;

        // Connection status changed event
        public delegate void delConnectionStatusChanged(AsynchronousClient sender, ConnectionStatus status);
        public event delConnectionStatusChanged ConnectionStatusChanged;

        //Object for thread syncing
        object syncLock = new object();

        //Endpoint for connection to server socket
        private IPEndPoint remoteEndPoint;
        #endregion

        #region Enumerations
        // Define connection states
        public enum ConnectionStatus
        {
            NeverConnected,
            Connecting,
            Connected,
            AutoReconnecting,
            DisconnectedByUser,
            DisconnectedByHost,
            ConnectFail_Timeout,
            ReceiveFail_Timeout,
            SendFail_Timeout,
            SendFail_NotConnected,
            Error
        }
        #endregion

        #region Fields 
        private static String response = String.Empty;
        private ConnectionStatus connectionStatus;
        #endregion

        #region Properties
        public ConnectionStatus ConnectionState
        {
            get
            {
                return connectionStatus;
            }
            private set
            {
                bool raiseEvent = (value != connectionStatus);
                connectionStatus = value;
                // Raise status changed event on a change of status state
                if (ConnectionStatusChanged != null && raiseEvent)
                    ConnectionStatusChanged.BeginInvoke(this, connectionStatus, new AsyncCallback(cbChangeConnectionStateComplete), this);
            }
        }

        // Reconnect timer interval
        public int ReconnectInterval { get; set; }

        // Set true to autoreconnect at the given reconnection interval after a remote host closes the connection
        public bool AutoReconnect { get; set; }

        // Time to wait after a connection is attempted before a timeout event occurs
        public int ConnectTimeout
        {
            get
            {
                return (int)tmrConnectTimeout.Interval;
            }
            set
            {
                tmrConnectTimeout.Interval = (double)value;
            }
        }

        // Time to wait after a send operation is attempted before a timeout event occurs
        public int SendTimeout
        {
            get
            {
                return (int)tmrSendTimeout.Interval;
            }
            set
            {
                tmrSendTimeout.Interval = (double)value;
            }
        }

        // Time to wait after a receive operation is attempted before a timeout event occurs
        public int ReceiveTimeout
        {
            get
            {
                return (int)tmrReceiveTimeout.Interval;
            }
            set
            {
                tmrReceiveTimeout.Interval = (double)value;
            }
        }

        // Read-only synchronizing object for asynchronous operations
        public object SyncLock
        {
            get
            {
                return syncLock;
            }
        }
        #endregion

        #region Public Methods
        // Constructor
        public AsynchronousClient(string ipAddress, Int32 port)
        {
            // Establish the remote endpoint for the socket
            IPAddress _ipAddress = IPAddress.Parse(ipAddress);
            remoteEndPoint = new IPEndPoint(_ipAddress, port);
            
            // Create the TCP/IP socket
            client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            // Disable Nagle's algorithm to allow for small packets
            client.NoDelay = true;

            // Setup timers
            ReceiveTimeout = DEFAULTTIMEOUT;
            SendTimeout = DEFAULTTIMEOUT;
            ConnectTimeout = DEFAULTTIMEOUT;
            ReconnectInterval = RECONNECTINTERVAL;

            tmrReceiveTimeout.AutoReset = false;
            tmrReceiveTimeout.Elapsed += new System.Timers.ElapsedEventHandler(tmrReceiveTimeout_Elapsed);

            tmrConnectTimeout.AutoReset = false;
            tmrConnectTimeout.Elapsed += new System.Timers.ElapsedEventHandler(tmrConnectTimeout_Elapsed);

            tmrSendTimeout.AutoReset = false;
            tmrSendTimeout.Elapsed += new System.Timers.ElapsedEventHandler(tmrSendTimeout_Elapsed);

            // Set initial connection state
            ConnectionState = ConnectionStatus.NeverConnected;
        }

        // Connect to the host
        public void ConnectClient()
        {
            // Connect to the remote device
            if (client != null)
            {
                // Set the connection state
                if (this.ConnectionState == ConnectionStatus.Connected)
                    return;
                this.ConnectionState = ConnectionStatus.Connecting;

                Thread.Sleep(10);

                // Start connection timeout timer
                tmrConnectTimeout.Start();

                // Connect to the remote endpoint
                client.BeginConnect(remoteEndPoint, new AsyncCallback(cbConnect), client);
            }
        }

        // Disconnect from the remote host
        public void DisconnectClient()
        {
            if ((this.ConnectionState != ConnectionStatus.Connected) & (this.ConnectionState != ConnectionStatus.Connecting))
                return;

            // Halt timers
            tmrReceiveTimeout.Stop();
            tmrSendTimeout.Stop();
            tmrConnectTimeout.Stop();

            // Disconnect
            client.BeginDisconnect(true, new AsyncCallback(cbDisconnectComplete), this.client);
        }

        // Method for sending data to the host/server
        public void Send(String data)
        {
            if (connectionStatus != ConnectionStatus.Connected)
            {
                connectionStatus = ConnectionStatus.SendFail_NotConnected;
                return;
            }

            // Convert the string data to byte data using ASCII encoding
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Setup error instance and start timer
            SocketError err = new SocketError();
            tmrSendTimeout.Start();

            // Begin sending the data to the remote device
            client.BeginSend(byteData, 0, byteData.Length, 0, out err, new AsyncCallback(cbSendComplete), client);

            // If error, invoke DisconnectByHost => will attempt re-connect if enabled
            if (err != SocketError.Success)
            {
                Action doDisconnectByHost = new Action(DisconnectByHost);
                doDisconnectByHost.Invoke();
            }
        }

        // Destructor
        public void Dispose()
        {
            // Close and dispose of the socket
            this.client.Close();
            this.client.Dispose();
        }
        #endregion

        #region Private Methods & Event Handlers
        // Timer event handlers
        void tmrSendTimeout_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            this.ConnectionState = ConnectionStatus.SendFail_Timeout;
            DisconnectByHost();
        }
        void tmrReceiveTimeout_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            this.ConnectionState = ConnectionStatus.ReceiveFail_Timeout;
            DisconnectByHost();
        }
        void tmrConnectTimeout_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            ConnectionState = ConnectionStatus.ConnectFail_Timeout;
            DisconnectByHost();
        }

        // Method to change state and try auto-reconnect if disconnected by host
        private void DisconnectByHost()
        {
            this.ConnectionState = ConnectionStatus.DisconnectedByHost;

            tmrSendTimeout.Stop();
            tmrReceiveTimeout.Stop();
            
            if (AutoReconnect)
            {
                Reconnect();
            }
        }

        // Reconnect to host
        private void Reconnect()
        {
            if (this.ConnectionState == ConnectionStatus.Connected)
                return;

            this.ConnectionState = ConnectionStatus.AutoReconnecting;
            try
            {
                this.client.BeginDisconnect(true, new AsyncCallback(cbDisconnectByHostComplete), this.client);
            }
            catch { }
        }
        #endregion

        #region Callbacks
        // Aysnc event callback for connection complete
        private void cbConnect(IAsyncResult asyncResult)
        {
            // Retrieve the socket from the state object
            Socket _client = (Socket)asyncResult.AsyncState;

            if (asyncResult == null)
                throw new InvalidOperationException("Invalid IAsyncResult - Could not interpret as a socket object");

            // If not connected, check if auto-reconnect enabled and if so, reconnect
            if (!_client.Connected)
            {
                if (AutoReconnect)
                {
                    // Delay and try reconnecting
                    System.Threading.Thread.Sleep(ReconnectInterval);
                    Action reconnect = new Action(ConnectClient);
                    reconnect.Invoke();
                    return;
                }
                else
                {
                    return;
                }
            }

            // Complete the connection
            _client.EndConnect(asyncResult);

            Thread.Sleep(10);

            //Invoke callback for connection complete
            var callBack = new Action(cbClientConnectComplete);
            callBack.Invoke();
        }

        // Callback for setting status upon successful connection
        private void cbClientConnectComplete()
        {
            if (client.Connected == true)
            {
                // Reset timer & set state
                tmrConnectTimeout.Stop();
                this.ConnectionState = ConnectionStatus.Connected;

                // Create the state object.
                StateObject state = new StateObject();
                state.workSocket = client;

                // Start listening for data
                client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(cbDataReceived), state);
            }
            else
            {
                this.ConnectionState = ConnectionStatus.Error;
            }
        }

        // Callback for client disconnect
        private void cbDisconnectComplete(IAsyncResult result)
        {
            var client = result.AsyncState as Socket;
            if (client == null)
                throw new InvalidOperationException("Invalid IAsyncResult - Could not interpret as a socket object");

            client.EndDisconnect(result);
            this.ConnectionState = ConnectionStatus.DisconnectedByUser;
        }

        private void cbDisconnectByHostComplete(IAsyncResult asyncResult)
        {
            // Retrieve the socket from the state object
            Socket _client = (Socket)asyncResult.AsyncState;

            if (_client == null)
                throw new InvalidOperationException("Invalid IAsyncResult - Could not interpret as a socket object");

            _client.EndDisconnect(asyncResult);

            // Try reconnecting by invoking ConnectClient method
            if (this.AutoReconnect)
            {
                Action doConnect = new Action(ConnectClient);
                doConnect.Invoke();
                return;
            }
        }

        // Async callback event for send method
        private void cbSendComplete(IAsyncResult asyncResult)
        {
            // Retrieve the socket from the state object
            Socket _client = (Socket)asyncResult.AsyncState;

            if (_client == null)
                throw new InvalidOperationException("Invalid IAsyncResult - Could not interpret as a socket object");

            SocketError err = new SocketError();

            // Complete sending the data to the remote device
            int bytesSent = _client.EndSend(asyncResult, out err);

            if (err != SocketError.Success)
            {

            }
            else
            {
                lock (SyncLock)
                {
                    tmrSendTimeout.Stop();
                }
            }
        }

        // Aysnc event callback for data received
        private void cbDataReceived(IAsyncResult asyncResult)
        {
            // Retrieve the state object and the client socket from the asynchronous state object
            StateObject state = (StateObject)asyncResult.AsyncState;
            Socket _client = state.workSocket;

            if (_client == null)
                throw new InvalidOperationException("Invalid IAsyncResult - Could not interpret as a socket");

            SocketError err = new SocketError();

            // Read data from the remote device
            int bytesRead = _client.EndReceive(asyncResult, out err);

            if (bytesRead == 0 || err != SocketError.Success)
            {
                // All the data has arrived; put it in response
                if (state.sb.Length > 1)
                {
                    response = state.sb.ToString();
                }
                lock (SyncLock)
                {
                    //tmrReceiveTimeout.Start();
                    tmrReceiveTimeout.Stop();
                    return;
                }
            }
            // Data received
            else
            {
                lock (SyncLock)
                {
                    tmrReceiveTimeout.Stop();
                }
                // There might be more data, so store the data received so far
                state.sb.Append(Encoding.ASCII.GetString(state.buffer, 0, bytesRead));

                response = state.sb.ToString();

                // Fire event for data received
                if (this.DataReceived != null)
                    this.DataReceived.BeginInvoke(this, response, new AsyncCallback(cbDataRecievedCallbackComplete), this);

                // Get the rest of the data
                _client.BeginReceive(state.buffer, 0, StateObject.BufferSize, 0, new AsyncCallback(cbDataReceived), state);
            }
        }

        // Callback for data received event from host
        private void cbDataRecievedCallbackComplete(IAsyncResult asyncResult)
        {
            var r = asyncResult.AsyncState as AsynchronousClient;
            if (r == null)
                throw new InvalidOperationException("Invalid IAsyncResult - Could not interpret as AsynchronousClient object");

            r.DataReceived.EndInvoke(asyncResult);
        }

        // Callback for change in connection state
        private void cbChangeConnectionStateComplete(IAsyncResult result)
        {
            var r = result.AsyncState as AsynchronousClient;
            if (r == null)
                throw new InvalidOperationException("Invalid IAsyncResult - Could not interpret as an Async Client object");

            r.ConnectionStatusChanged.EndInvoke(result);
        }
        #endregion
    }   
}