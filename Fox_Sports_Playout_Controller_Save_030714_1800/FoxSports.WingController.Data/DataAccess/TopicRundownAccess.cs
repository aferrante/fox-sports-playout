﻿// Class for database access to load/store collections
// Instantiated by TopicRundownCollection class
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoxSports.WingController.Data.DataAccess
{
    using System;
    using System.Data;
    using System.Data.SqlClient;

    using FoxSports.WingController.Data.DataModel;

    /// <summary>
    /// Class for handling database access for topic rundown
    /// </summary>
    public class TopicRundownAccess
    {
        #region Properties and Members
        public string WingDBConnectionString { get; set; }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method to get the specified Topic Rundown from SQL DB and pass it back to the logic layer as a DataTable
        /// </summary>
        public DataTable GetTopicRundown(double topicRundownID)
        {
            DataTable dataTable = new DataTable();

            try
            {
                // Instantiate the connection
                using (SqlConnection connection = new SqlConnection(WingDBConnectionString))
                {
                    // Create the command and set its properties
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
                        {
                            cmd.CommandText = "sp_GetTopicRundown " + Convert.ToString(topicRundownID);
                            sqlDataAdapter.SelectCommand = cmd;
                            sqlDataAdapter.SelectCommand.Connection = connection;
                            sqlDataAdapter.SelectCommand.CommandType = CommandType.Text;

                            // Fill the datatable from adapter
                            sqlDataAdapter.Fill(dataTable);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return dataTable;
        }

        /// <summary>
        /// Method to save current topic rundown to DB
        /// </summary>
        public void SaveTopicRundown(DataTable topicRundown)
        {
            if (topicRundown.Rows.Count > 0)
            {
                try
                {
                    // Instantiate the connection
                    using (SqlConnection connection = new SqlConnection(WingDBConnectionString))
                    {
                        connection.Open();
                        
                        using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
                        {
                            // Create the command and set its properties
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                // Save out the top-level metadata for the stack
                                cmd.CommandText = "sp_SetTopicRundown @RundownID, @RundownName, @LastUpdated";
                                // Pull values from first row of datatable
                                DataRow row = topicRundown.Rows[0];

                                // Delete the rundown if it is already saved in the DB
                                DeleteTopicRundown(Convert.ToDouble(row["RundownID"]));

                                // Save back out
                                cmd.Parameters.Add("@RundownID", SqlDbType.Float).Value = row["RundownID"];
                                cmd.Parameters.Add("@RundownName", SqlDbType.Text).Value = row["RundownName"]; 
                                cmd.Parameters.Add("@LastUpdated", SqlDbType.DateTime).Value = DateTime.Now;
                                sqlDataAdapter.SelectCommand = cmd;
                                sqlDataAdapter.SelectCommand.Connection = connection;
                                sqlDataAdapter.SelectCommand.CommandType = CommandType.Text;

                                // Execute stored proc to store top-level metadata
                                sqlDataAdapter.SelectCommand.ExecuteNonQuery();
                            }

                            // Iterate through data table
                            foreach (DataRow row in topicRundown.Rows)
                            {
                                using (SqlCommand cmd = new SqlCommand())
                                {
                                    cmd.CommandText = "sp_SetTopicRundownElement @RundownID, @PageID, @Enabled, @TemplateID, " +
                                        "@Slug, @LongSlug";
                                    cmd.Parameters.Add("@RundownID", SqlDbType.Float).Value = row["RundownID"]; ;
                                    cmd.Parameters.Add("@PageID", SqlDbType.Float).Value = row["PageID"];
                                    cmd.Parameters.Add("@Enabled", SqlDbType.Bit).Value = row["Enabled"];
                                    cmd.Parameters.Add("@TemplateID", SqlDbType.Float).Value = row["TemplateID"];
                                    cmd.Parameters.Add("@Slug", SqlDbType.Text).Value = row["Slug"];
                                    cmd.Parameters.Add("@LongSlug", SqlDbType.Text).Value = row["LongSlug"];

                                    sqlDataAdapter.SelectCommand = cmd;
                                    sqlDataAdapter.SelectCommand.Connection = connection;
                                    sqlDataAdapter.SelectCommand.CommandType = CommandType.Text;

                                    // Execute stored proc to store top-level metadata
                                    sqlDataAdapter.SelectCommand.ExecuteNonQuery();
                                }
                            }
                        }
                        connection.Close();
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Method to delete specified topic rundown from DB
        /// </summary>
        public void DeleteTopicRundown(double topicRundownID)
        {
            try
            {
                // Instantiate the connection
                using (SqlConnection connection = new SqlConnection(WingDBConnectionString))
                {
                    connection.Open();

                    // Create the command and set its properties
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
                        {
                            cmd.CommandText = "sp_DeleteTopicRundown " + Convert.ToString(topicRundownID);
                            sqlDataAdapter.SelectCommand = cmd;
                            sqlDataAdapter.SelectCommand.Connection = connection;
                            sqlDataAdapter.SelectCommand.CommandType = CommandType.Text;

                            // Execute stored proc
                            sqlDataAdapter.SelectCommand.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
