﻿// Definition for class of Viz control functions; uses Asynchronous socket class defined in ClientSocket
// M. Dilworth  VDS  01/28/14
using System;
using System.Net;
using System.Net.Sockets;
using FoxSports.WingController.VizIntf.ClientSocket;

// Subclass of TCPClientWrapper class for Viz communications
namespace FoxSports.WingController.VizIntf.VizControlFunctions
{
    public class VizControlPort : TCPClientWrapper
    {
        #region Public Methods

        // Constructor
        public VizControlPort(IPAddress ip, int port, bool autoreconnect = true)
            : base(ip, port, autoreconnect)
        {
        }

        // Function to load Viz scene
        public void LoadScene(String SceneName)
        {
            SendStringToViz("0 RENDERER*MAIN_LAYER SET_OBJECT SCENE*" + SceneName);
        }

        // Function to unload Viz scene
        public void UnloadScene(String SceneName)
        {
            SendStringToViz("0 SCENE*" + SceneName + " CLOSE");
        }

        // Function to send Datapool command
        public void SendDataPoolCommand(String Data)
        {
            SendStringToViz("0 RENDERER*FUNCTION*DataPool*Data SET " + Data);
        }
        #endregion

        #region Private Methods
        // Simple function to just always ensure we send the null character at the end of the string as required by Viz
        private void SendStringToViz(String Command)
        {
            // Append null character required for Viz & send
            char nullChar = '\0';
            this.Send(Command + nullChar);
        }
        #endregion
    }
}