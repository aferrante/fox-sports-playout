﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fox_Sports_Playout_Controller
{
    public partial class frmTemplateDataEntry : Form
    {
        public frmTemplateDataEntry()
        {
            InitializeComponent();
            TemplateSelectDropdown.SelectedIndex = 0;

            //Fill with dummy data
            TemplateFieldsGrid.RowCount = 2;
            DataGridViewRow row1 = TemplateFieldsGrid.Rows[0];
            row1.Cells["Template_Field_Name"].Value = "Slug";
            row1.Cells["Template_Field_Value"].Value = "Cutler";
            DataGridViewRow row2 = TemplateFieldsGrid.Rows[1];
            row2.Cells["Template_Field_Name"].Value = "Long Slug";
            row2.Cells["Template_Field_Value"].Value = "Looking Sharp";
        }

        private void TemplateSelectDropdown_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
