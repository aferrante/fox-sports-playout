﻿using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Configuration;
using FoxSports.WingController.Data;

namespace FoxSports.WingController.Data.DataAccess
{
    using System.Configuration;

    // ConnectionAccess class
    public abstract class ConnectionAccess
    {
        // Gets connection string
        protected string ConnectionString
        {
            get
            {
                return ConfigurationManager.ConnectionStrings["wingDBConnectionString"].ToString(); 
            }
        }
    }
}
