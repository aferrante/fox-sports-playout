﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoxSports.WingController.VizIntf.VizCommandDefs
{
    public class VizCommandDefs
    {
        public const string WingOn = "WING=ON";
        public const string WingOff = "WING=OFF";
    }
}
