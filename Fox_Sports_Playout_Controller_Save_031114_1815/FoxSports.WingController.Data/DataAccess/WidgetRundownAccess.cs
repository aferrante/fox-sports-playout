﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoxSports.WingController.Data.DataAccess
{
    using System;
    using System.Data;
    using System.Data.SqlClient;

    using FoxSports.WingController.Data.DataModel;

    /// <summary>
    /// Class for handling database access for widget rundown
    /// </summary>
    public class WidgetRundownAccess
    {
        #region Properties and Members
        public string WingDBConnectionString { get; set; }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method to get the specified Widget Rundown from SQL DB and pass it back to the logic layer as a DataTable
        /// </summary>
        public DataTable GetWidgetRundown(double widgetRundownID)
        {
            DataTable dataTable = new DataTable();

            try
            {
                // Instantiate the connection
                using (SqlConnection connection = new SqlConnection(WingDBConnectionString))
                {
                    // Create the command and set its properties
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
                        {
                            cmd.CommandText = "sp_GetWidgetRundown " + Convert.ToString(widgetRundownID);
                            sqlDataAdapter.SelectCommand = cmd;
                            sqlDataAdapter.SelectCommand.Connection = connection;
                            sqlDataAdapter.SelectCommand.CommandType = CommandType.Text;

                            // Fill the datatable from adapter
                            sqlDataAdapter.Fill(dataTable);
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return dataTable;
        }

        /// <summary>
        /// Method to save current widget rundown to DB
        /// </summary>
        public void SaveWidgetRundown(DataTable widgetRundown)
        {
            if (widgetRundown.Rows.Count > 0)
            {
                try
                {
                    // Instantiate the connection
                    using (SqlConnection connection = new SqlConnection(WingDBConnectionString))
                    {
                        connection.Open();

                        using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
                        {
                            // Create the command and set its properties
                            using (SqlCommand cmd = new SqlCommand())
                            {
                                // Save out the top-level metadata for the stack
                                cmd.CommandText = "sp_SetWidgetRundown @RundownID, @RundownName, @LastUpdated";
                                // Pull values from first row of datatable
                                DataRow row = widgetRundown.Rows[0];

                                // Delete the rundown if it is already saved in the DB
                                DeleteWidgetRundown(Convert.ToDouble(row["RundownID"]));

                                // Save back out
                                cmd.Parameters.Add("@RundownID", SqlDbType.Float).Value = row["RundownID"];
                                cmd.Parameters.Add("@RundownName", SqlDbType.Text).Value = row["RundownName"];
                                cmd.Parameters.Add("@LastUpdated", SqlDbType.DateTime).Value = DateTime.Now;
                                sqlDataAdapter.SelectCommand = cmd;
                                sqlDataAdapter.SelectCommand.Connection = connection;
                                sqlDataAdapter.SelectCommand.CommandType = CommandType.Text;

                                // Execute stored proc to store top-level metadata
                                sqlDataAdapter.SelectCommand.ExecuteNonQuery();
                            }

                            // Iterate through data table
                            foreach (DataRow row in widgetRundown.Rows)
                            {
                                using (SqlCommand cmd = new SqlCommand())
                                {
                                    cmd.CommandText = "sp_SetWidgetRundownElement @RundownID, @PageID, @Enabled, @TemplateID, " +
                                        "@TitleSize, @TitleText_1, @TitleText_2, " + 
                                        "@DataType_1, @DataValue_1, " +
                                        "@DataType_2, @DataValue_2, " +
                                        "@DataType_3, @DataValue_3, " +
                                        "@DataType_4, @DataValue_4, " +
                                        "@DataType_5, @DataValue_5, " +
                                        "@DataType_6, @DataValue_6, " +
                                        "@DataType_7, @DataValue_7, " +
                                        "@DataType_8, @DataValue_8"; 
                                    cmd.Parameters.Add("@RundownID", SqlDbType.Float).Value = row["RundownID"]; ;
                                    cmd.Parameters.Add("@PageID", SqlDbType.Float).Value = row["PageID"];
                                    cmd.Parameters.Add("@Enabled", SqlDbType.Bit).Value = row["Enabled"];
                                    cmd.Parameters.Add("@TemplateID", SqlDbType.Float).Value = row["TemplateID"];
                                    cmd.Parameters.Add("@TitleSize", SqlDbType.Int).Value = row["TitleSize"];
                                    cmd.Parameters.Add("@TitleText_1", SqlDbType.Text).Value = row["TitleText_1"];
                                    cmd.Parameters.Add("@TitleText_2", SqlDbType.Text).Value = row["TitleText_2"];
                                    cmd.Parameters.Add("@DataType_1", SqlDbType.Int).Value = row["DataType_1"];
                                    cmd.Parameters.Add("@DataValue_1", SqlDbType.Text).Value = row["DataValue_1"];
                                    cmd.Parameters.Add("@DataType_2", SqlDbType.Int).Value = row["DataType_2"];
                                    cmd.Parameters.Add("@DataValue_2", SqlDbType.Text).Value = row["DataValue_2"];
                                    cmd.Parameters.Add("@DataType_3", SqlDbType.Int).Value = row["DataType_3"];
                                    cmd.Parameters.Add("@DataValue_3", SqlDbType.Text).Value = row["DataValue_3"];
                                    cmd.Parameters.Add("@DataType_4", SqlDbType.Int).Value = row["DataType_4"];
                                    cmd.Parameters.Add("@DataValue_4", SqlDbType.Text).Value = row["DataValue_4"];
                                    cmd.Parameters.Add("@DataType_5", SqlDbType.Int).Value = row["DataType_5"];
                                    cmd.Parameters.Add("@DataValue_5", SqlDbType.Text).Value = row["DataValue_5"];
                                    cmd.Parameters.Add("@DataType_6", SqlDbType.Int).Value = row["DataType_6"];
                                    cmd.Parameters.Add("@DataValue_6", SqlDbType.Text).Value = row["DataValue_6"];
                                    cmd.Parameters.Add("@DataType_7", SqlDbType.Int).Value = row["DataType_7"];
                                    cmd.Parameters.Add("@DataValue_7", SqlDbType.Text).Value = row["DataValue_7"];
                                    cmd.Parameters.Add("@DataType_8", SqlDbType.Int).Value = row["DataType_8"];
                                    cmd.Parameters.Add("@DataValue_8", SqlDbType.Text).Value = row["DataValue_8"];
                                    sqlDataAdapter.SelectCommand = cmd;
                                    sqlDataAdapter.SelectCommand.Connection = connection;
                                    sqlDataAdapter.SelectCommand.CommandType = CommandType.Text;

                                    // Execute stored proc to store top-level metadata
                                    sqlDataAdapter.SelectCommand.ExecuteNonQuery();
                                }
                            }
                        }
                        connection.Close();
                    }
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }

        /// <summary>
        /// Method to delete specified widget rundown from DB
        /// </summary>
        public void DeleteWidgetRundown(double widgetRundownID)
        {
            try
            {
                // Instantiate the connection
                using (SqlConnection connection = new SqlConnection(WingDBConnectionString))
                {
                    connection.Open();

                    // Create the command and set its properties
                    using (SqlCommand cmd = new SqlCommand())
                    {
                        using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
                        {
                            cmd.CommandText = "sp_DeleteWidgetRundown " + Convert.ToString(widgetRundownID);
                            sqlDataAdapter.SelectCommand = cmd;
                            sqlDataAdapter.SelectCommand.Connection = connection;
                            sqlDataAdapter.SelectCommand.CommandType = CommandType.Text;

                            // Execute stored proc
                            sqlDataAdapter.SelectCommand.ExecuteNonQuery();
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
