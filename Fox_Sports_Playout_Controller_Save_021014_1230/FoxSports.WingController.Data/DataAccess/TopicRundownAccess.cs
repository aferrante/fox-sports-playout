﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoxSports.WingController.Data.DataAccess
{
    using System;
    using System.Data;
    using System.Data.SqlClient;

    using FoxSports.WingController.Data.DataModel;

    public class TopicRundownAccess
    {
        public string WingDBConnectionString { get; set; }

        // Method to get specified Topic Rundown from SQL DB
        public DataTable GetTopicRundown(double topicRundownID)
        {
            DataTable dataTable = new DataTable();

            using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
            {
                // Create the command and set its properties
                sqlDataAdapter.SelectCommand = new SqlCommand();
                sqlDataAdapter.SelectCommand.Connection = new SqlConnection(WingDBConnectionString);
                sqlDataAdapter.SelectCommand.CommandType = CommandType.Text;

                // Assign the SQL to the command object
                sqlDataAdapter.SelectCommand.CommandText = "sp_GetTopicRundown " + Convert.ToString(topicRundownID);

                try
                {
                    // Fill the datatable from adapter
                    sqlDataAdapter.Fill(dataTable);
                }
                catch (Exception) 
                {
                   throw;
                }

                // Clean up
                //sqlDataAdapter.SelectCommand.Connection.Close();
                //sqlDataAdapter.SelectCommand.Connection.Dispose();
                //sqlDataAdapter.SelectCommand.Dispose();
            }
            return dataTable;
        }

        // Method to save current topic rundown to DB
        public void SaveTopicRundown(double topicRundownID, DataTable topicRundown)
        {
            using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
            {
                // Save the top level metadata for the rundown
                // Create the command and set its properties
                sqlDataAdapter.SelectCommand = new SqlCommand();
                sqlDataAdapter.SelectCommand.Connection = new SqlConnection(WingDBConnectionString);
                sqlDataAdapter.SelectCommand.CommandType = CommandType.Text;

                // Execute stored proc to store top-level metadata
                sqlDataAdapter.SelectCommand.CommandText = "sp_SetTopicRundown " + Convert.ToString(topicRundownID);
                try
                {
                    // Execute the stored proc
                    sqlDataAdapter.SelectCommand.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    throw;
                }

                // Execute stored proc to save individual rundown elements
            }
        }
        
        // Method to delete specified topic rundown from DB
        public void DeleteTopicRundown(double topicRundownID)
        {
            using (SqlDataAdapter sqlDataAdapter = new SqlDataAdapter())
            {
                // Create the command and set its properties
                sqlDataAdapter.SelectCommand = new SqlCommand();
                sqlDataAdapter.SelectCommand.Connection = new SqlConnection(WingDBConnectionString);
                sqlDataAdapter.SelectCommand.CommandType = CommandType.Text;

                // Assign the SQL to the command object
                sqlDataAdapter.SelectCommand.CommandText = "sp_DeleteTopicRundown " + Convert.ToString(topicRundownID);

                try
                {
                    // Execute the stored proc
                    sqlDataAdapter.SelectCommand.ExecuteNonQuery();
                }
                catch (Exception)
                {
                    throw;
                }

                // Clean up
                sqlDataAdapter.SelectCommand.Connection.Close();
                sqlDataAdapter.SelectCommand.Connection.Dispose();
                sqlDataAdapter.SelectCommand.Dispose();
            }
        }
    }
}
