﻿// Definition for class used for widget collection operations
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FoxSports.WingController.Data.DataAccess;
using FoxSports.WingController.Data.DataModel;
using System.Data.SqlClient;
using System.ComponentModel;

namespace FoxSports.WingController.Logic.WidgetCollection
{
    /// <summary>
    /// Class for operations related to the widget rundown
    /// </summary>
    public class WidgetRundownCollection
    {
        #region Properties and Members
        public BindingList<WidgetRundownElementModel> widgetRundownElements;
        public string WingDBConnectionString { get; set; }
        #endregion

        #region Public Properties
        public string WidgetRundownName { get; set; }
        #endregion

        #region Public Methods
        // Constructor - instantiates list collection
        public WidgetRundownCollection()
        {
            // Create list
            widgetRundownElements = new BindingList<WidgetRundownElementModel>();
        }

        /// <summary>
        /// Get the widget rundown from the SQL DB; clears out existing collection first
        /// </summary>
        public BindingList<WidgetRundownElementModel> GetWidgetRundownCollection(double widgetRundownID)
        {
            DataTable dataTable;

            // Clear out the current collection
            widgetRundownElements.Clear();

            try
            {
                WidgetRundownAccess widgetRundown = new WidgetRundownAccess();
                widgetRundown.WingDBConnectionString = WingDBConnectionString;
                dataTable = widgetRundown.GetWidgetRundown(widgetRundownID);

                foreach (DataRow row in dataTable.Rows)
                {
                    WidgetRundownName = row["Rundownname"].ToString();
                    var newWidget = new WidgetRundownElementModel()
                    {
                        rundownId = Convert.ToDouble(row["RundownId"]),
                        pageId = Convert.ToInt32(row["PageId"]),
                        enabled = Convert.ToBoolean(row["Enabled"]),
                        templateId = Convert.ToInt32(row["TemplateID"]),
                        titleSize = Convert.ToInt32(row["TitleSize"]),
                        titleText_1 = row["TitleText_1"].ToString(),
                        titleText_2 = row["TitleText_2"].ToString(),
                        dataType_1 = Convert.ToInt32(row["DataType_1"]),
                        dataValue_1 = row["DataValue_1"].ToString(),
                        dataType_2 = Convert.ToInt32(row["DataType_2"]),
                        dataValue_2 = row["DataValue_2"].ToString() ?? "",
                        dataType_3 = Convert.ToInt32(row["DataType_3"]),
                        dataValue_3 = row["DataValue_3"].ToString(),
                        dataType_4 = Convert.ToInt32(row["DataType_4"]),
                        dataValue_4 = row["DataValue_4"].ToString(),
                        dataType_5 = Convert.ToInt32(row["DataType_5"]),
                        dataValue_5 = row["DataValue_5"].ToString(),
                        dataType_6 = Convert.ToInt32(row["DataType_6"]),
                        dataValue_6 = row["DataValue_6"].ToString(),
                        dataType_7 = Convert.ToInt32(row["DataType_7"]),
                        dataValue_7 = row["DataValue_7"].ToString(),
                        dataType_8 = Convert.ToInt32(row["DataType_8"]),
                        dataValue_8 = row["DataValue_8"].ToString()
                    };
                    widgetRundownElements.Add(newWidget);
                }
            }
            catch
            {
                throw;
            }
            // Return 
            return widgetRundownElements;
        }

        /// <summary>
        /// Save the topic list to the SQL DB; clears out existing collection first
        /// </summary>
        public void SaveWidgetRundownCollection(BindingList<WidgetRundownElementModel> widgetRundownElements)
        {
            if (widgetRundownElements.Count > 0)
            {
                DataTable dataTable = new DataTable();
                dataTable.Columns.Add("RundownName", typeof(String));
                dataTable.Columns.Add("RundownID", typeof(Double));
                dataTable.Columns.Add("PageID", typeof(Int32));
                dataTable.Columns.Add("TemplateID", typeof(Int32));
                dataTable.Columns.Add("Enabled", typeof(Boolean));
                dataTable.Columns.Add("TitleSize", typeof(Int32));
                dataTable.Columns.Add("TitleText_1", typeof(String));
                dataTable.Columns.Add("TitleText_2", typeof(String));
                dataTable.Columns.Add("DataType_1", typeof(Int32));
                dataTable.Columns.Add("DataValue_1", typeof(String));
                dataTable.Columns.Add("DataType_2", typeof(Int32));
                dataTable.Columns.Add("DataValue_2", typeof(String));
                dataTable.Columns.Add("DataType_3", typeof(Int32));
                dataTable.Columns.Add("DataValue_3", typeof(String));
                dataTable.Columns.Add("DataType_4", typeof(Int32));
                dataTable.Columns.Add("DataValue_4", typeof(String));
                dataTable.Columns.Add("DataType_5", typeof(Int32));
                dataTable.Columns.Add("DataValue_5", typeof(String));
                dataTable.Columns.Add("DataType_6", typeof(Int32));
                dataTable.Columns.Add("DataValue_6", typeof(String));
                dataTable.Columns.Add("DataType_7", typeof(Int32));
                dataTable.Columns.Add("DataValue_7", typeof(String));
                dataTable.Columns.Add("DataType_8", typeof(Int32));
                dataTable.Columns.Add("DataValue_8", typeof(String));

                for (int i = 0; i < widgetRundownElements.Count; i++)
                {
                    DataRow widgetElement = dataTable.NewRow();
                    widgetElement["RundownName"] = WidgetRundownName;
                    widgetElement["RundownID"] = widgetRundownElements[i].rundownId;
                    widgetElement["PageID"] = widgetRundownElements[i].pageId;
                    widgetElement["TemplateID"] = widgetRundownElements[i].templateId;
                    widgetElement["Enabled"] = widgetRundownElements[i].enabled;
                    widgetElement["TitleSize"] = widgetRundownElements[i].titleSize;
                    widgetElement["TitleText_1"] = widgetRundownElements[i].titleText_1;
                    widgetElement["TitleText_2"] = widgetRundownElements[i].titleText_2;
                    widgetElement["DataType_1"] = widgetRundownElements[i].dataType_1;
                    widgetElement["DataValue_1"] = widgetRundownElements[i].dataValue_1;
                    widgetElement["DataType_2"] = widgetRundownElements[i].dataType_2;
                    widgetElement["DataValue_2"] = widgetRundownElements[i].dataValue_2;
                    widgetElement["DataType_3"] = widgetRundownElements[i].dataType_3;
                    widgetElement["DataValue_3"] = widgetRundownElements[i].dataValue_3;
                    widgetElement["DataType_4"] = widgetRundownElements[i].dataType_4;
                    widgetElement["DataValue_4"] = widgetRundownElements[i].dataValue_4;
                    widgetElement["DataType_5"] = widgetRundownElements[i].dataType_5;
                    widgetElement["DataValue_5"] = widgetRundownElements[i].dataValue_5;
                    widgetElement["DataType_6"] = widgetRundownElements[i].dataType_6;
                    widgetElement["DataValue_6"] = widgetRundownElements[i].dataValue_6;
                    widgetElement["DataType_7"] = widgetRundownElements[i].dataType_7;
                    widgetElement["DataValue_7"] = widgetRundownElements[i].dataValue_7;
                    widgetElement["DataType_8"] = widgetRundownElements[i].dataType_8;
                    widgetElement["DataValue_8"] = widgetRundownElements[i].dataValue_8;

                    dataTable.Rows.Add(widgetElement);
                }
                WidgetRundownAccess widgetRundown = new WidgetRundownAccess();
                widgetRundown.WingDBConnectionString = WingDBConnectionString;
                widgetRundown.SaveWidgetRundown(dataTable);
            }
        }

        /// <summary>
        /// Append an element to the collection
        /// </summary>
        public void AppendWidgetRundownElement(WidgetRundownElementModel widgetRundownElement)
        {
            widgetRundownElements.Add(widgetRundownElement);
        }

        /// <summary>
        /// Insert an element into the collection at the specified location
        /// </summary>
        public void AppendWidgetRundownElement(Int16 insertPoint, WidgetRundownElementModel widgetRundownElement)
        {
            widgetRundownElements.Insert(insertPoint, widgetRundownElement);
        }

        /// <summary>
        /// Delete an element from the collection at the specified location
        /// </summary>
        public void DeleteWidgetRundownElement(Int16 deletePoint)
        {
            if (widgetRundownElements.Count > 0 && deletePoint <= (widgetRundownElements.Count - 1))
            {
                widgetRundownElements.RemoveAt(deletePoint);
            }
        }

        /// <summary>
        /// Move the specified element down in the list
        /// </summary>
        public void MoveWidgetRundownElementDown(Int16 itemIndex)
        {
            if (itemIndex < widgetRundownElements.Count - 1)
            {
                var item = widgetRundownElements[itemIndex];
                widgetRundownElements.RemoveAt(itemIndex);
                widgetRundownElements.Insert(itemIndex + 1, item);
            }
        }

        /// <summary>
        /// Move the specified element up in the list
        /// </summary>
        public void MovewidgetRundownElementUp(Int16 itemIndex)
        {
            if (itemIndex > 0)
            {
                var item = widgetRundownElements[itemIndex];
                widgetRundownElements.RemoveAt(itemIndex);
                widgetRundownElements.Insert(itemIndex - 1, item);
            }
        }

        /// <summary>
        /// Enable the specified element
        /// </summary>
        public void EnableTopicRundownElement(Int16 itemIndex)
        {
            var item = widgetRundownElements[itemIndex];
            item.enabled = true;
        }

        /// <summary>
        /// Disable the specified element
        /// </summary>
        public void DisableWidgetRundownElement(Int16 itemIndex)
        {
            var item = widgetRundownElements[itemIndex];
            item.enabled = false;
        }
        #endregion
    }
}
