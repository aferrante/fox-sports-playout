﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoxSports.WingController.Data.DataModel
{
    /// <summary>
    /// Class definition for Playout Commands collection
    /// </summary>
    public class PlayoutCommandModel
    {
        public string commandText { get; set; }
        public string parameterValue { get; set; }
    }
}
