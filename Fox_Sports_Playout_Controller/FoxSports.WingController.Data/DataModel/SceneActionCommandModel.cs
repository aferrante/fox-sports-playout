﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoxSports.WingController.Data.DataModel
{
    /// <summary>
    /// Class definition for Scene Action Commands collection
    /// </summary>
    public class SceneActionCommandModel
    {
        public Int32 actionId { get; set; }
        public Int32 commandId { get; set; }
        public string commandText { get; set; }
        public string commandDescription { get; set; }
        public Int32 parameterType { get; set; }
        public string parameterValue { get; set; }
        public string parameterDescription { get; set; }
    }
}
