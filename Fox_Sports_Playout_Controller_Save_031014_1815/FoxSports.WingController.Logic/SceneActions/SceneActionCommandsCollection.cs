﻿// Definition for class used for all scene action command functions
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FoxSports.WingController.Data.DataAccess;
using FoxSports.WingController.Data.DataModel;
using FoxSports.WingController.VizIntf.VizControlFunctions;
using System.Data.SqlClient;
using System.ComponentModel;
using FoxSports.WingController.Data.Enums;

namespace FoxSports.WingController.Logic.SceneActions
{
    /// <summary>
    /// Class for operations related to the topic rundown
    /// </summary>
    public class SceneActionCommandsCollection
    {
        #region Delegates & Events (testing only)
        public delegate void delCommandAdded(SceneActionCommandsCollection sender, string Command);
        public event delCommandAdded CommandAdded;        
        #endregion

        #region Properties and Members
        public List<SceneActionCommandModel> sceneActionCommands;
        public List<PlayoutCommandModel> playoutCommands;
        public string WingDBConnectionString { get; set; }
        private VizControlPort VizController;
        private BindingList<TopicRundownElementModel> topicRundownElements;
        #endregion

        #region Public Methods
        // Constructor - instantiates list collection
        public SceneActionCommandsCollection(VizControlPort VizControl, BindingList<TopicRundownElementModel> topicRundownElementsCollection)
        {
            // Create collections
            sceneActionCommands = new List<SceneActionCommandModel>();
            playoutCommands = new List<PlayoutCommandModel>();
            VizController = VizControl;
            topicRundownElements = topicRundownElementsCollection;
        }

        /// <summary>
        /// Get the scene action list from the SQL DB; clears out existing collection first
        /// </summary>
        public List<SceneActionCommandModel> GetSceneActionCommandCollection()
        {
            DataTable dataTable;

            // Clear out the current collection
            sceneActionCommands.Clear();

            try
            {
                SceneActionCommandsAccess sceneActionCommandsAccess = new SceneActionCommandsAccess();
                sceneActionCommandsAccess.WingDBConnectionString = WingDBConnectionString;
                dataTable = sceneActionCommandsAccess.GetSceneActionCommands();

                foreach (DataRow row in dataTable.Rows)
                {
                    var newSceneActionCommand = new SceneActionCommandModel()
                    {
                        actionId = Convert.ToInt32(row["ActionId"]),
                        commandId = Convert.ToInt32(row["CommandId"]),
                        commandText = row["CommandText"].ToString(),
                        commandDescription = row["CommandDescription"].ToString(),
                        parameterType = Convert.ToInt32(row["ParameterType"]),
                        parameterValue = row["ParameterValue"].ToString(),
                        parameterDescription = row["ParameterDescription"].ToString(),
                    };
                    sceneActionCommands.Add(newSceneActionCommand);
                }
            }
            catch
            {
                throw;
            }
            // Return 
            return sceneActionCommands;
        }

        /// <summary>
        /// Function to create the playout command collection based on the action ID sent; also calls function to look up parameter values
        /// </summary>
        public void SendPlayoutCommands(Int32 selectedActionID)
        {
            // Clear out the current collection
            playoutCommands.Clear();

            try
            {
                // Do LINQ query to get commands accociated with the specified Action ID
                var applicableCommands = from sceneActionCommand in sceneActionCommands
                                 where (sceneActionCommand.actionId == selectedActionID)
                                 select new { Command = sceneActionCommand.commandText,
                                              ParameterType = sceneActionCommand.parameterType,
                                              ParameterValue = sceneActionCommand.parameterValue
                                 };
                if (applicableCommands != null)
                {
                    // Send each variable - if not a static value, call method to get required value
                    foreach (var item in applicableCommands)
                    {
                        // Fire event to update listbox for debug on main form
                        CommandAdded(this, item.Command + " = " + item.ParameterValue);

                        // Get value of parameter
                        string paramValue = "";
                        // It's a variable so call method to get applicable data value
                        if (item.ParameterType == (int)SceneActionParameterTypes.Variable_Value)
                        {
                            paramValue = GetCurrentParameterValue(item.ParameterValue);
                        }
                        // It's a static value, so just send the value retrieved from the database
                        else if (item.ParameterType == (int)SceneActionParameterTypes.Static_Value)
                        {
                            paramValue = item.ParameterValue;
                        }

                        // Send command to Viz
                        this.VizController.SendDataPoolCommand(item.Command + "=" + paramValue);
                    }                                       
                }
            }
            catch
            {
                throw;
            }
        }

        // Function to get the current value of the specified variable for a single command
        private string GetCurrentParameterValue(string ParameterValue)
        {
            string outputValue = ParameterValue;

            // Lookup variable value
            // General fields
            if (ParameterValue == "$Blank") 
            {
                outputValue = " ";
            }
            // Rundown fields
            else if (ParameterValue == "$ShowSkinName")
            {
            }
            else if (ParameterValue == "$RundownRow1")
            {
                if (topicRundownElements.Count > 0)
                {
                    outputValue = topicRundownElements[0].slug;
                }
                else
                {
                outputValue = " ";
                }
            }
            else if (ParameterValue == "$RundownRow2")
            {
                if (topicRundownElements.Count > 1)
                {
                    outputValue = topicRundownElements[1].slug;
                }
                else
                {
                    outputValue = " ";
                }
            }
            else if (ParameterValue == "$RundownRow3")
            {
                if (topicRundownElements.Count > 2)
                {
                    outputValue = topicRundownElements[2].slug;
                }
                else
                {
                    outputValue = " ";
                }
            }
            else if (ParameterValue == "$RundownRow4")
            {
                if (topicRundownElements.Count > 3)
                {
                    outputValue = topicRundownElements[3].slug;
                }
                else
                {
                    outputValue = " ";
                }
            }
            else if (ParameterValue == "$RundownRow5")
            {
                if (topicRundownElements.Count > 4)
                {
                    outputValue = topicRundownElements[4].slug;
                }
                else
                {
                    outputValue = " ";
                }
            }
            else if (ParameterValue == "$RundownRow6")
            {
                if (topicRundownElements.Count > 5)
                {
                    outputValue = topicRundownElements[5].slug;
                }
                else
                {
                    outputValue = " ";
                }
            }
            else if (ParameterValue == "$RundownNote1")
            {
            }
            else if (ParameterValue == "$RundownNote2")
            {
            }
            else if (ParameterValue == "$RundownNote3")
            {
            }
            else if (ParameterValue == "$RundownNote4")
            {
            }
            else if (ParameterValue == "$RundownNote5")
            {
            }
            else if (ParameterValue == "$RundownNote6")
            {
            }
            else if (ParameterValue == "$RundownColor1") 
            {
            }
            else if (ParameterValue == "$RundownColor2") 
            {
            }
            else if (ParameterValue == "$RundownColor3") 
            {
            }
            else if (ParameterValue == "$RundownColor4") 
            {
            }
            else if (ParameterValue == "$RundownColor5") 
            {
            }
            else if (ParameterValue == "$RundownColor6") 
            {
            }
            // Widget fields
            else if (ParameterValue == "$WidgetColor") 
            {
            }
            else if (ParameterValue == "$PromoImage")
            {
            }
            else if (ParameterValue == "$WidgetHeadLogo") 
            {
            }
            else if (ParameterValue == "$WidgetHeadshot") 
            {
            }
            else if (ParameterValue == "$WidgetLogo") 
            {
            }
            else if (ParameterValue == "$WidgetLogoHead") 
            {
            }
            else if (ParameterValue == "$WidgetNoteText1") 
            {
            }
            else if (ParameterValue == "$WidgetTitle1") 
            {
            }
            else if (ParameterValue == "$WidgetTitle2") 
            {
            }
            return outputValue;
        }
        #endregion
    }
}
