﻿// Main form for Fox Sports Wing Playout Controller application
// © Copyright 2013-2014 Video Design Software Inc. - All rights reserved.
// You may use this code module and all associated code modules only under the specific terms of the source code license between Video Design Software Inc. and Fox Sports Media Group.
// M Dilworth  Video Design Software 
// Rev: 2014/02/10

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Configuration;
using FoxSports.WingController.VizIntf.VizControlFunctions;
using FoxSports.WingController.VizIntf.ClientSocket;
using FoxSports.WingController.Data.DataAccess;
using FoxSports.WingController.Logic.RundownCollection;
using FoxSports.WingController.Logic.WidgetCollection;
using FoxSports.WingController.Logic.SceneActions;
using FoxSports.WingController.Data.DataModel;
using FoxSports.WingController.Data.Enums;

namespace Fox_Sports_Playout_Controller
{
    /// <summary>
    /// Class definition for main program form
    /// </summary>
    public partial class frmMain : Form
    {
        /// <summary>
        /// Globals
        /// </summary>
        // Flag to indicator wing is in
        bool wingIn = false;
        // Database connection string
        private string wingDBConnectionString;

        /// <summary>
        /// Define classes for collections and logic
        /// </summary>
        // Define the collection object for the Topic Rundown
        private TopicRundownCollection topicRundownCollection;
        BindingList<TopicRundownElementModel> topicRundownElements;

        // Define the collection object for the Widget Rundown
        private WidgetRundownCollection widgetRundownCollection;
        BindingList<WidgetRundownElementModel> widgetRundownElements;

        // Define the class for the scene actions collection
        private SceneActionCommandsCollection sceneActionCommandsCollection;
        List<SceneActionCommandModel> sceneActionCommands;

        /// <summary>
        /// Declare socket parameters for Viz communications & Viz scene name
        /// </summary>
        private Int32 socketPort;
        private string socketIPAddress;
        private string vizSceneName;
        public VizControlPort VizControl;

        /// <summary>
        /// Function to read in data from application config file
        /// </summary>
        private void ReadInConfigData()
        {
            //Read in values from the config file
            socketIPAddress = Properties.Settings.Default.vizControlIPAddress;
            socketPort = Properties.Settings.Default.vizControlPortNumber;
            wingDBConnectionString = Properties.Settings.Default.wingDBConnectionString;
            vizSceneName = Properties.Settings.Default.vizSceneName;
        }

        /// <summary>
        /// Handler for main form activation
        /// </summary>
        public frmMain()
        {
            InitializeComponent();

            // Read in values from the config file
            ReadInConfigData();

            // Setup topic rundown collection & bind to grid
            this.topicRundownCollection = new TopicRundownCollection();
            this.topicRundownCollection.WingDBConnectionString = wingDBConnectionString;
            topicRundownElements = this.topicRundownCollection.GetTopicRundownCollection(0);
            var topicGridDataSource = new BindingSource(topicRundownElements, null);
            TopicRundownGrid.DataSource = topicGridDataSource;
            // Get the current data for the topic rundown from the database
            GetTopicRundownElementsFromDB();

            // Setup widget rundown collection & bind to grid
            this.widgetRundownCollection = new WidgetRundownCollection();
            this.widgetRundownCollection.WingDBConnectionString = wingDBConnectionString;
            widgetRundownElements = this.widgetRundownCollection.GetWidgetRundownCollection(1);
            var widgetGridDataSource = new BindingSource(widgetRundownElements, null);
            WidgetRundownGrid.DataSource = widgetGridDataSource;
            // Get the data for the widget rundown from the database
            GetWidgetRundownElementsFromDB();

            // Instantiate Viz interface component & connect to the Viz engine
            VizControl = new VizControlPort(System.Net.IPAddress.Parse(socketIPAddress), socketPort, true);
            // Initialize the events
            VizControl.DataReceived += new TCPClientWrapper.delDataReceived(VizControl_DataReceived);
            VizControl.ConnectionStatusChanged += new TCPClientWrapper.delConnectionStatusChanged(VizControl_ConnectionStatusChanged);
            // Connect
            VizControl.AutoReconnect = true;
            VizControl.Connect();
            VizControl.LoadScene(vizSceneName);

            // Setup the scene commands collection
            this.sceneActionCommandsCollection = new SceneActionCommandsCollection(VizControl, topicRundownElements);
            this.sceneActionCommandsCollection.WingDBConnectionString = wingDBConnectionString;
            sceneActionCommands = this.sceneActionCommandsCollection.GetSceneActionCommandCollection();
            // For testing
            sceneActionCommandsCollection.CommandAdded += new SceneActionCommandsCollection.delCommandAdded(AddToListbox);

            // Enable timers
            TimeOfDayTimer.Enabled = true;
        }

        // Test Function
        private void AddToListbox(SceneActionCommandsCollection sender, string Command)
        {
            //listBox1.Items.Add(Command);
        }

        /// <summary>
        /// VIZ CONTROL PORT EVENTS
        /// </summary>
        // Fired when the connection status changes in the TCP client       
        void VizControl_ConnectionStatusChanged(TCPClientWrapper sender, VizControlPort.ConnectionStatus status)
        {
            //Check if this event was fired on a different thread, if it is then we must invoke it on the UI thread
            if (InvokeRequired)
            {
                Invoke(new TCPClientWrapper.delConnectionStatusChanged(VizControl_ConnectionStatusChanged), sender, status);
                return;
            }
            statusLabel.Text = "Connection Status: " + status.ToString();

            //Set the indicator color if the connection is good
            if (status.ToString() == "Connected")
            {
                vizConnectLED.BackColor = Color.Lime;
            }
            else
            {
                vizConnectLED.BackColor = Color.Transparent;
            }
        }

        // Fired when new data is received in the TCP client
        void VizControl_DataReceived(TCPClientWrapper sender, object data)
        {
            //Check if this needs to be invoked in the UI thread
            if (InvokeRequired)
            {
                try
                {
                    Invoke(new VizControlPort.delDataReceived(VizControl_DataReceived), sender, data);
                }
                catch
                { }
                return;
            }

            //Interpret the received data object as a string
            string strData = data as string;

            //Add the received data to a rich text box
            statusLabel.Text = "Data received: " + strData.Trim() + "\r\n";
        }

        /// <summary>
        /// TOPIC RUNDOWN FUNCTIONS
        /// </summary>
        /// <summary>
        // Get the topic rundown
        /// </summary>
        private void GetTopicRundownElementsFromDB()
        {
            topicRundownElements = this.topicRundownCollection.GetTopicRundownCollection(0);
            lblTopicRundownName.Text = this.topicRundownCollection.TopicRundownName.ToString();
            lblTopicRundownCount.Text = topicRundownElements.Count.ToString();
        }

        /// <summary>
        /// Delete the selected entry from the topic rundown collection
        /// </summary>
        private void deleteSelectedItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (TopicRundownGrid.SelectedRows.Count > 0)
            {
                topicRundownCollection.DeleteTopicRundownElement((short)TopicRundownGrid.CurrentRow.Index);
                this.Validate();
            }
        }

        /// <summary>
        /// Move the selected item down in the topic rundown
        /// </summary>
        private void btnMoveTopicDown_Click(object sender, EventArgs e)
        {
            if (TopicRundownGrid.RowCount > 0)
            {
                topicRundownCollection.MoveTopicRundownElementDown((short)TopicRundownGrid.CurrentRow.Index);
                if (TopicRundownGrid.CurrentRow.Index < TopicRundownGrid.RowCount - 1)
                {
                    TopicRundownGrid.Rows[TopicRundownGrid.CurrentRow.Index + 1].Selected = true;
                    TopicRundownGrid.CurrentCell = TopicRundownGrid[0, TopicRundownGrid.CurrentRow.Index + 1];
                }
            }
        }

        /// <summary>
        /// Move the selected item up in the topic rundown
        /// </summary>
        private void btnMoveTopicUp_Click(object sender, EventArgs e)
        {
            if (TopicRundownGrid.RowCount > 0)
            {
                int currentRowIndex = TopicRundownGrid.CurrentRow.Index;
                topicRundownCollection.MoveTopicRundownElementUp((short)TopicRundownGrid.CurrentRow.Index);
                if (TopicRundownGrid.CurrentRow.Index > 0)
                {
                    TopicRundownGrid.Rows[currentRowIndex - 1].Selected = true;
                    TopicRundownGrid.CurrentCell = TopicRundownGrid[0, currentRowIndex - 1];
                }
            }
        }

        /// <summary>
        /// Enable the selected item in the Topic Rundown
        /// </summary>
        private void enableSelectedItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (TopicRundownGrid.RowCount > 0)
            {
                //TopicRundownElementModel obj = (TopicRundownElementModel)TopicRundownGrid.CurrentRow.DataBoundItem;
                //obj.enabled = true;
                topicRundownCollection.EnableRundownElement((short)TopicRundownGrid.CurrentRow.Index);
                TopicRundownGrid.Refresh();

                // Need to force validation
                this.Validate();
            }
        }

        /// <summary>
        /// Disable the selected item in the Topic Rundown
        /// </summary>
        private void disableSelectedItemToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (TopicRundownGrid.RowCount > 0)
            {
                //TopicRundownElementModel obj = (TopicRundownElementModel)TopicRundownGrid.CurrentRow.DataBoundItem;
                //obj.enabled = false;
                topicRundownCollection.DisableRundownElement((short)TopicRundownGrid.CurrentRow.Index);
                TopicRundownGrid.Refresh();

                // Need to force validation
                this.Validate();
            }
        }

        /// <summary>
        /// Select & load a Topic Rundown out to the database
        /// </summary>
        private void LoadTopicPlaylistBtn_Click(object sender, EventArgs e)
        {
            Form loadTopicPlaylist = new frmPlaylistLoad();
            loadTopicPlaylist.ShowDialog();
        }

        /// <summary>
        /// Save the Topic Rundown out to the database
        /// </summary>
        private void btnSaveTopicPlaylist_Click(object sender, EventArgs e)
        {
            if (TopicRundownGrid.RowCount > 0)
            {
                topicRundownCollection.SaveTopicRundownCollection(topicRundownElements);
            }
        }

        /// <summary>
        /// Take next rundown element to air
        /// </summary>
        private void btnTakeNextTopic_Click(object sender, EventArgs e)
        {
            // Send the command
            if (wingIn)
            {
                this.sceneActionCommandsCollection.SendPlayoutCommands((int)ActionIDs.Send_Rundown);
            }
        }


        // WIDGET RUNDOWN FUNCTIONS
        /// <summary>
        /// Function to query the SQL database and get the data for the specified widgetrundown
        /// </summary>
        private void GetWidgetRundownElementsFromDB()
        {
            widgetRundownElements = this.widgetRundownCollection.GetWidgetRundownCollection(1);
            lblWidgetRundownName.Text = this.widgetRundownCollection.WidgetRundownName.ToString();
            lblWidgetRundownCount.Text = widgetRundownElements.Count.ToString();
        }

        /// <summary>
        /// Delete the selected item in the Widget Rundown
        /// </summary>
        private void deleteSelectedWidgetItem_Click(object sender, EventArgs e)
        {
            if (WidgetRundownGrid.SelectedRows.Count > 0)
            {
                widgetRundownCollection.DeleteWidgetRundownElement((short)WidgetRundownGrid.CurrentRow.Index);
                this.Validate();
            }
        }

        /// <summary>
        /// Move the selected item down in the widget rundown
        /// </summary>
        private void btnMoveWidgetDown_Click(object sender, EventArgs e)
        {
            if (WidgetRundownGrid.RowCount > 0)
            {
                widgetRundownCollection.MoveWidgetRundownElementDown((short)WidgetRundownGrid.CurrentRow.Index);
                if (WidgetRundownGrid.CurrentRow.Index < WidgetRundownGrid.RowCount - 1)
                {
                    WidgetRundownGrid.Rows[WidgetRundownGrid.CurrentRow.Index + 1].Selected = true;
                    WidgetRundownGrid.CurrentCell = WidgetRundownGrid[0, WidgetRundownGrid.CurrentRow.Index + 1];
                }
            }
        }

        /// <summary>
        /// Move the selected item up in the widget rundown
        /// </summary>
        private void btnMoveWidgetUp_Click(object sender, EventArgs e)
        {
            if (WidgetRundownGrid.RowCount > 0)
            {
                int currentRowIndex = WidgetRundownGrid.CurrentRow.Index;
                widgetRundownCollection.MoveWidgetRundownElementUp((short)WidgetRundownGrid.CurrentRow.Index);
                if (WidgetRundownGrid.CurrentRow.Index > 0)
                {
                    WidgetRundownGrid.Rows[currentRowIndex - 1].Selected = true;
                    WidgetRundownGrid.CurrentCell = WidgetRundownGrid[0, currentRowIndex - 1];
                }
            }
        }

        /// <summary>
        /// Enable the selected item in the Widget Rundown
        /// </summary>
        private void enableSelectedWidgetItem_Click(object sender, EventArgs e)
        {
            if (WidgetRundownGrid.RowCount > 0)
            {
                widgetRundownCollection.EnableWidgetRundownElement((short)WidgetRundownGrid.CurrentRow.Index);
                WidgetRundownGrid.Refresh();

                // Need to force validation
                this.Validate();
            }
        }

        /// <summary>
        /// Disable the selected item in the Widget Rundown
        /// </summary>
        private void disableSelectedWidgetItem_Click(object sender, EventArgs e)
        {
            if (WidgetRundownGrid.RowCount > 0)
            {
                widgetRundownCollection.DisableWidgetRundownElement((short)WidgetRundownGrid.CurrentRow.Index);
                WidgetRundownGrid.Refresh();

                // Need to force validation
                this.Validate();
            }
        }

        /// <summary>
        /// Save the Widget Rundown out to the database
        /// </summary>
        private void btnSaveWidgetPlaylist_Click(object sender, EventArgs e)
        {
            if (WidgetRundownGrid.RowCount > 0)
            {
                widgetRundownCollection.SaveWidgetRundownCollection(widgetRundownElements);
            }
        }

        /// <summary>
        /// TIMER FUNCTIONS
        /// </summary>
        //Handler for time of day timer
        private void TimeOfDayTimer_Tick(object sender, EventArgs e)
        {
            DateTime currentDateTime = DateTime.Now;
            lblTimeOfDay.Text = currentDateTime.ToString();
        }

        /// <summary>
        /// MISCELLANEOUS GUI FUNCTIONS
        /// </summary>
        /// <summary>
        /// Handler for main Wing In/Out button
        /// </summary>
        private void WingInBtn_Click(object sender, EventArgs e)
        {
            if (wingIn == false)
            {
                //Set button/indicator properties & set flag
                wingIn = true;
                MainWingControlPanel.BackColor = Color.Lime;
                btnWingIn.Text = "Take Wing Out (F1)";
                btnWingIn.Image = Fox_Sports_Playout_Controller.Properties.Resources.StatusAnnotations_Stop_32xLG;
                // Send the command
                this.sceneActionCommandsCollection.SendPlayoutCommands((int)ActionIDs.Wing_In);
            }
            else
            {
                //Set button/indicator properties & clear flag
                wingIn = false;
                MainWingControlPanel.BackColor = Color.Transparent;
                btnWingIn.Text = "Take Wing In (F1)";
                btnWingIn.Image = Fox_Sports_Playout_Controller.Properties.Resources.StatusAnnotations_Play_32xLG_color;
                // Send the command
                this.sceneActionCommandsCollection.SendPlayoutCommands((int)ActionIDs.Wing_Out);

            }
        }


        /// <summary>
        /// Handler for change in iNews monitoring checkbox status - enable/disable save/load playlist butons accordingly
        /// </summary>
        private void iNewsMonitoringEnableCheckbox_CheckStateChanged(object sender, EventArgs e)
        {
            if (iNewsMonitoringEnableCheckbox.Checked)
            {
                btnSaveTopicPlaylist.Visible = false;
                btnLoadTopicPlaylist.Visible = false;
                iNewsMonitoringEnableCheckbox.BackColor = Color.Transparent;
                iNewsMonitoringEnableCheckbox.Text = "iNews Monitoring Enabled";
            }
            else
            {
                btnSaveTopicPlaylist.Visible = true;
                btnLoadTopicPlaylist.Visible = true;
                iNewsMonitoringEnableCheckbox.BackColor = Color.Yellow;
                iNewsMonitoringEnableCheckbox.Text = "iNews Monitoring Disabled";
            }
        }
        
        #region Functions for launching support dialogs
        /// <summary>
        /// Handler for create new topic rundown element
        /// </summary>
        private void CreateNewTopicBtn_Click(object sender, EventArgs e)
        {
            Form createNewTopicRecord = new frmTemplateDataEntry();
            createNewTopicRecord.ShowDialog();
        }

        /// <summary>
        /// Handler for Change Skin button
        /// </summary>
        private void button5_Click(object sender, EventArgs e)
        {
            Form selectSkin = new frmSkinSelect();
            selectSkin.ShowDialog();
        }
        #endregion
    }
}
