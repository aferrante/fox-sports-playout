﻿// Definition for class used for all rundown collection operations
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FoxSports.WingController.Data.DataAccess;
using FoxSports.WingController.Data.DataModel;
using System.Data.SqlClient;
using System.ComponentModel;

namespace FoxSports.WingController.Logic.RundownCollection
{
    /// <summary>
    /// Class for operations related to the topic rundown
    /// </summary>
    public class TopicRundownCollection
    {
        #region Properties and Members
        public BindingList<TopicRundownElementModel> topicRundownElements;
        public string WingDBConnectionString { get; set; }
        #endregion

        #region Public Methods
        // Constructor - instantiates list collection
        public TopicRundownCollection()
        {
            // Create list
            topicRundownElements = new BindingList<TopicRundownElementModel>();
        }

        /// <summary>
        /// Get the topic list from the SQL DB; clears out existing collection first
        /// </summary>
        public BindingList<TopicRundownElementModel> GetTopicRundownCollection(double topicRundownID)
        {
            //var topicRundownCollection = new List<TopicRundownElementModel>();
            //DataTable dataTable = new DataTable();
            DataTable dataTable;

            // Clear out the current collection
            topicRundownElements.Clear();

            try
            {
            TopicRundownAccess topicRundown = new TopicRundownAccess();
                topicRundown.WingDBConnectionString = WingDBConnectionString;
                dataTable = topicRundown.GetTopicRundown(topicRundownID);

                foreach (DataRow row in dataTable.Rows)
                {
                    var newTopic = new TopicRundownElementModel()
                    {
                        enabled = Convert.ToBoolean(row["Enabled"]),
                        rundownName = row["Rundownname"].ToString(),
                        rundownId = Convert.ToDouble(row["RundownId"]),
                        pageId = Convert.ToInt32(row["PageId"]),
                        templateId = Convert.ToInt32(row["TemplateId"]),
                        slug = row["Slug"].ToString(),
                        longSlug = row["longSlug"].ToString()
                    };
                    topicRundownElements.Add(newTopic);
                }
            }
            catch
            {
                throw;
            }
            // Return 
            return topicRundownElements;
        }

        /// <summary>
        /// Save the topic list to the SQL DB; clears out existing collection first
        /// </summary>
        public void SaveTopicRundownCollection(BindingList<TopicRundownElementModel> topicRundownElements)
        {
                       
            if (topicRundownElements.Count > 0)
            {
                DataTable dataTable = new DataTable();
                dataTable.Columns.Add("RundownName");
                dataTable.Columns.Add("RundownID");
                dataTable.Columns.Add("PageID");
                dataTable.Columns.Add("TemplateID");
                dataTable.Columns.Add("Enabled");
                dataTable.Columns.Add("Slug");
                dataTable.Columns.Add("LongSlug");

                for (int i = 0; i < topicRundownElements.Count; i++)
                {
                    DataRow rundownElement = dataTable.NewRow();
                    rundownElement["RundownName"] = topicRundownElements[i].rundownName;
                    rundownElement["RundownID"] = topicRundownElements[i].rundownId;
                    rundownElement["PageID"] = topicRundownElements[i].pageId;
                    rundownElement["TemplateID"] = topicRundownElements[i].templateId;
                    rundownElement["Enabled"] = topicRundownElements[i].enabled;
                    rundownElement["Slug"] = topicRundownElements[i].slug;
                    rundownElement["LongSlug"] = topicRundownElements[i].longSlug;

                    dataTable.Rows.Add(rundownElement);
                }
                TopicRundownAccess topicRundown = new TopicRundownAccess();
                topicRundown.WingDBConnectionString = WingDBConnectionString;
                topicRundown.SaveTopicRundown(dataTable);
            }
        }

        /// <summary>
        /// Append an element to the collection
        /// </summary>
        public void AppendTopicRundownElement(TopicRundownElementModel topicRundownElement)
        {
            topicRundownElements.Add(topicRundownElement);
        }

        /// <summary>
        /// Insert an element into the collection at the specified location
        /// </summary>
        public void AppendTopicRundownElement(Int16 insertPoint, TopicRundownElementModel topicRundownElement)
        {
            topicRundownElements.Insert(insertPoint, topicRundownElement);
        }

        /// <summary>
        /// Delete an element from the collection at the specified location
        /// </summary>
        public void DeleteTopicRundownElement(Int16 deletePoint)
        {
            if (topicRundownElements.Count > 0 && deletePoint <= (topicRundownElements.Count-1))
            {
                topicRundownElements.RemoveAt(deletePoint);
            }
        }

        /// <summary>
        /// Move the specified element down in the list
        /// </summary>
        public void MoveTopicRundownElementDown(Int16 itemIndex)
        {
            if (itemIndex < topicRundownElements.Count-1)
            {
                var item = topicRundownElements[itemIndex];
                topicRundownElements.RemoveAt(itemIndex);
                topicRundownElements.Insert(itemIndex + 1, item);
            }
        }

        /// <summary>
        /// Move the specified element up in the list
        /// </summary>
        public void MoveTopicRundownElementUp(Int16 itemIndex)
        {
            if (itemIndex > 0)
            {
                var item = topicRundownElements[itemIndex];
                topicRundownElements.RemoveAt(itemIndex);
                topicRundownElements.Insert(itemIndex - 1, item);
            }
        }

        /// <summary>
        /// Enable the specified element
        /// </summary>
        public void EnableRundownElement(Int16 itemIndex)
        {
            var item = topicRundownElements[itemIndex];
            item.enabled = true;
        }

        /// <summary>
        /// Disable the specified element
        /// </summary>
        public void DisableRundownElement(Int16 itemIndex)
        {
            var item = topicRundownElements[itemIndex];
            item.enabled = false;
        }
        #endregion
    }
}
