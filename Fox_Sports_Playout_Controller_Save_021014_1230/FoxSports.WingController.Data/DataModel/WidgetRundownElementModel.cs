﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FoxSports.WingController.Data.DataModel
{
    public class WidgetRundownElementModel
    {
        public double rundownid { get; set; }

        public double pageid { get; set; }

        public bool enabled { get; set; }

        public int templateId { get; set; }

        public string dataField1 { get; set; }

        public string dataField2 { get; set; }
        
        public string dataField3 { get; set; }
        
        public string dataField4 { get; set; }
        
        public string dataField5 { get; set; }
        
        public string dataField6 { get; set; }
    }
}
